/*document.addEventListener('DOMContentLoaded', function () {
  particleground(document.getElementById('particles'), {
    dotColor: '#fff',
    lineColor: '#fff'
  });
  var intro = document.getElementById('intro');
  intro.style.marginTop = - intro.offsetHeight / 2 + 'px';
}, false);
*/


// jQuery plugin example:
$(document.body).ready(function() {
  $('.particles-view').particleground({
    dotColor: '#f3f3f3',
    lineColor: '#ffffff',
    lineWidth: .3
  });
  $('.intro').css({
    'margin-top': -($('.intro').height() / 2)
  });
});
