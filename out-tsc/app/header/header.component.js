var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { LoginService } from "../login/login.service";
import { HeaderService } from "./header.service";
import { AdmissionService } from "../admission/admission.service";
import { FormValidator } from "../form.validator";
import { TransportService } from "../transport/transport.service";
import { GroupService } from "../groups/groups.service";
export var HeaderComponent = (function () {
    /*//////////////////////////*/
    function HeaderComponent(router, loginservice, headerservice, admissionservice, formValidator, transportservice, groupservice) {
        this.router = router;
        this.loginservice = loginservice;
        this.headerservice = headerservice;
        this.admissionservice = admissionservice;
        this.formValidator = formValidator;
        this.transportservice = transportservice;
        this.groupservice = groupservice;
        this.getSchoolData = [];
        this.getSchoolName = "";
        this.getSchoolImage = "";
        this.getSchoolState = "";
        this.getSchoolLocality = "";
        this.getAuthToken = localStorage.getItem('usertoken');
        this.getSchoolId = localStorage.getItem('schoolinfo');
        /*Admission variables*/
        this.currentSchoolId = localStorage.getItem('schoolinfo');
        this.selectedAcademicYearObj = null;
        this.selectedCategory = null;
        this.selectedClassSection = null;
        this.selectedClassSectionId = null;
        this.selectedBusRoute = null;
        this.selectedBusStop = null;
        this.academicId = 0;
        this.schoolAcademicCurrentYear = "";
        this.feesComponentsList = [];
        this.addAmountStatus = true;
        this.applyAmountStatus = false;
        this.mTotalFees = "Rs. 0";
        this.mTotalBusFees = "Rs. 0";
        this.feesAmount = 0;
        this.mFeesComponentName = "";
        this.mFeesComponentDue = "";
        this.mFeesComponentAmount = "0";
        this.busRouteList = [];
        this.busStopList = [];
        this.admissionStudentImage = "assets/img/avatar-students.png";
        this.mStudentName = "";
        this.mGenderGroup = "";
        this.mBloodGroup = "";
        this.mReligion = "";
        this.mNationality = "";
        this.mFatherAddress = "";
        this.mMotherAddress = "";
        this.mAdmissionStream = "";
        this.tab1activestatus = true;
        this.selectedAcademicYear = "";
        this.admissionBase = "";
        this.nextTabStatus1 = false;
        this.nextTabStatus2 = false;
        this.classSections = null;
        this.mTotalFees = "Rs. 0";
    }
    //constructor(private admissionservice: AdmissionService,private groupservice:GroupeSrvice,  private router: Router, private loginservice: LoginService,private elRef:ElementRef,private transportservice:TransportService, private  formValidator: FormValidator) {    this.mTotalFees  = "Rs. 0";  }
    HeaderComponent.prototype.getSchoolInfo = function () {
        this.headerservice.schoolInfo(this.getSchoolId, this.getAuthToken)
            .subscribe(function (data) {
        }, function (error) { return console.log(error); }, function () { });
    };
    //token auth
    HeaderComponent.prototype.isValidToken = function () {
        var _this = this;
        var isValidToken = false;
        var isValidSchool = 0;
        this.loginservice.isValidToken(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            isValidToken = data.isValid;
            console.log("Token Validation in login page: ", isValidToken);
            if (isValidToken == true) {
                //this.router.navigate(['/dashboard']);
                //school id validation///////////////////////////////////////////////
                _this.loginservice.isValidSchoolId(localStorage.getItem('schoolinfo'))
                    .subscribe(function (data) {
                    isValidSchool = data.schoolExist;
                    _this.getSchoolId = localStorage.getItem('schoolinfo');
                    if (isValidSchool != 1) {
                        _this.router.navigate(['/login']);
                    }
                    else {
                        console.log("Stay in page (Header)");
                        //Get School Info////////////////
                        _this.headerservice.schoolInfo(_this.getSchoolId, localStorage.getItem('usertoken'))
                            .subscribe(function (data) {
                            console.log("Get school info on header: ", data);
                            // this.getSchoolData.push(data[0][0]);
                            //
                            // console.log("Get school data array : ----------- ",this.getSchoolData);
                            //this.getSchoolData = data[0].;
                            _this.getSchoolName = data.schoolName;
                            _this.getSchoolImage = data.schoolImage;
                            _this.getSchoolState = data.state;
                            _this.getSchoolLocality = data.locality;
                            _this.getAllAcademicYears();
                            _this.getAllCategories();
                            _this.getCurrentAcademic(_this.currentSchoolId);
                            _this.getAllBusRoutes(_this.currentSchoolId);
                        }, function (error) { return console.log(error); }, function () { });
                    }
                }, function (error) { return console.log(error); }, function () { });
            }
            else {
                _this.router.navigate(['/login']);
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    //tab 1 validate
    HeaderComponent.prototype.validateTab1 = function () {
        this.mDOB = $("#admission-dob").val();
        this.mAdmissionDate = $("#admission-date").val();
        //customToastr('Updated','success');
        if (!this.formValidator.validateIsData(this.mStudentName)) {
            customToastr('Invalid student name!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.mGenderGroup)) {
            customToastr('Invalid student gender!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.mDOB)) {
            customToastr('Select student DOB!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.mBloodGroup)) {
            customToastr('Blood group is not valid!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.mNationality)) {
            customToastr('Select student nationality!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.selectedAcademicYear)) {
            customToastr('Invalid academic year!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.admissionBase)) {
            customToastr('Select admission base!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.mAdmissionStream)) {
            customToastr('Select admission stream!', 'error');
        }
        else if (this.selectedCategory == null) {
            customToastr('Invalid class category!', 'error');
        }
        else if (this.selectedClassSectionId == null) {
            customToastr('Invalid class!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.mAdmissionDate)) {
            customToastr('Selet admission date!', 'error');
        }
        else {
            //customToastr('go----!','success');
            this.nextTabStatus1 = true;
        }
        /*console.log("Student name: ",this.mStudentName);
         console.log("Student sur name: ",this.mSurName);
         console.log("Student gender: ",this.mGenderGroup);
         console.log("STudent DOB: ",$("#admission-dob").val());
         console.log("Blood group: ",this.mBloodGroup);
         console.log("Relign: ",this.mReligion);
         console.log("Caste: ",this.mCast);
         console.log("Nationality: ",this.mNationality);
         console.log("St email: ",this.mStudentEmail);
         console.log("St contact: ",this.mStudenNumber);
    
         console.log("Academic year",this.selectedAcademicYear);
         console.log("Academic base",this.admissionBase);
         console.log("Academic stream",this.mAdmissionStream);
         console.log("Class cat",this.selectedCategory);
         console.log("Class section",this.selectedClassSection);
         console.log("Adm num",this.mAdmissionNumber);
         console.log("Adm date",$("#admission-date").val());*/
    };
    //tab 2 validate
    HeaderComponent.prototype.validateTab2 = function () {
        if (this.mFatherName == undefined || !this.formValidator.validateIsData(this.mFatherName)) {
            customToastr('Father name is missing!', 'error');
        }
        else if (this.mFatherNumber == undefined || !this.formValidator.validateIsData(this.mFatherNumber)) {
            customToastr('Enter father contact number!', 'error');
        }
        else if (this.mFatherAddress == undefined || !this.formValidator.validateIsData(this.mFatherAddress)) {
            customToastr('Father address is missing!', 'error');
        }
        else if (this.mMotherName == undefined || !this.formValidator.validateIsData(this.mMotherName)) {
            customToastr('Mother name is missing!', 'error');
        }
        else if (this.mMotherNumber == undefined || !this.formValidator.validateIsData(this.mMotherNumber)) {
            customToastr('Enter mother contact number!', 'error');
        }
        else if (this.mMotherSameFatherAddress != true && this.mMotherAddress == undefined || !this.formValidator.validateIsData(this.mMotherAddress)) {
            customToastr('Mother address is missing!', 'error');
        }
        else {
            //customToastr('go----!','success');
            this.nextTabStatus2 = true;
        }
        /*console.log("--",this.mFatherName);
         console.log("--",this.mFatherNumber);
         console.log(this.mFatherOccupation);
         console.log(this.mFatherQualification);
         console.log("--",this.mFatherAddress);
    
         console.log("--",this.mMotherName);
         console.log("--",this.mMotherNumber);
         console.log(this.mMotherOccupation);
         console.log(this.mMotherQualification);
         console.log("--",this.mMotherSameFatherAddress);
         console.log("--",this.mMotherAddress);
    
         console.log(this.mGuardianName);
         console.log(this.mGuardianNumber);
         console.log(this.mGuardianOccupation);
         console.log(this.mGuardianQualification);
         console.log(this.mGuardianSameFatherAddress);
         console.log(this.mGuardianAddress);*/
    };
    HeaderComponent.prototype.finishAdmissionProcess1 = function () {
        //customToastr('Admission Success!','success');
        console.log("admission processing");
        this.mDOB = $("#admission-dob").val();
        this.mAdmissionDate = $("#admission-date").val();
        //customToastr('Updated','success');
        if (!this.formValidator.validateIsData(this.mStudentName)) {
            customToastr('Invalid student name!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.mGenderGroup)) {
            customToastr('Invalid student gender!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.mDOB)) {
            customToastr('Select student DOB!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.mBloodGroup)) {
            customToastr('Blood group is not valid!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.mNationality)) {
            customToastr('Select student nationality!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.selectedAcademicYear)) {
            customToastr('Invalid academic year!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.admissionBase)) {
            customToastr('Select admission base!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.mAdmissionStream)) {
            customToastr('Select admission stream!', 'error');
        }
        else if (this.selectedCategory == null) {
            customToastr('Invalid class category!', 'error');
        }
        else if (this.selectedClassSectionId == null) {
            customToastr('Invalid class!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.mAdmissionDate)) {
            customToastr('Selet admission date!', 'error');
        }
        else if (this.mFatherName == undefined || !this.formValidator.validateIsData(this.mFatherName)) {
            customToastr('Father name is missing!', 'error');
        }
        else if (this.mFatherNumber == undefined || !this.formValidator.validateIsData(this.mFatherNumber)) {
            customToastr('Enter father contact number!', 'error');
        }
        else if (this.mFatherAddress == undefined || !this.formValidator.validateIsData(this.mFatherAddress)) {
            customToastr('Father address is missing!', 'error');
        }
        else if (this.mMotherName == undefined || !this.formValidator.validateIsData(this.mMotherName)) {
            customToastr('Mother name is missing!', 'error');
        }
        else if (this.mMotherNumber == undefined || !this.formValidator.validateIsData(this.mMotherNumber)) {
            customToastr('Enter mother contact number!', 'error');
        }
        else if (this.mMotherAddress == undefined || !this.formValidator.validateIsData(this.mMotherAddress)) {
            customToastr('Mother address is missing!', 'error');
        }
        else {
            this.finishedAdmission();
        }
    };
    HeaderComponent.prototype.mMotherSameFatherAddressClick = function () {
        console.log("mother father address: ", this.mMotherSameFatherAddress);
        if (this.mMotherSameFatherAddress == true) {
            this.mMotherAddress = this.mFatherAddress;
        }
        else {
            this.mMotherAddress = "";
        }
    };
    HeaderComponent.prototype.mGuardianSameFatherAddressClick = function () {
        if (this.mGuardianSameFatherAddress == true) {
            this.mGuardianAddress = this.mFatherAddress;
        }
        else {
            this.mGuardianAddress = "";
        }
    };
    HeaderComponent.prototype.ngOnInit = function () {
        this.isValidToken();
        this.getSchoolInfo();
        $(".after").append("<script>function customToastr(message,type){toastr.options.hideDuration = 0;toastr.clear();toastr.options.closeButton = true;toastr.options.progressBar = false;toastr.options.debug = false;toastr.options.positionClass = 'toast-top-right';toastr.options.showDuration = 330;toastr.options.hideDuration = 330;toastr.options.timeOut = 5000;toastr.options.extendedTimeOut = 1000;toastr.options.showEasing = 'swing';toastr.options.hideEasing = 'swing';toastr.options.showMethod = 'slideDown';toastr.options.hideMethod = 'slideUp';toastr[type](message, '');return 0;} </" + "script>");
    };
    ;
    HeaderComponent.prototype.logout = function () {
        localStorage.setItem('usertoken', null);
        localStorage.setItem('schoolinfo', null);
        this.router.navigate(['/login']);
        console.log("Logout");
    };
    HeaderComponent.prototype.getCurrentAcademic = function (schoolId) {
        var _this = this;
        this.groupservice.groupsStudentAcademicList(schoolId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            for (var i = 0; i < data.academicList.length; i++) {
                //this.schoolAcademicList.push(data.academicList[i]);
                if (data.academicList[i].InActive == 0) {
                    _this.academicId = data.academicList[i].pkAcademicId;
                    _this.schoolAcademicCurrentYear = data.academicList[i].Year;
                }
            }
            console.log("School  Academic Current id: ", _this.academicId);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    HeaderComponent.prototype.getAllBusRoutes = function (schoolId) {
        var _this = this;
        this.transportservice.getAllBuses(schoolId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Bus Routes", data);
            _this.busRouteList = data;
        }, function (error) { return console.log(error); }, function () {
        });
    };
    HeaderComponent.prototype.getStopsOfRoute = function (schoolId, classId) {
        var _this = this;
        this.transportservice.getstopsOfRoute(schoolId, classId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Bus Stops", data);
            _this.busStopList = data;
        }, function (error) { return console.log(error); }, function () {
        });
    };
    HeaderComponent.prototype.getAllAcademicYears = function () {
        //particle back ground implement
        //$('.particles-bg').particleground({dotColor: '#dadada',lineColor: '#c4c4c4',lineWidth: .3});
        //crop me
        var _this = this;
        this.admissionservice.getAllAcademicYears(this.currentSchoolId, localStorage.getItem('usertoken'))
            .subscribe(
        // data => this.schoolMatrices  = data,
        function (data) {
            console.log("Fetch Academic years : ", data);
            if (data != null && data != undefined && data.length > 0) {
                _this.academicYearsList = data;
                _this.selectedAcademicYearObj = data[0];
            }
        }, function (error) { return console.log(error); });
    };
    HeaderComponent.prototype.getAllCategories = function () {
        var _this = this;
        this.admissionservice.getAllCategories(this.currentSchoolId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Fetch Categories : ", data);
            _this.categoryList = data; //
        }, function (error) { return console.log(error); });
    };
    HeaderComponent.prototype.fetchCategoryDetails = function (categoryId) {
        var _this = this;
        this.admissionservice.getCategoryDetailsFromCategoryId(this.currentSchoolId, categoryId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Fetch Category Details : ", data);
            if (data != undefined && data != null && data.className != undefined && data.className.length > 0) {
                _this.classSectionList = data.className;
            }
            console.log("Fetch Category Details class section list : ", _this.classSectionList);
        }, function (error) { return console.log(error); });
    };
    HeaderComponent.prototype.fetchClassSectionOnClassId = function (classId) {
        var _this = this;
        this.admissionservice.getClassDetailsFromClassId(classId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Fetch class section Details : ", data);
            if (data != undefined && data != null && data != undefined && data.length > 0) {
                _this.classSections = data;
            }
        }, function (error) { return console.log(error); });
    };
    HeaderComponent.prototype.fetchFeeComponentOfClass = function (classId) {
        var _this = this;
        this.feesComponentsList = [];
        this.admissionservice.getFeeComponentsOfClass(this.currentSchoolId, this.academicId, classId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Fetch Fee Component Details : ", data);
            if (data != null && data != undefined && data.length > 0) {
                _this.feesComponentsList = data;
                _this.calculateTotalFees();
            }
        }, function (error) { return console.log(error); });
    };
    HeaderComponent.prototype.showAddAmount = function (feesTypeId, position) {
        console.log("component ", feesTypeId);
        $(".showaddamount_" + feesTypeId).hide();
        $(".showapplyamount_" + feesTypeId).show();
    };
    HeaderComponent.prototype.applyAmount = function (feesTypeId, position) {
        console.log("apply_", feesTypeId + "__" + position);
        $(".showaddamount_" + feesTypeId).show();
        $(".showapplyamount_" + feesTypeId).hide();
        var amount = $(".feesamount_" + feesTypeId).val();
        console.log("Amount: " + amount + " _ ");
        if (amount != null && amount != undefined && amount != "") {
            try {
                amount = parseInt(amount);
                var feesData = this.feesComponentsList;
                feesData[position].Amount = amount;
                this.feesComponentsList = feesData;
                this.calculateTotalFees();
                this.applyFeesAmountForTerm(feesTypeId, amount, position);
            }
            catch (err) {
                console.log(err);
            }
        }
    };
    HeaderComponent.prototype.applyFeesAmountForTerm = function (feesTypeId, amount, position) {
        var _this = this;
        this.admissionservice.applyFeesAmountForTerm(this.currentSchoolId, this.academicId, this.selectedClassSection, feesTypeId, amount, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Fetch Fee Component Details : ", data);
            if (data != undefined && data != null && data.status != undefined) {
                if (data.status == false) {
                    delete _this.feesComponentsList[position].Amount;
                }
                else {
                    console.log("Applied");
                }
            }
            else {
                delete _this.feesComponentsList[position].Amount;
            }
            _this.calculateTotalFees();
            //this.fetchFeeComponentOfClass(this.selectedClassSection);
        }, function (error) {
            console.log("ERR");
            console.log(error);
            delete _this.feesComponentsList[position].Amount;
            _this.calculateTotalFees();
        });
    };
    HeaderComponent.prototype.addFeesComponent = function () {
        var _this = this;
        var selectedAssignee = $('#fees-comp-assigned').find(":selected").text();
        console.log(this.mFeesComponentName);
        console.log(this.mFeesComponentAmount);
        this.mFeesComponentDue = $("#fees-comp-due").val();
        console.log($("#fees-comp-due").val());
        console.log(selectedAssignee);
        var feesObj = {
            Name: this.mFeesComponentName,
            includeInTerm: 0,
            academicId: this.academicId,
            AppliesTo: selectedAssignee,
            duedate: this.mFeesComponentDue,
            amount: this.mFeesComponentAmount,
            classId: this.selectedClassSection,
            schoolId: this.currentSchoolId
        };
        var updateInfo = {
            Name: feesObj.Name,
            Amount: this.mFeesComponentAmount,
            pkFeesTypeId: -100
        };
        this.feesComponentsList.push(updateInfo);
        this.admissionservice.createFeesComponent(feesObj, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Fetch Fee Component Details : ", data);
            //this.fetchFeeComponentOfClass(this.selectedClassSection);
            if (data.status != undefined && data.status == true) {
                _this.feesComponentsList[_this.feesComponentsList.length - 1].pkFeesTypeId = data.data.pkFeesTypeId;
            }
        }, function (error) {
            console.log("ERR");
            console.log(error);
        });
        $("#newcomponent .close").click();
    };
    HeaderComponent.prototype.calculateTotalFees = function () {
        this.feesAmount = 0;
        for (var i = 0; i < this.feesComponentsList.length; i++) {
            if (this.feesComponentsList[i].Amount != undefined) {
                this.feesAmount += this.feesComponentsList[i].Amount;
            }
        }
        this.mTotalFees = "Rs. " + this.feesAmount;
    };
    HeaderComponent.prototype.iterateFareOfBus = function (stopNumber) {
        for (var i = 0; i < this.busStopList.length; i++) {
            if (this.busStopList[i].StopNumber == stopNumber) {
                this.mTotalBusFees = "Rs " + this.busStopList[i].fare;
                break;
            }
        }
    };
    HeaderComponent.prototype.iterateBusId = function (routeId) {
        for (var i = 0; i < this.busRouteList.length; i++) {
            if (this.busRouteList[i].pkRouteId == routeId) {
                this.mBusId = this.busRouteList[i].pkBusId;
                break;
            }
        }
    };
    HeaderComponent.prototype.finishedAdmission = function () {
        console.log("Finish admission process started");
        this.mMotherOccupation = $('#admission-mother-occupation').find(":selected").text();
        this.mAdmissionBase = $("#admission-base").find(":selected").text();
        this.mAdmissionStream = $("#admission-stream").find(":selected").text();
        this.mDOB = $("#admission-dob").val();
        this.mAdmissionDate = $("#admission-date").val();
        this.mGuardianOccupation = $("#admission-guardian-occupation").find(":selected").text();
        this.mFatherOccupation = $("#admission-father-occupation").val();
        this.mBloodGroup = $("#admission-blood-group").find(":selected").text();
        this.mReligion = $("#admission-religion").find(":selected").text();
        var admissionObj = {
            schoolId: this.currentSchoolId,
            registrationId: this.mAdmissionNumber,
            studentName: this.mStudentName,
            classId: this.selectedClassSectionId,
            parentName: this.mFatherName,
            studentEmail: this.mStudentEmail,
            emailID: this.mFatherEmail,
            dateOfBirth: this.mDOB,
            mobileNumber: this.mFatherNumber,
            dateOfAdmission: this.mAdmissionDate,
            address: this.mFatherAddress,
            fkAcademicId: this.selectedAcademicYearObj,
            academicId: this.selectedAcademicYearObj,
            surname: this.mSurName,
            bloodGroup: this.mBloodGroup,
            religion: this.mReligion,
            cast: this.mCast,
            nationality: this.mNationality,
            studentPhone: this.mStudenNumber,
            fatherOccupation: this.mFatherOccupation,
            fatherQualification: this.mFatherQualification,
            fatherEmail: this.mFatherEmail,
            motherName: this.mMotherName,
            motherPhone: this.mMotherNumber,
            motherOccupation: this.mMotherOccupation,
            motherQualification: this.mMotherQualification,
            motherEmail: this.mMotherEmail,
            motherAddress: this.mMotherAddress,
            guardianName: this.mGuardianName,
            guardianPhone: this.mGuardianNumber,
            guardianOccupation: this.mGuardianOccupation,
            guardianQualification: this.mGuardianQualification,
            guardianEmail: this.mGuardianEmail,
            gender: this.mGenderGroup,
            admissionBase: this.mAdmissionBase,
            admissionStream: this.mAdmissionStream,
            studentImage: "/image/placeholder.png",
            stopId: this.selectedBusStop,
            busId: this.mBusId
        };
        var imageData = $(".cropme-admission img").attr('src');
        console.log(imageData);
        console.log(admissionObj);
        //TODO: Show progress
        if (imageData != this.admissionStudentImage) {
            this.uploadNewAdmissionImage(imageData, function (imageName) {
                admissionObj.studentImage = imageName;
                this.callNewAdmissionService(admissionObj);
                console.log("Image uploading....................");
            });
        }
        else {
            this.callNewAdmissionService(admissionObj);
        }
    };
    HeaderComponent.prototype.callNewAdmissionService = function (admissionObj) {
        console.log("Finishing admission process..........");
        this.admissionservice.createNewAdmission(admissionObj, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            //callback(data);
            //TODO: Check Status & show status message
            console.log("Finish status: ", data.status);
            if (data.status == true) {
                customToastr('New admission success!!!', 'success');
                $('.close').click();
            }
            else {
                customToastr('Something went wrong please try again!!!', 'error');
            }
        }, function (error) {
            //console.log(error)
            //callback(false)
            //TODO: Show error message
            console.log("Error status: ", error);
        });
    };
    HeaderComponent.prototype.uploadNewAdmissionImage = function (imageData, callback) {
        this.admissionservice.uploadImage(imageData, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("FIMAGE UPLAOD : ", data);
            callback(data);
        }, function (error) {
            console.log(error);
            callback(false);
        });
    };
    HeaderComponent.prototype.updateSelectedAcademic = function (academicId) {
        this.selectedAcademicYearObj = academicId;
        console.log(this.selectedAcademicYearObj);
    };
    HeaderComponent.prototype.updateSelectedCategory = function (categoryId) {
        this.classSectionList = null;
        this.classSections = null;
        this.selectedCategory = categoryId;
        console.log(this.selectedCategory);
        this.fetchCategoryDetails(this.selectedCategory);
    };
    HeaderComponent.prototype.updateSelectedClassSection = function (classId) {
        this.selectedClassSection = classId;
        console.log(this.selectedClassSection);
        this.fetchClassSectionOnClassId(classId);
        this.fetchFeeComponentOfClass(this.selectedClassSection);
    };
    HeaderComponent.prototype.updateSelectedClassSectionId = function (classId) {
        this.selectedClassSectionId = classId;
    };
    HeaderComponent.prototype.updateSelectedBusRoute = function (routeId) {
        this.selectedBusRoute = routeId;
        this.iterateBusId(routeId);
        this.getStopsOfRoute(this.currentSchoolId, this.selectedBusRoute);
    };
    HeaderComponent.prototype.updateSelectedBusStop = function (stopNumber) {
        this.selectedBusStop = stopNumber;
        this.iterateFareOfBus(stopNumber);
    };
    HeaderComponent = __decorate([
        Component({
            selector: 'app-header',
            templateUrl: './header.component.html',
            styleUrls: ['./header.component.css'],
            providers: [LoginService, HeaderService, AdmissionService, GroupService, LoginService, TransportService, FormValidator]
        }), 
        __metadata('design:paramtypes', [Router, LoginService, HeaderService, AdmissionService, FormValidator, TransportService, GroupService])
    ], HeaderComponent);
    return HeaderComponent;
}());
//# sourceMappingURL=F:/BW_PR/src/app/header/header.component.js.map