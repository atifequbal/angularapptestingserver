var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewEncapsulation } from '@angular/core';
import { AcademicsService } from "./academics.service";
export var AcademicsComponent = (function () {
    function AcademicsComponent(academicService) {
        this.academicService = academicService;
        this.academicList = [];
        this.classStandards = [];
        this.classSections = [];
        this.students = [];
        this.nextClassOfClass = [];
        this.studentPromotionList = [];
        this.loaded = false;
        this.selectedAcademic = { pkAcademicId: null, totalStudents: 0, promotedStudents: 0, newStudents: 0 };
        this.studentDataTable = null;
        this.academicsListTable = null;
        this.currentAcademic = { pkAcademicId: '' };
        this.nextAcademic = null;
        this.studentPromotionListTable = null;
        this.fromPromoteStudent = false;
        this.statusMessage = "Please wait academic transition is in progress";
        this.generatePromotionListMessage = "Please wait we are generating student promotion list for review";
        this.academicTransitionResults = { Year: ".......", totalClasses: ".......", totalStaff: "......", totalStudents: "......" };
        this.academicPermissions = { ADD_NEW_ACADEMIC: "DISABLED", PROMOTE_STUDENT: "DISABLED" };
    }
    AcademicsComponent.prototype.goOut = function (ac) {
        $("#open_view-academics_offcanvas").trigger("click");
        this.selectedAcademic = ac;
        this.getClassStandardsOfSchool();
        this.applyDataTable();
        if (this.studentDataTable != null) {
            this.studentDataTable.clear().draw();
        }
    };
    AcademicsComponent.prototype.fetchSections = function () {
        var id = $("#selectStandard").val();
        if (id != '') {
            $("#viewStudentsButton").removeClass("disabled");
            this.getClassSections(id);
        }
        else {
            $("#viewStudentsButton").addClass("disabled");
        }
    };
    AcademicsComponent.prototype.viewStudents = function () {
        var selectStandard = $("#selectStandard").val();
        var selectSection = $("#selectSection").val();
        var selectStatus = $("#selectStatus").val();
        if (selectStandard != '' || selectSection != '') {
            this.getStudents(selectStandard, selectSection, selectStatus, this.selectedAcademic.pkAcademicId);
        }
    };
    AcademicsComponent.prototype.ngOnInit = function () {
        var adminPermissionlist = localStorage.getItem('adminPermissionlist');
        if (adminPermissionlist) {
            var list = JSON.parse(adminPermissionlist);
            this.academicPermissions.ADD_NEW_ACADEMIC = list.ADD_NEW_ACADEMIC;
            this.academicPermissions.PROMOTE_STUDENT = list.PROMOTE_STUDENT;
        }
        $(".after").empty();
        $(".after").append("<script>function customToastr(message,type){toastr.options.hideDuration = 0;toastr.clear();toastr.options.closeButton = true;toastr.options.progressBar = false;toastr.options.debug = false;toastr.options.positionClass = 'toast-top-right';toastr.options.showDuration = 330;toastr.options.hideDuration = 330;toastr.options.timeOut = 5000;toastr.options.extendedTimeOut = 1000;toastr.options.showEasing = 'swing';toastr.options.hideEasing = 'swing';toastr.options.showMethod = 'slideDown';toastr.options.hideMethod = 'slideUp';toastr[type](message, '');return 0;}</" + "script>");
        $("#device-breakpoints").remove();
        $("#viewStudentsButton").addClass("disabled");
        this.getAcademicList();
        $(document).ready(function () {
            $('.viewstudent').attr('href', '#offcanvas-view-student-feedetails2');
        });
    };
    AcademicsComponent.prototype.getAcademicList = function () {
        var _this = this;
        this.academicService.fetchAcademicList()
            .subscribe(
        // data => this.schoolMatrices  = data,
        function (data) {
            if (data.errorcode) {
            }
            else {
                _this.academicList = data.academicList;
                if (_this.academicList.length == 0) {
                    $(".dataTables_empty").removeClass('hidden');
                }
                else {
                    $(".dataTables_empty").addClass('hidden');
                }
                _this.academicList.forEach(function (item) {
                    item.current = false;
                    item.notstarted = false;
                    item.completed = false;
                    if (item.InActive == 0) {
                        item.current = true;
                        $("#currentAcYear").text(item.Year);
                        $("#currentAcStudents").text(item.totalStudents);
                        $("#currentAcPromotedStudents").text(item.promotedStudents);
                        $("#currentAcNewStudents").text(item.newStudents);
                    }
                    else if (item.InActive == 1) {
                        var sDate = new Date(item.StartDate);
                        var eDate = new Date(item.EndDate);
                        if (eDate < new Date()) {
                            item.completed = true;
                        }
                        else {
                            item.notstarted = true;
                        }
                    }
                });
            }
            if (!_this.loaded) {
                _this.loaded = true;
                var h = _this;
                setTimeout(function () {
                    h.applyAcademicListTable();
                }, 100);
            }
            else {
                h.applyAcademicListTable();
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    AcademicsComponent.prototype.getClassStandardsOfSchool = function () {
        var _this = this;
        this.classSections = [];
        this.academicService.getAvailableClass().subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                _this.classStandards = data.schoolStandards;
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    AcademicsComponent.prototype.getClassSections = function (id) {
        var _this = this;
        this.academicService.getClassSections(id).subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                _this.classSections = data[0];
            }
        }, function (error) { return console.log("error occured", error); }, function () {
        });
    };
    AcademicsComponent.prototype.getStudents = function (standardClassId, sectionId, status, academicId) {
        var _this = this;
        this.academicService.getClassStudents(standardClassId, sectionId, status, academicId)
            .subscribe(function (data) {
            if (data.errorcode) {
                customToastr("Failed to fetch students. Please try again", 'error');
            }
            else {
                _this.students = data;
                var rows = [];
                _this.students.forEach(function (stu, index) {
                    var dateText = "";
                    var active = "Active";
                    if (stu.DateOfBirth && stu.DateOfBirth != "null" && stu.DateOfBirth != "0000-00-00") {
                        var date = new Date(stu.DateOfBirth);
                        dateText = "" + date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear();
                    }
                    if (!stu.isAppUser) {
                        active = '<p style="background: #dadada;color: white;display: inline-flex;padding: 0px 15px;text-transform: uppercase;border-radius: 20px;font-size: 10px;">Active</p>';
                    }
                    else {
                        active = '<p style="background: #9E9E9E;color: white;display: inline-flex;padding: 0px 15px;text-transform: uppercase;border-radius: 20px;font-size: 10px;">Active</p>';
                    }
                    rows.push([index + 1, stu.studentName, stu.parentName, stu.mobileNumber, dateText, active]);
                });
                _this.studentDataTable.clear().draw();
                _this.studentDataTable.rows.add(rows).draw();
            }
        }, function (error) { return console.log("error occured", error); }, function () {
        });
    };
    AcademicsComponent.prototype.applyAcademicListTable = function () {
        if (this.academicsListTable != null) {
            this.academicsListTable.destroy();
        }
        this.academicsListTable = $('#academics_list_table').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columns",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": '_MENU_ entries per page',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "oLanguage": {
                "sEmptyTable": "There are no Academics"
            }
        });
    };
    AcademicsComponent.prototype.applyDataTable = function () {
        if (this.studentDataTable == null) {
            this.studentDataTable = $('#datatableSlideStudents').DataTable({
                "dom": 'lCfrtip',
                "order": [],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "language": {
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                },
                "oLanguage": {
                    "sEmptyTable": "There are no Students"
                }
            });
        }
    };
    AcademicsComponent.prototype.createAcademic = function () {
        var _this = this;
        var sta = $("#startDate_add").val();
        var end = $("#endDate_add").val();
        var isCurentAcademic = $("#isCurentAcademic").prop('checked');
        sta = sta.trim();
        end = end.trim();
        if (sta != '' && end != '') {
            var sArr = sta.split("/");
            var eArr = end.split("/");
            isCurentAcademic = isCurentAcademic ? 0 : 1;
            var s = sArr[2] + "-" + sArr[1] + "-" + sArr[0];
            var e = eArr[2] + "-" + eArr[1] + "-" + eArr[0];
            if (this.fromPromoteStudent) {
                isCurentAcademic = 1;
            }
            this.academicService.createAcademic(s, e, isCurentAcademic)
                .subscribe(function (data) {
                if (data.errorcode) {
                    customToastr("Create academic failed Please try again", 'error');
                }
                else {
                    if (!data.status) {
                        customToastr("Academic already exist", "error");
                    }
                    else {
                        $("#close_add_academic_popup").trigger("click");
                        _this.loaded = false;
                        _this.getAcademicList();
                        customToastr("Academic successfully created", "success");
                        if (_this.fromPromoteStudent) {
                            _this.promoteStudentsFirstStep();
                        }
                    }
                }
            }, function (error) { return console.log("error occured", error); }, function () {
            });
        }
        else {
            customToastr("Please provide start and end dates", "error");
        }
    };
    /* promote students */
    AcademicsComponent.prototype.promoteStudentsFirstStep = function () {
        var _this = this;
        $("#promote_student_next").addClass("disabled");
        $("#promote_student_finish").addClass("hide");
        $("#rootwizard1").find(".progress").css({ width: '80%' });
        $("#rootwizard1").find(".progress-bar").css({ width: '0%' });
        var step = $("#promote_students_wizard").find(".active").find(".step").text();
        if (step == 1) {
            $("#rootwizard1").find(".progress-bar").css({ width: '0%' });
        }
        else if (step == 2) {
            $("#rootwizard1").find(".progress-bar").css({ width: '35%' });
        }
        else if (step == 3) {
            $("#rootwizard1").find(".progress-bar").css({ width: '65%' });
        }
        else if (step == 4) {
            $("#rootwizard1").find(".progress-bar").css({ width: '80%' });
        }
        this.academicService.getCurrentAndNextAcademic()
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                _this.currentAcademic = data.academic;
                _this.nextAcademic = data.nextAcademic;
                if (!_this.nextAcademic) {
                    $("#promote_student_next").addClass("disabled");
                }
                else {
                    $("#promote_student_next").removeClass("disabled");
                }
            }
        }, function (error) { return console.log("error occured", error); }, function () {
        });
    };
    AcademicsComponent.prototype.openCreateAcademic = function () {
        $('#create_new_academic').trigger('click');
    };
    AcademicsComponent.prototype.openCreateAcademicsPopup = function (fromPromoteStudent) {
        $("#startDate_add").datepicker({ autoclose: true });
        $("#endDate_add").datepicker({ autoclose: true });
        this.fromPromoteStudent = fromPromoteStudent;
    };
    AcademicsComponent.prototype.promoteStudentNext = function () {
        var step = $("#promote_students_wizard").find(".active").find(".step").text();
        if (step == 1) {
            $("#rootwizard1").find(".progress-bar").css({ width: '35%' });
            $("#rootwizard1").find(".active").addClass('done');
            $("#rootwizard1").find(".active").removeClass('active');
            $("#rootwizard1").find("#step2").trigger('click');
            $("#promote_student_next").find('a').text("Process Transition");
            customToastr("Fetching class list. Pls wait...", 'success');
            this.getNextClassOfClass();
            this.applyPromoteStudentsTable();
        }
        else if (step == 2) {
            $("#rootwizard1").find(".progress-bar").css({ width: '65%' });
            $("#rootwizard1").find(".active").addClass('done');
            $("#rootwizard1").find(".active").removeClass('active');
            $("#rootwizard1").find("#step6").trigger('click');
        }
        else if (step == 3) {
            $("#rootwizard1").find(".progress-bar").css({ width: '80%' });
            $("#rootwizard1").find(".active").addClass('done');
            $("#rootwizard1").find(".active").removeClass('active');
            $("#rootwizard1").find("#step7").trigger('click');
            this.processAcademicPromotion();
        }
        else if (step == 4) {
            $(".modal-backdrop").model('hide');
        }
    };
    AcademicsComponent.prototype.processAcademicPromotion = function () {
        var _this = this;
        $("#promote_student_finish").find('a').text("Finish");
        this.statusMessage = "Please wait academic transition is in progress ....";
        customToastr(this.statusMessage, 'success');
        $("#promote_student_prev").hide();
        $("#promote_student_next").hide();
        $("#promote_student_finish").addClass("show");
        $("#promote_student_finish").addClass("disabled");
        $("#loading_academic_transition").addClass("show");
        $("#loading_academic_transition").removeClass("hide");
        var obj = {
            currentAcademic: this.currentAcademic.pkAcademicId,
            nextAcademic: this.nextAcademic.pkAcademicId,
            reason: 'Academic completed',
            classTimetableClearInfo: [],
            feeStructureRemain: false
        };
        this.academicService.processPromoteStudent(obj)
            .subscribe(function (data) {
            if (!data.status) {
                customToastr("Academic transition failed. Please try again", 'error');
                $("#promote_student_finish").removeClass("disabled");
                $("#promote_student_finish").find('a').text("Retry");
                $("#loading_academic_transition").addClass("hide");
                $("#loading_academic_transition").removeClass("show");
                _this.statusMessage = "Academic transition failed. Pls try again";
            }
            else {
                _this.statusMessage = "Generating Final Report ....";
                customToastr(_this.statusMessage, 'success');
                _this.getAcademicTransitionResult();
            }
        }, function (error) { return console.log("error occured", error); }, function () {
        });
    };
    AcademicsComponent.prototype.promoteStudentPrev = function () {
        var step = $("#promote_students_wizard").find(".active").find(".step").text();
        if (step == 2) {
            $("#promote_student_next").find("a").text("Next");
        }
    };
    AcademicsComponent.prototype.getNextClassOfClass = function () {
        var _this = this;
        $("#generating_promotion_list").addClass("show");
        $("#generating_promotion_list").removeClass("hide");
        this.generatePromotionListMessage = "Please wait we are generating student promotion list for review ....";
        customToastr(this.generatePromotionListMessage, 'success');
        this.academicService.getNextClassOfClass()
            .subscribe(function (data) {
            if (data.errorcode || !data.status) {
                $("#promote_student_next").hide();
                _this.generatePromotionListMessage = "Classes order is not proper";
                customToastr("Failed to generate promotion list, Classes order is not proper", 'error');
            }
            else {
                $("#generating_promotion_list").addClass("hide");
                $("#generating_promotion_list").removeClass("show");
                $("#promote_student_next").show();
                _this.nextClassOfClass = data.classes;
                var h = _this;
                setTimeout(function () {
                    $("#select_promoting_class").val(h.nextClassOfClass[0].fkClassId);
                    h.fetchStudentPromotionList();
                }, 100);
                customToastr("Please select a class to show promoting students list", 'success');
            }
        }, function (error) { return console.log("error occured", error); }, function () {
        });
    };
    AcademicsComponent.prototype.fetchStudentPromotionList = function () {
        var _this = this;
        var classId = $("#select_promoting_class").val();
        this.studentPromotionListTable.clear().draw();
        this.academicService.getPromotionListByClass(classId)
            .subscribe(function (data) {
            if (data.errorcode || !data.status) {
            }
            else {
                var select = "<select id='change_promote_class_of_student' name='change_promote_class_of_student'  style='text-decoration: none;color:grey;font-size:12px'>";
                _this.nextClassOfClass.forEach(function (it) {
                    var a = $("<option value='" + it.fkClassId + "'></option>");
                    a.text(it.classNumber + " - " + it.classDivision);
                    select += a[0].outerHTML;
                });
                select += "<option value='END'>END</option></select>";
                if (data.list && data.list.length > 0) {
                    var rows = [];
                    data = data.list;
                    for (var i = 0; i < data.length; i++) {
                        var html = $(select);
                        html.attr("info", JSON.stringify(data[i]));
                        html.val(data[i].toClassId);
                        rows.push([i + 1, data[i].studentName, data[i].parentName, data[i].fromClassNumber + " - " + data[i].fromClassDivision, html[0].outerHTML]);
                    }
                    _this.studentPromotionListTable.rows.add(rows).draw();
                }
                else {
                    _this.studentPromotionListTable.clear().draw();
                }
            }
        }, function (error) { return console.log("error occured", error); }, function () {
        });
    };
    AcademicsComponent.prototype.applyPromoteStudentsTable = function () {
        if (this.studentPromotionListTable == null) {
            this.studentPromotionListTable = $('#student_promotionList_table').DataTable({
                "dom": 'lCfrtip',
                "order": [],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "oLanguage": {
                    "sEmptyTable": "There are no students to show"
                },
                "rowCallback": function (row, data, index) {
                    var info = $(row).find("#change_promote_class_of_student").attr("info");
                    info = JSON.parse(info);
                    $(row).find("#change_promote_class_of_student").val(info.toClassId || 'END');
                },
                "language": {
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
            var h = this;
            $("#student_promotionList_table tbody").on("change", "#change_promote_class_of_student", function () {
                var info = $(this).val();
                var id = JSON.parse($(this).attr("info")).idSchoolNextAcademicPromotionList;
                if (info == "END") {
                    info = null;
                }
                h.updateStudentPromote(info, id);
            });
        }
    };
    AcademicsComponent.prototype.updateStudentPromote = function (toClassId, promoteId) {
        this.academicService.updateStudentPromotionList(toClassId, promoteId)
            .subscribe(function (data) {
            if (!data.status) {
                customToastr(data.message, 'error');
            }
            else {
                customToastr("Student promoting class successfully updated", 'success');
            }
        }, function (error) { return console.log("error occured", error); }, function () {
        });
    };
    AcademicsComponent.prototype.getAcademicTransitionResult = function () {
        var _this = this;
        this.academicService.promoteStudentResult()
            .subscribe(function (data) {
            if (!data.status) {
                customToastr("Failed to fetch academic transition results", 'error');
            }
            else {
                $("#loading_academic_transition").addClass("hide");
                $("#loading_academic_transition").removeClass("show");
                _this.statusMessage = "Final Report";
                _this.academicTransitionResults = data.info;
                $("#promote_student_finish").removeClass("disabled");
            }
        }, function (error) { return console.log("error occured", error); }, function () {
        });
    };
    AcademicsComponent.prototype.promoteStudentFinish = function () {
        if ($("#promote_student_finish").find('a').text() == "Retry") {
            this.processAcademicPromotion();
        }
        else {
            $(".modal-backdrop").trigger("click");
            this.loaded = false;
            this.getAcademicList();
        }
    };
    AcademicsComponent = __decorate([
        Component({
            selector: 'app-academics',
            templateUrl: './academics.component.html',
            styleUrls: [
                'assets/css/theme-default/bootstrap.css?1422792965',
                'assets/css/theme-default/materialadmin.css?1425466319',
                'assets/css/theme-default/font-awesome.min.css?1422529194',
                'assets/css/theme-default/material-design-iconic-font.min.css?1421434286',
                'assets/css/theme-default/libs/wizard/wizard.css?1425466601',
                'assets/css/theme-default/libs/toastr/toastr.css?1425466569',
                'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
                'assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
                './academics.component.css'],
            encapsulation: ViewEncapsulation.None,
            providers: [AcademicsService]
        }), 
        __metadata('design:paramtypes', [AcademicsService])
    ], AcademicsComponent);
    return AcademicsComponent;
}());
//# sourceMappingURL=F:/BW_PR/src/app/academics/academics.component.js.map