var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import 'rxjs/Rx';
import { AppSettings } from "../../config/config";
export var AcademicsService = (function () {
    function AcademicsService(http) {
        this.http = http;
        this._url = AppSettings.API_ENDPOINT;
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        //this.headers.append('authToken','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJJZCI6MjEsImFkbWluSWQiOjJ9.wAg5Xa7oI998zK_QRAvOg43cYL_UlNRWhHipCdUafu0');
        this.headers.append('authToken', localStorage.getItem('usertoken'));
    }
    //for fetching academics of school
    AcademicsService.prototype.fetchAcademicList = function () {
        var json = JSON.stringify({ schoolId: '20' });
        var params = json;
        return this.http.post(this._url + '/academiclist', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
        // .subscribe(
        //   data => this.getMatrices = JSON.stringify(data),
        //   error => alert(error),
        //   () => console.log("Post data response: ",this.getMatrices)
        // );
    };
    //for fetching metrics of school
    AcademicsService.prototype.fetchAcademicMetrics = function () {
        var json = JSON.stringify({ schoolId: '20' });
        var params = json;
        return this.http.post(this._url + '/academiclist', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
        // .subscribe(
        //   data => this.getMatrices = JSON.stringify(data),
        //   error => alert(error),
        //   () => console.log("Post data response: ",this.getMatrices)
        // );
    };
    //fetch available class
    AcademicsService.prototype.getAvailableClass = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/classManagement/listStandards', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    AcademicsService.prototype.getClassSections = function (standardClassId) {
        var json = JSON.stringify({ standardClassId: standardClassId });
        var params = json;
        return this.http.post(this._url + '/classManagement/classmanagement_standard_SectionList', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    AcademicsService.prototype.getClassStudents = function (standardClassId, sectionId, status, academicId) {
        var json = JSON.stringify({ classStandardId: standardClassId, classSectionId: sectionId, status: status, page: 1, academicId: academicId });
        var params = json;
        return this.http.post(this._url + '/studentlist/getStudentsListAcademicPage', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    AcademicsService.prototype.createAcademic = function (start, end, activeStatus) {
        var json = JSON.stringify({ startDate: start, endDate: end, inActive: activeStatus });
        var params = json;
        return this.http.post(this._url + '/academics/createAcademic', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    /*  Academic transition */
    AcademicsService.prototype.getCurrentAndNextAcademic = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/promote/academics/get/currentAndNext', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    AcademicsService.prototype.getNextClassOfClass = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/promote/academics/get/nextclassofclass', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    AcademicsService.prototype.getPromotionListByClass = function (classId) {
        var json = JSON.stringify({ classId: classId });
        var params = json;
        return this.http.post(this._url + '/promote/academics/promotionlist/classId/class', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    AcademicsService.prototype.updateStudentPromotionList = function (toClassId, promoteId) {
        var json = JSON.stringify({ toClassId: toClassId, idSchoolNextAcademicPromotionList: promoteId });
        var params = json;
        return this.http.post(this._url + '/promote/academics/update/promotionlist', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    AcademicsService.prototype.processPromoteStudent = function (reqObj) {
        var json = JSON.stringify(reqObj);
        var params = json;
        return this.http.post(this._url + '/promote/academics/process/nextacademic/school', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    AcademicsService.prototype.promoteStudentResult = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/promote/academics/process/result', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    AcademicsService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [Http])
    ], AcademicsService);
    return AcademicsService;
}());
//# sourceMappingURL=F:/BW_PR/src/app/academics/academics.service.js.map