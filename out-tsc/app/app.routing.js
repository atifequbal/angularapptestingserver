import { RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { GroupsComponent } from "./groups/groups.component";
import { AcademicsComponent } from "./academics/academics.component";
import { BroadcastComponent } from "./broadcast/broadcast.component";
import { ClassmanageComponent } from "./classmanage/classmanage.component";
import { FeesmanageComponent } from "./feesmanage/feesmanage.component";
import { TransportComponent } from "./transport/transport.component";
import { SettingsComponent } from "./settings/settings.component";
import { ReportComponent } from "./report/report.component";
import { PagenotfoundComponent } from "./pagenotfound/pagenotfound.component";
import { AdmissionComponent } from "./admission/admission.component";
var APP_ROUTES = [
    { path: 'login', component: LoginComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'admission', component: AdmissionComponent },
    { path: 'groups', component: GroupsComponent },
    { path: 'academics', component: AcademicsComponent },
    { path: 'broadcast', component: BroadcastComponent },
    { path: 'class', component: ClassmanageComponent },
    { path: 'fees', component: FeesmanageComponent },
    { path: 'transport', component: TransportComponent },
    { path: 'settings', component: SettingsComponent },
    { path: 'report', component: ReportComponent },
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: '**', component: PagenotfoundComponent }
];
//register the route
export var routing = RouterModule.forRoot(APP_ROUTES, { useHash: true }); //use the const routing in app module import
//# sourceMappingURL=F:/BW_PR/src/app/app.routing.js.map