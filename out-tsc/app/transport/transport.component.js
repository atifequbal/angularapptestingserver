var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import { LoginService } from "../login/login.service";
import { TransportService } from "./transport.service";
export var TransportComponent = (function () {
    function TransportComponent(router, loginservice, transportservice) {
        this.router = router;
        this.loginservice = loginservice;
        this.transportservice = transportservice;
        this.activeBus = 0;
        this.inActiveBus = 0;
        this.totalVehicles = 0;
        this.busList = [];
        this.activeBusList = [];
        this.inactiveBusList = [];
        this.busDetails = [];
        this.busRouteList = [];
        this.addRoutes = { RouteName: "", StartPoint: "", EndPoint: "", TotalStops: "" };
        this.busStopDetail = [];
        this.driverDetails = [];
        this.addBus = { BusNumber: "", BusName: "", Capacity: "", fkStaffId: "", DeviceId: "", fkRouteId: "", BusStatus: "" };
        this.myData = [];
        this.updateRoutes = { RouteName: "", StartPoint: "", EndPoint: "", TotalStop: "" };
        this.stopDetails = [];
        this.updateStops = { BusStopName: "", Busfare: "", Latitude: "", Longitude: "" };
        this.viewStop = [];
        this.assignRouteStop = [{ FkRouteId: "", FkStopId: "", stopNumber: "" }];
        this.getSchoolId = localStorage.getItem('schoolinfo');
        this.selectedRoutId = 0;
        this.selectedBusId = 0;
        this.addStop = { BusStopName: "", fare: "", Latitude: "", Longitude: "" };
        this.selectedStopId = 0;
        this.updateBus = { BusName: "", Capacity: "", fkStaffId: "", DeviceId: "", fkRouteId: "", BusStatus: "" };
        this.options = {
            "type": "serial",
            "theme": "light",
            "dataProvider": []
        };
    }
    TransportComponent.prototype.isValidToken = function () {
        var _this = this;
        var isValidToken = false;
        var isValidSchool = 0;
        this.loginservice.isValidToken(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            isValidToken = data.isValid;
            if (isValidToken == true) {
                _this.loginservice.isValidSchoolId(localStorage.getItem('schoolinfo'))
                    .subscribe(function (data) {
                    isValidSchool = data.schoolExist;
                    _this.getSchoolId = localStorage.getItem('schoolinfo');
                    if (isValidSchool != 1) {
                        _this.router.navigate(['/login']);
                    }
                    else {
                    }
                }, function (error) {
                    return function () { };
                });
            }
            else {
                _this.router.navigate(['/login']);
            }
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.getTransportCount = function () {
        var _this = this;
        this.transportservice.fetchTransportCount(localStorage.getItem('schoolinfo'), localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            _this.activeBus = data[0].ActiveBus.toLocaleString('en');
            _this.inActiveBus = data[0].InActiveBus.toLocaleString('en');
            _this.totalVehicles = data[0].TotalVehicles.toLocaleString('en');
            $(".after").append("<script>function customToastr(message,type){toastr.options.hideDuration = 0;toastr.clear();toastr.options.closeButton = true;toastr.options.progressBar = false;toastr.options.debug = false;toastr.options.positionClass = 'toast-top-right';toastr.options.showDuration = 330;toastr.options.hideDuration = 330;toastr.options.timeOut = 5000;toastr.options.extendedTimeOut = 1000;toastr.options.showEasing = 'swing';toastr.options.hideEasing = 'swing';toastr.options.showMethod = 'slideDown';toastr.options.hideMethod = 'slideUp';toastr[type](message, '');return 0;}</" + "script>");
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.destroyDataTable = function (table) {
        $('#' + table).DataTable().destroy();
    };
    TransportComponent.prototype.initializeDataTable = function (table) {
        setTimeout(function () {
            $('#' + table).DataTable({
                "order": [[0, 'asc']],
            });
            //this.scriptInit();
        }, 500);
    };
    TransportComponent.prototype.getAllBuses = function () {
        var _this = this;
        this.destroyDataTable('all-transport');
        this.transportservice.fetchAllBuses(localStorage.getItem('schoolinfo'), localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            for (var i = 0; i < data.length; i++) {
                _this.busList.push(data[i]);
                if (data.length - 1 == i) {
                    _this.initializeDataTable('all-transport');
                }
            }
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.getActiveBuses = function () {
        var _this = this;
        this.destroyDataTable('active-transport');
        this.transportservice.fetchActiveBuses(localStorage.getItem('schoolinfo'), localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            for (var i = 0; i < data.length; i++) {
                _this.activeBusList.push(data[i]);
                if (data.length - 1 == i) {
                    _this.initializeDataTable('active-transport');
                }
            }
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.getInActiveBuses = function () {
        var _this = this;
        this.destroyDataTable('inactive-transport');
        this.transportservice.fetchInActiveBuses(localStorage.getItem('schoolinfo'), localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            for (var i = 0; i < data.length; i++) {
                _this.inactiveBusList.push(data[i]);
                if (data.length - 1 == i) {
                    _this.initializeDataTable('inactive-transport');
                }
            }
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.updateBusPopup = function () {
        $('#update-bus').modal('show');
    };
    TransportComponent.prototype.deleteBusPopup = function () {
        $('#delete-bus').modal('show');
    };
    TransportComponent.prototype.busDetail = function (Iablist) {
        var _this = this;
        this.selectedBusId = Iablist.pkBusId;
        this.transportservice.fetchBusesDetail(localStorage.getItem('usertoken'), Iablist.pkBusId)
            .subscribe(function (data) {
            _this.busDetails = data[0];
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.getRouteList = function () {
        var _this = this;
        this.destroyDataTable('routes-table');
        this.transportservice.fetchBusesRoute(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            for (var i = 0; i < data.length; i++) {
                _this.busRouteList.push(data[i]);
                if (data.length - 1 == i) {
                    _this.initializeDataTable('routes-table');
                }
            }
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.getBusStop = function () {
        var _this = this;
        this.transportservice.fetchBusStop(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            _this.busStopDetail = data;
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.addBusRoute = function (addroutes) {
        var _this = this;
        $('#Route-Status').show();
        if (addroutes.RouteName != null && addroutes.RouteName != "" && addroutes.RouteName != undefined && addroutes.StartPoint != null && addroutes.StartPoint != "" && addroutes.StartPoint != undefined && addroutes.EndPoint != null && addroutes.EndPoint != "" && addroutes.EndPoint != undefined && addroutes.TotalStops != null && addroutes.TotalStops != "" && addroutes.TotalStops != undefined) {
            this.transportservice.insertBusRoute(this.addRoutes.RouteName, this.addRoutes.StartPoint, this.addRoutes.EndPoint, this.addRoutes.TotalStops, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                $('#Route-Status').hide();
                if (data.status == true) {
                    customToastr('Route is added successfully', 'success');
                    $("#addroutepopupclose").trigger("click");
                    _this.getRouteList();
                }
                else {
                    customToastr('Route is not added please try again', 'error');
                }
            }, function (error) {
                return function () {
                };
            });
        }
        else {
            $('#Route-Status').hide();
            customToastr('Please fill all the fields', 'error');
        }
    };
    TransportComponent.prototype.getDriverList = function () {
        var _this = this;
        this.transportservice.fetchDriverList(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            _this.driverDetails = data;
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.addBuses = function (addbus) {
        var _this = this;
        $('#bus-Status').show();
        if (addbus.BusNumber != null && addbus.BusNumber != "" && addbus.BusNumber != undefined && addbus.BusName != null && addbus.BusName != "" && addbus.BusName != undefined && addbus.Capacity != null && addbus.Capacity != "" && addbus.Capacity != undefined && addbus.fkStaffId != null && addbus.fkStaffId != "" && addbus.fkStaffId != undefined && addbus.DeviceId != null && addbus.DeviceId != "" && addbus.DeviceId != undefined && addbus.fkRouteId != null && addbus.fkRouteId != "" && addbus.fkRouteId != undefined && addbus.BusStatus != null && addbus.BusStatus != "" && addbus.BusStatus != undefined) {
            this.transportservice.addNewBus(this.addBus.BusNumber, this.addBus.BusName, this.addBus.Capacity, this.addBus.fkStaffId, this.addBus.DeviceId, this.addBus.fkRouteId, this.addBus.BusStatus, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                $('#bus-Status').hide();
                if (data.status == true) {
                    customToastr('Bus is added successfully', 'success');
                    $('.backdrop').trigger("click");
                    _this.getAllBuses();
                    _this.getTransportCount();
                    _this.getActiveBuses();
                    _this.getInActiveBuses();
                }
                else if (data.status == false) {
                    customToastr('Bus is already exist', 'error');
                }
                else {
                    customToastr('Bus is not added please try again', 'error');
                }
            }, function (error) {
                return function () {
                };
            });
        }
        else {
            $('#bus-Status').hide();
            customToastr('Please fill all the fields', 'error');
        }
    };
    TransportComponent.prototype.assignRoutePopup = function () {
        $('#assign_stop').modal('show');
    };
    TransportComponent.prototype.editRoutePopup = function () {
        $('#edit-routes').modal('show');
    };
    TransportComponent.prototype.deleteRoutePopup = function () {
        $('#delete-routes').modal('show');
    };
    TransportComponent.prototype.routeDetail = function (routeId) {
        var _this = this;
        this.selectedRoutId = routeId;
        this.transportservice.fetchRouteDetail(localStorage.getItem('usertoken'), routeId)
            .subscribe(function (data) {
            var h = _this;
            setTimeout(function () {
                h.myData = data[0];
            }, 100);
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.updateBusRoute = function (updateRoutes) {
        var _this = this;
        $('#updateRoute-Status').show();
        this.updateRoutes.RouteName = $('.route_Name').val();
        this.updateRoutes.StartPoint = $('.startpoint').val();
        this.updateRoutes.EndPoint = $('.endpoint').val();
        this.updateRoutes.TotalStop = $('.totalStop').val();
        if (updateRoutes.RouteName != null && updateRoutes.RouteName != "" && updateRoutes.RouteName != undefined && updateRoutes.StartPoint != null && updateRoutes.StartPoint != "" && updateRoutes.StartPoint != undefined && updateRoutes.EndPoint != null && updateRoutes.EndPoint != "" && updateRoutes.EndPoint != undefined && updateRoutes.TotalStop != null && updateRoutes.TotalStop != "" && updateRoutes.TotalStop != undefined) {
            this.transportservice.updateBusRoute(this.updateRoutes.RouteName, this.updateRoutes.StartPoint, this.updateRoutes.EndPoint, this.updateRoutes.TotalStop, this.selectedRoutId, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                $('#updateRoute-Status').hide();
                if (data.status == true) {
                    customToastr('Route is updated successfully', 'success');
                    $("#updateroutepopupclose").trigger("click");
                    _this.getRouteList();
                }
                else {
                    customToastr('Route is not updated please try again', 'error');
                }
            }, function (error) {
                return function () {
                };
            });
        }
        else {
            $('#updateRoute-Status').hide();
            customToastr('Please fill all fields', 'error');
        }
    };
    TransportComponent.prototype.deleteRouteDetail = function (routeId) {
        var _this = this;
        this.transportservice.deleteRouteDetail(localStorage.getItem('usertoken'), this.selectedRoutId)
            .subscribe(function (data) {
            if (data.status == true) {
                customToastr('Route Deleted', 'success');
                $("#closedeleteroutepopup").trigger("click");
                _this.getRouteList();
            }
            else if (data.status == false) {
                customToastr('Route is used in bus, cannot be deleted', 'error');
                $("#closedeleteroutepopup").trigger("click");
            }
            else {
                customToastr('Route not deleted please try again', 'error');
            }
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.addBusStop = function (addstops) {
        var _this = this;
        $('#addStop-Status').show();
        if (addstops.BusStopName != null && addstops.BusStopName != "" && addstops.BusStopName != undefined && addstops.fare != null && addstops.fare != "" && addstops.fare != undefined && addstops.Latitude != null && addstops.Latitude != "" && addstops.Latitude != undefined && addstops.Longitude != null && addstops.Longitude != "" && addstops.Longitude != undefined) {
            this.transportservice.addBusStop(this.addStop.BusStopName, this.addStop.fare, this.addStop.Latitude, this.addStop.Longitude, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                $('#addStop-Status').hide();
                if (data.status == true) {
                    customToastr('Stops is added successfully', 'success');
                    $("#addstopspopupclose").trigger("click");
                    _this.getBusStop();
                }
                else {
                    customToastr('Stops is not added please try again', 'error');
                }
            }, function (error) {
                return function () {
                };
            });
        }
        else {
            $('#addStop-Status').hide();
            customToastr('Please fill all the fields', 'error');
        }
    };
    TransportComponent.prototype.editStopPopup = function () {
        $('#edit-stops').modal('show');
    };
    TransportComponent.prototype.deleteStopPopup = function () {
        $('#delete-stops').modal('show');
    };
    TransportComponent.prototype.stopDetail = function (stopId) {
        var _this = this;
        this.selectedStopId = stopId;
        this.transportservice.fetchStopDetail(localStorage.getItem('usertoken'), stopId)
            .subscribe(function (data) {
            _this.stopDetails = data[0];
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.updateBusStop = function (updateStops) {
        var _this = this;
        $('#stops-Status').show();
        this.updateStops.BusStopName = $('.stop-name').val();
        this.updateStops.Busfare = $('.stop-fare').val();
        this.updateStops.Latitude = $('.stop-latit').val();
        this.updateStops.Longitude = $('.stop-long').val();
        if (updateStops.BusStopName != null && updateStops.BusStopName != "" && updateStops.BusStopName != undefined && updateStops.Busfare != null && updateStops.Busfare != "" && updateStops.Busfare != undefined && updateStops.Latitude != null && updateStops.Latitude != "" && updateStops.Latitude != undefined && updateStops.Longitude != null && updateStops.Longitude != "" && updateStops.Longitude != undefined) {
            this.transportservice.updateBusStop(this.updateStops.BusStopName, this.updateStops.Busfare, this.updateStops.Latitude, this.updateStops.Longitude, this.selectedStopId, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                $('#stops-Status').hide();
                if (data.status == true) {
                    customToastr('Stops is updated successfully', 'success');
                    $("#updateStopspopupclose").trigger("click");
                    _this.getBusStop();
                }
                else {
                    customToastr('Stops is not updated please try again', 'error');
                }
            }, function (error) {
                return function () {
                };
            });
        }
        else {
            $('#stops-Status').hide();
            customToastr('Please fill all the fields', 'error');
        }
    };
    TransportComponent.prototype.deleteStopDetail = function (stopId) {
        var _this = this;
        this.transportservice.deleteStopDetail(localStorage.getItem('usertoken'), this.selectedStopId)
            .subscribe(function (data) {
            if (data.status == true) {
                customToastr('Stop Deleted', 'success');
                $("#closedeletestoppopup").trigger("click");
                _this.getBusStop();
            }
            else if (data.status == false) {
                customToastr('Stop is used in Route, Cannot be delete', 'error');
                $("#closedeletestoppopup").trigger("click");
            }
            else {
                customToastr('Stop not deleted please try again', 'error');
            }
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.viewStopForRoute = function (routeId) {
        var _this = this;
        $('#view-stop').modal('show');
        this.transportservice.fetchStopForRoute(localStorage.getItem('usertoken'), routeId)
            .subscribe(function (data) {
            _this.viewStop = data;
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.assignStopForRoute = function (assignroutestop) {
        $('#stopsAssign-Status').show();
        var reqObj = [];
        var valid = true;
        var stops = $('#addanotherStopParent').find(".roots_for_bus").map(function (index, ele) {
            var stopId = $(ele).find("#stoplist").val();
            var stopNumber = $(ele).find("#stopnumber").val();
            if (stopId && stopId != "" && stopNumber && stopNumber != "") {
                reqObj.push({ stopId: stopId, stopNumber: stopNumber });
            }
            else {
                valid = false;
            }
        });
        if (valid) {
            if (reqObj.length > 0) {
                this.transportservice.assignStopForRoute(reqObj, this.selectedRoutId, localStorage.getItem('usertoken'))
                    .subscribe(function (data) {
                    $('#stopsAssign-Status').hide();
                    if (data.status == true) {
                        customToastr('Stop is assign to Route successfully', 'success');
                        $("#assignstoppopupclose").trigger("click");
                    }
                    else if (data.status == false) {
                        customToastr('Stop is already exist in this route', 'error');
                    }
                    else {
                        customToastr('Stop is not assign to Route please try again', 'error');
                    }
                }, function (error) {
                    return function () {
                    };
                });
            }
            else {
                $('#stopsAssign-Status').hide();
                customToastr('Provide atleast one stop', 'error');
            }
        }
        else {
            $('#stopsAssign-Status').hide();
            customToastr('Provide atleast one stop', 'error');
        }
    };
    TransportComponent.prototype.deleteBusDetail = function (pkBusId) {
        var _this = this;
        this.transportservice.deleteBusDetail(localStorage.getItem('usertoken'), this.selectedBusId)
            .subscribe(function (data) {
            if (data.status == true) {
                customToastr('Bus Deleted', 'success');
                $("#deletebuspopup").trigger("click");
                _this.getAllBuses();
                _this.getTransportCount();
                _this.getActiveBuses();
                _this.getInActiveBuses();
            }
            else {
                customToastr('Bus not deleted please try again', 'error');
            }
        }, function (error) {
            return function () {
            };
        });
    };
    TransportComponent.prototype.selectedUpdateRoute = function (value) {
        this.updateBus.fkRouteId = value;
    };
    TransportComponent.prototype.updateBusDetail = function (UpdateBus) {
        var _this = this;
        $('#updateBus-Status').show();
        this.updateBus.Capacity = $('.bus-cap').val();
        this.updateBus.BusName = $('.bus-name').val();
        this.updateBus.fkStaffId = $('.bus-staff').val();
        this.updateBus.fkRouteId = $('.bus-route').val();
        this.updateBus.BusStatus = $('input[name=active]:checked', '#stat').val();
        if (UpdateBus.BusName != "" && UpdateBus.BusName != null && UpdateBus.BusName != undefined && UpdateBus.Capacity != "" && UpdateBus.Capacity != null && UpdateBus.Capacity != undefined && UpdateBus.fkStaffId != "" && UpdateBus.fkStaffId != null && UpdateBus.fkStaffId != undefined && UpdateBus.fkRouteId != "" && UpdateBus.fkRouteId != null && UpdateBus.fkRouteId != undefined && UpdateBus.BusStatus != "" && UpdateBus.BusStatus != null && UpdateBus.BusStatus != undefined) {
            this.transportservice.updateBus(this.updateBus.BusName, this.updateBus.Capacity, this.updateBus.fkStaffId, this.updateBus.fkRouteId, this.updateBus.BusStatus, this.selectedBusId, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                $('#updateBus-Status').hide();
                if (data.status == true) {
                    customToastr('Bus Updated Successfully', 'success');
                    $("#editbuspopup").trigger("click");
                    _this.getAllBuses();
                    _this.getTransportCount();
                    _this.getActiveBuses();
                    _this.getInActiveBuses();
                }
                else {
                    customToastr('Bus not updated please try again', 'error');
                }
            }, function (error) {
                return function () {
                };
            });
        }
        else {
            $('#updateBus-Status').hide();
            customToastr('Please fill all fields', 'error');
        }
    };
    TransportComponent.prototype.initscript = function () {
        $(document).ready(function () {
            $('#all-transport').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#active-transport').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#inactive-transport').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#routes-table').DataTable();
            $('#stops-table').DataTable();
            /*sidedrawer*/
            $('.add-new-vehicle').attr('href', '#offcanvas-add-trans');
            $('.gradeX.all-trans').attr('href', '#offcanvas-view-all-trans');
            /**/
        });
    };
    TransportComponent.prototype.assignStop = function () {
        var h = this;
        $("#addmoreStop").on("click", function () {
            var html = '<div class="row roots_for_bus" id="addanotherStop">' +
                '<form class="form">' +
                '<div class="form-group col-md-6">' +
                '<select id="stoplist" name="select13" class="form-control" style="color: rgba(49, 53, 52, 0.51);font-size: small;">' +
                '<option value="">Select Stop</option>' +
                '';
            h.busStopDetail.forEach(function (s) {
                html += '<option value="' + s.PkBusStopId + '">' +
                    '' + s.BusStopName + '' +
                    '</option>';
            });
            html += '</select>' +
                '</div>' +
                '<div class="form-group col-md-6">' +
                '<input type="text" class="form-control" id="stopnumber" name="assignStopNo">' +
                '<label for="stopnumber">Stop Number</label>' +
                '</div>' +
                '</form>' +
                '</div>';
            $('#addanotherStopParent').append(html);
        });
    };
    TransportComponent.prototype.ngOnInit = function () {
        this.isValidToken();
        $(".after").empty();
        $("#device-breakpoints").remove();
        this.assignStop();
        this.initscript();
        this.getTransportCount();
        this.getAllBuses();
        this.getActiveBuses();
        this.getInActiveBuses();
        this.getRouteList();
        this.getBusStop();
        this.getDriverList();
    };
    TransportComponent = __decorate([
        Component({
            selector: 'app-transport',
            templateUrl: './transport.component.html',
            styleUrls: [
                'assets/css/theme-default/bootstrap.css?1422792965',
                'assets/css/theme-default/materialadmin.css?1425466319',
                'assets/css/theme-default/font-awesome.min.css?1422529194',
                'assets/css/theme-default/material-design-iconic-font.min.css?1421434286',
                'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
                'assets/css/theme-default/libs/toastr/toastr.css?1425466569',
                './export.css',
                './jquery.dataTables.min.css',
                './buttons.dataTables.min.css',
                './transport.component.css'],
            encapsulation: ViewEncapsulation.None,
            providers: [LoginService, TransportService]
        }), 
        __metadata('design:paramtypes', [Router, LoginService, TransportService])
    ], TransportComponent);
    return TransportComponent;
}());
//# sourceMappingURL=F:/BW_PR/src/app/transport/transport.component.js.map