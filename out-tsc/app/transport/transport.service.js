var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import 'rxjs/Rx';
import { AppSettings } from "../../config/config";
export var TransportService = (function () {
    function TransportService(http) {
        this.http = http;
        this._url = AppSettings.API_ENDPOINT;
    }
    TransportService.prototype.getAllBusRoutes = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/getBusRouteList', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.getAllBuses = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/getbuses', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.getstopsOfRoute = function (schoolId, routeId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, routeId: routeId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/viewStopforRoute', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.fetchTransportCount = function (schoolId, AUTHTOKEN) {
        console.log("in sevice call");
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/getbuscount', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.fetchAllBuses = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/getbuses', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.fetchActiveBuses = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/activebuses', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.fetchInActiveBuses = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/getinactivebuses', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.fetchBusesDetail = function (AUTHTOKEN, busId) {
        var json = JSON.stringify({ pkBusId: busId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/getdriverinfo', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.fetchBusesRoute = function (AUTHTOKEN) {
        var json = JSON.stringify({});
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/getBusRouteList', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.insertBusRoute = function (routename, startpoint, endpoint, totalstops, AUTHTOKEN) {
        var json = JSON.stringify({ RouteName: routename, StartPoint: startpoint, EndPoint: endpoint, TotalStops: totalstops });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/addBusrouteinfo', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.fetchBusStop = function (AUTHTOKEN) {
        var json = JSON.stringify({});
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/getBusStopsList', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.fetchDriverList = function (AUTHTOKEN) {
        var json = JSON.stringify({});
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/driverlist', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.addNewBus = function (busnumber, busname, capacity, staffId, deviceId, routeId, status, AUTHTOKEN) {
        var json = JSON.stringify({ BusNumber: busnumber, BusName: busname, Capacity: capacity, fkStaffId: staffId, DeviceId: deviceId, fkRouteId: routeId, BusStatus: status });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/addbuses', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.updateBus = function (busname, capacity, staffId, routeId, status, busId, AUTHTOKEN) {
        var json = JSON.stringify({ BusId: busId, BusName: busname, Capacity: capacity, fkStaffId: staffId, fkRouteId: routeId, BusStatus: status });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/update_busDetail', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.fetchRouteDetail = function (AUTHTOKEN, routeId) {
        var json = JSON.stringify({ routeId: routeId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/routeDetail', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.updateBusRoute = function (routename, startpoint, endpoint, totalstop, routeId, AUTHTOKEN) {
        var json = JSON.stringify({ RouteName: routename, StartPoint: startpoint, EndPoint: endpoint, TotalStop: totalstop, routeId: routeId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/updatebusroute', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.deleteRouteDetail = function (AUTHTOKEN, routeId) {
        var json = JSON.stringify({ routeId: routeId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/deleteBusRoute', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.addBusStop = function (stopsName, fare, latitude, longitude, AUTHTOKEN) {
        var json = JSON.stringify({ BusStopName: stopsName, fare: fare, Latitude: latitude, Longitude: longitude });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/createbusstop', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.fetchStopDetail = function (AUTHTOKEN, stopId) {
        var json = JSON.stringify({ stopId: stopId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/stopsDetail', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.updateBusStop = function (stopsName, fare, latitude, longitude, stopId, AUTHTOKEN) {
        var json = JSON.stringify({ BusStopName: stopsName, Busfare: fare, Latitude: latitude, Longitude: longitude, stopId: stopId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/updateBusStops', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.deleteStopDetail = function (AUTHTOKEN, stopId) {
        var json = JSON.stringify({ stopId: stopId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/deleteBusStops', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.fetchStopForRoute = function (AUTHTOKEN, routeId) {
        var json = JSON.stringify({ routeId: routeId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/viewStopforRoute', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.assignStopForRoute = function (stoplist, pkRouteId, AUTHTOKEN) {
        var json = JSON.stringify({ stopIdList: stoplist, FkRouteId: pkRouteId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/addBusRouteStopsInfo', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService.prototype.deleteBusDetail = function (AUTHTOKEN, pkBusId) {
        var json = JSON.stringify({ pkBusId: pkBusId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/bus/deleteBusDetail', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    TransportService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [Http])
    ], TransportService);
    return TransportService;
}());
//# sourceMappingURL=F:/BW_PR/src/app/transport/transport.service.js.map