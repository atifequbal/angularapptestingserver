var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import 'rxjs/Rx';
import { AppSettings } from "../../config/config";
export var GroupService = (function () {
    function GroupService(http) {
        this.http = http;
        this._url = AppSettings.API_ENDPOINT;
    }
    //get scripts
    GroupService.prototype.getScripts = function () {
        var scripts = [{ "src": "assets/js/libs/jquery/jquery-1.11.2.min.js" },
            { "src": "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js" },
            { "src": "assets/js/misc/jquery.particleground.js" },
            { "src": "assets/js/misc/demo.js" },
            { "src": "_crop/scripts/jquery.Jcrop.js" },
            { "src": "_crop/scripts/jquery.SimpleCropper.js" },
            { "src": "_upload/js/custom-file-input.js" },
            { "src": "assets/js/libs/jquery/jquery-migrate-1.2.1.min.js" },
            { "src": "assets/js/libs/bootstrap/bootstrap.min.js" },
            { "src": "assets/js/libs/wizard/jquery.bootstrap.wizard.min.js" },
            { "src": "assets/js/libs/spin.js/spin.min.js" },
            { "src": "assets/js/libs/autosize/jquery.autosize.min.js" },
            { "src": "assets/js/libs/DataTables/jquery.dataTables.min.js" },
            { "src": "assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js" },
            { "src": "assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js" },
            { "src": "assets/js/libs/nanoscroller/jquery.nanoscroller.min.js" },
            { "src": "_crop/scripts/jquery.Jcrop.js" },
            { "src": "_crop/scripts/jquery.SimpleCropper.js" },
            { "src": "assets/js/core/source/App.js" },
            { "src": "assets/js/core/source/AppNavigation.js" },
            { "src": "assets/js/core/source/AppOffcanvas.js" },
            { "src": "assets/js/core/source/AppCard.js" },
            { "src": "assets/js/core/source/AppForm.js" },
            { "src": "assets/js/core/source/AppNavSearch.js" },
            { "src": "assets/js/core/source/AppVendor.js" },
            { "src": "assets/js/core/demo/Demo.js" },
            { "src": "assets/js/core/demo/DemoFormWizard.js" },
            { "src": "assets/js/core/demo/DemoTableDynamic.js" },
            { "src": "assets/js/libs/toastr/toastr.js" },
            { "src": "assets/js/core/demo/DemoUIMessages.js" },
            { "src": "http://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js" }];
        return scripts;
    };
    //import student proceed [validate json data for duplicates with temp table]
    GroupService.prototype.validateJsonDataOnProceed = function (data, getSchoolId, getImportStudentsClassId, getAcademicId, AUTHTOKEN) {
        var json = data;
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/importStudent?schoolId=' + getSchoolId + '&classId=' + getImportStudentsClassId + '&academicId=' + getAcademicId, params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //update student details
    GroupService.prototype.updateStudentDetails = function (admissionObj, AUTHTOKEN) {
        var json = JSON.stringify(admissionObj);
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/addstudent/updateStudent', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //fetch available class categories
    GroupService.prototype.groupsClassCategories = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classCategory', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //fetch available class with cat id
    GroupService.prototype.groupsAvailableClass = function (schoolId, catid, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, categoryId: catid });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/className', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //fetch available classes in school with school id
    GroupService.prototype.groupsAvailableAllClass = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/allClassName', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //fetch available class section with class id
    GroupService.prototype.groupsAvailableClassSection = function (classid, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classid });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classSectionName', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //get student list
    GroupService.prototype.groupsStudentList = function (schoolId, academicId, catId, classId, sectionId, countfrom, lengthOf, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, academicId: academicId, categoryId: catId, classId: classId, sectionId: sectionId, countFrom: countfrom, lengthOf: lengthOf });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/studentlist', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //get academic list
    GroupService.prototype.groupsStudentAcademicList = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/academiclist', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //get school designation list
    GroupService.prototype.groupsSchoolDesignationList = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/schoolDesignation', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //get staff list
    GroupService.prototype.groupsStaffList = function (schoolId, designationId, isBlockedStatus, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, designationId: designationId, isBlockedStatus: isBlockedStatus });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/stafflist', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //get staff list details
    GroupService.prototype.groupsStaffListDetail = function (staffId, AUTHTOKEN) {
        var json = JSON.stringify({ staffId: staffId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/stafflist/staffDetail', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //get staff qualification list
    GroupService.prototype.groupsStaffQualifList = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/stafflist/staffQualification', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //staff update
    GroupService.prototype.groupsStaffUpdate = function (staffName, staffDOB, staffGender, staffAppStatus, staffBlockStatus, staffQualification, staffDesignation, staffDOJ, staffMobile, staffEmail, staffAddress, staffId, AUTHTOKEN) {
        var json = JSON.stringify({ staffName: staffName, staffDOB: staffDOB, staffGender: staffGender, staffAppStatus: staffAppStatus, staffBlockStatus: staffBlockStatus, staffQualification: staffQualification, staffDesignation: staffDesignation, staffDOJ: staffDOJ, staffMobile: staffMobile, staffEmail: staffEmail, staffAddress: staffAddress, staffId: staffId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/stafflist/staffUpdate', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //staff update
    GroupService.prototype.groupsAddNewStaff = function (staffName, empId, dateOfBirth, gender, isAppUser, isBlocked, qualification, qualificationId, designationId, dateOfJoining, mobileNumber, emailId, address, schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ staffName: staffName, empId: empId, dateOfBirth: dateOfBirth, gender: gender, isAppUser: isAppUser, isBlocked: isBlocked, qualification: qualification, qualificationId: qualificationId, designationId: designationId, dateOfJoining: dateOfJoining, mobileNumber: mobileNumber, emailId: emailId, address: address, schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/stafflist/staffAdd', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //addnewdesignation
    GroupService.prototype.groupsStaffEditDesignation = function (designationId, designationName, isSuperAdmin, isClassTeacher, isDriver, AUTHTOKEN) {
        var json = JSON.stringify({ designationId: designationId, designationName: designationName, isSuperAdmin: isSuperAdmin, isClassTeacher: isClassTeacher, isDriver: isDriver, });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/stafflist/staffEditDesignation', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //add new qualification
    GroupService.prototype.groupsStaffEditQualification = function (pkStaffQualificationId, qualification, AUTHTOKEN) {
        var json = JSON.stringify({ pkStaffQualificationId: pkStaffQualificationId, qualification: qualification });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/stafflist/staffEditQualification', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //addnewdesignation
    GroupService.prototype.groupsStaffAdddesignation = function (schoolId, designation, isSuperAdmin, isClassTeacher, isDriver, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, designation: designation, isSuperAdmin: isSuperAdmin, isClassTeacher: isClassTeacher, isDriver: isDriver, });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/stafflist/staffAddDesignation', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //add new qualification
    GroupService.prototype.groupsStaffAddqualification = function (schoolId, qualification, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, qualification: qualification });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/stafflist/staffAddQualification', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //view student detail from list selection
    GroupService.prototype.getSelectedStudentDetail = function (studentId, AUTHTOKEN) {
        var json = JSON.stringify({ studentId: studentId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/studentlist/studentview', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //get class detail with classid
    GroupService.prototype.getClassDetailWithClassId = function (classId, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/studentlist/studentclassdetail', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //add student
    GroupService.prototype.addNewStudentDetails = function (studentName, studentDOB, studentGender, studentClassId, studentAdmissionDate, studentAdmissionNumber, studentParentName, studentParentContact, studentParentEmail, studentAddress, studentAcademicId, studentSchoolId, AUTHTOKEN) {
        var json = JSON.stringify({ studentName: studentName, studentDOB: studentDOB, studentGender: studentGender, studentClassId: studentClassId, studentAdmissionDate: studentAdmissionDate, studentAdmissionNumber: studentAdmissionNumber, studentParentName: studentParentName, studentParentContact: studentParentContact, studentParentEmail: studentParentEmail, studentAddress: studentAddress, studentAcademicId: studentAcademicId, studentSchoolId: studentSchoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/addstudent', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    /* this._service.postWithFile(this.baseUrl + "add-update",postData,file).then(result => {
     console.log(result);
     });*/
    //student bulk upload xls-json to temp
    GroupService.prototype.createxlsjson = function (postData, file) {
        //var json = JSON.stringify({source: source, destination: destination});
        //var params = json;
        // var headers = new Headers();
        // headers.append('Content-Type','application/json');
        return this.http.post(this._url + '/uploadxlsjson', postData, file)
            .map(function (res) { return res.toString(); });
    };
    GroupService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [Http])
    ], GroupService);
    return GroupService;
}());
//# sourceMappingURL=F:/BW_PR/src/app/groups/groups.service.js.map