var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewEncapsulation, ElementRef, Renderer, NgZone } from '@angular/core';
import { GroupService } from "./groups.service";
import { FormValidator } from "../form.validator";
import { Http } from "@angular/http";
import { Observable } from "rxjs";
import { AppSettings } from "../../config/config";
import { Router } from "@angular/router";
import { LoginService } from "../login/login.service";
import { AdmissionService } from "../admission/admission.service";
export var GroupsComponent = (function () {
    function GroupsComponent(_ngZone, admissionservice, router, groupservice, formValidator, http, elementRef, renderer, loginservice, elRef) {
        var _this = this;
        this._ngZone = _ngZone;
        this.admissionservice = admissionservice;
        this.router = router;
        this.groupservice = groupservice;
        this.formValidator = formValidator;
        this.http = http;
        this.loginservice = loginservice;
        this.elRef = elRef;
        this.preLoader = false;
        //new variables//////////////////////////////////
        this.getAuthToken = localStorage.getItem('usertoken');
        this.getSchoolId = localStorage.getItem('schoolinfo');
        this.academicId = 0;
        this.staffQualificationListArray = [];
        this.selectedStaffId = 0;
        //student
        this.selectedStudentListId = 0;
        /*////////////////////////////////////////////*/
        this.loader = false;
        this.loaderAddStudent = false;
        this.schoolCategories = [];
        this.schoolClasses = [];
        this.schoolClassesSections = [];
        this.schoolClassesAdd = [];
        this.schoolClassesSectionsAdd = [];
        this.studentListArray = [];
        this.schoolAcademicList = [];
        this.schoolAcademicCurrent = [];
        this.schoolDesignationList = [];
        this.selectClassStatus = true;
        this.selectClassSectionStatus = true;
        this.selectClassStatusAdd = true;
        this.selectClassSectionStatusAdd = true;
        this.selectedCategory = null;
        this.selectedCategoryAdd = null;
        this.selectedClass = null;
        this.selectedClassAdd = null;
        this.selectedSection = null;
        this.selectedSectionAdd = null;
        this.staffStatus = 1; //active
        this.selectedDesignation = null;
        this.staffListArray = [];
        this.countFrom = 0;
        this.lengthof = 10;
        //add student variables
        this.studentName = "";
        this.studentDOB = "";
        this.studentGender = 0;
        this.studentClassId = this.selectedSectionAdd;
        this.studentAdmissionDate = "";
        this.studentAdmissionNumber = "";
        this.studentParentName = "";
        this.studentParentContact = "";
        this.studentParentEmail = "";
        this.studentAddress = "";
        this.studentAcademicId = this.academicId;
        this.studentSchoolId = this.getSchoolId;
        this.addNewStudentStatus = 0;
        this.addNewStudentgender = 1;
        //import student
        this.proceedxlx = false;
        this.Exceldata = "";
        this.schoolAllClass = [];
        //add new student variables
        this.addNewStudentName = "";
        this.addNewStudentSurName = "";
        this.addNewStudentGender = "";
        this.addNewStudentDOB = "";
        this.addNewStudentBloodGroup = "";
        this.addNewStudentReligion = "";
        this.addNewStudentCaste = "";
        this.addNewStudentNationality = "";
        this.addNewStudentEmail = "";
        this.addNewStudentNumber = "";
        this.addNewStudentAademicid = this.academicId;
        this.addNewStudentAdmissionBase = "";
        this.addNewStudentAdmissionStream = "";
        this.addNewStudentAdmissionNumber = "";
        this.addNewStudentClassId = "";
        this.addNewStudentFatherName = "";
        this.addNewStudentFatherNumber = "";
        this.addNewStudentFatherOccupation = "";
        this.addNewStudentFatherQualification = "";
        this.addNewStudentFatherAddress = "";
        this.addNewStudentMotherName = "";
        this.addNewStudentMotherNumber = "";
        this.addNewStudentMotherOccupation = "";
        this.addNewStudentMotherQualification = "";
        this.addNewStudentMotherSameFatherAddress = "";
        this.addNewStudentMotherAddress = "";
        this.addNewStudentGuardianName = "";
        this.addNewStudentGuardianNumber = "";
        this.addNewStudentGuardianOccupation = "";
        this.addNewStudentGuardianQualification = "";
        this.addNewStudentGuardianSameFatherAddress = "";
        this.addNewStudentGuardianAddress = "";
        this.superAdminAccess = false;
        this.classTeacherAccess = false;
        this.driverAccess = false;
        this.editDesignationState = false;
        this.editingDesignationId = null;
        this.pkStaffQualificationId = null;
        this.editQualificationState = false;
        this._url = AppSettings.API_ENDPOINT;
        this.tempSelectedStudentId = 0;
        this.loader_import_student = true;
        this.loader_edit_student = false;
        this.schoolClassesEdit = [];
        this.schoolClassesSectionsEdit = [];
        //import student new one
        //select category function//
        this.getClassInImport = [];
        this.getSectionInImport = [];
        this.getImportStudentsClassId = 0;
        this.getImportStudentProceedStatus = false;
        this.importStudentListArray = [];
        this.fileList = [];
        window['angularComponentRef'] = { component: this, zone: _ngZone };
        renderer.listenGlobal('document', 'click', function (evt) {
            //console.log("EVENT TRIGGERED: ",evt.srcElement)
            _this.paginateClass = evt.srcElement.className;
            _this.paginateId = evt.srcElement.getAttribute('aria-controls');
            if (_this.paginateClass.includes("paginate_button") && _this.paginateId.includes("table-importstudent")) {
                // console.log('Paginate Class', this.paginateClass);
                // console.log('Paginate Id', this.paginateId);
                _this.importstudent_modalproceed();
            }
        });
    }
    //select student from list to view details
    GroupsComponent.prototype.selectStudentFromList = function () {
        var _this = this;
        // add the HTML to the DOM
        this.elRef.nativeElement.querySelector('#groups-student-list tbody').addEventListener('click', function (event) { return _this.handleStudentViewFromList(event); });
    };
    GroupsComponent.prototype.handleStudentViewFromList = function (event) {
        var _this = this;
        if (this.tempSelectedStudentId != event.target.closest('tr').getAttribute('id')) {
            this.tempSelectedStudentId = event.target.closest('tr').getAttribute('id');
            this.selectedStudentListId = event.target.closest('tr').getAttribute('id');
            console.log("selected student id:  ", this.selectedStudentListId);
            //trigger canvas :-)
            $('.view-selected-student').click();
            //particles trigger
            $('#particles-view-student').particleground({ dotColor: '#f3f3f3', lineColor: '#ffffff', lineWidth: .3 });
            $('#particles-edit-student').particleground({ dotColor: '#f3f3f3', lineColor: '#ffffff', lineWidth: .3 });
            //selected student details
            this.groupservice.getSelectedStudentDetail(this.selectedStudentListId, this.getAuthToken)
                .subscribe(function (data) {
                console.log("Seleted student detail: ", data);
                //assign seleted student view element values from data
                _this.selectedStudentname = data.studentDetail.studentName;
                _this.selectedStudentimage = data.studentDetail.studentImage;
                _this.selectedStudentsurname = data.studentDetail.surname;
                _this.selectedStudentemail = data.studentDetail.studentEmail;
                _this.selectedStudentcontact = data.studentDetail.studentPhone;
                _this.selectedStudentadmissiondate = data.studentDetail.dateOfAdmission;
                _this.selectedStudentadmissionnumber = data.studentDetail.registrationId;
                _this.selectedStudentdateofbirth = data.studentDetail.dateOfBirth;
                if (data.studentDetail.gender == 1) {
                    _this.selectedStudentgender = true;
                }
                else if (data.studentDetail.gender == 0) {
                    _this.selectedStudentgender = false;
                }
                else {
                    _this.selectedStudentgender = false;
                }
                //get class detail with class id [category, class name, class section]
                _this.groupservice.getClassDetailWithClassId(data.studentDetail.classId, _this.getAuthToken)
                    .subscribe(function (data) {
                    _this.selectedStudentclasscategory = data.classDetail.categoryName;
                    _this.selectedStudentclassname = data.classDetail.className;
                    _this.selectedStudentclasssection = data.classDetail.classSection;
                    _this.selectedStudentclasscategoryId = data.classDetail.categoryId;
                    _this.selectedStudentclassnameId = data.classDetail.classNameId;
                    _this.selectedStudentclasssectionId = data.classDetail.classSectionId;
                }, function (error) { return console.log(error); }, function () { });
                // this.selectedStudentclasscategory = data.studentDetail.classId;
                // this.selectedStudentclassname = data.studentDetail.classId;
                // this.selectedStudentclasssection = data.studentDetail.classId;
                _this.selectedStudentclassid = data.studentDetail.rollNumber;
                if (data.studentDetail.isAppUser == 0) {
                    _this.selectedStudentbwappstatus = false;
                }
                else if (data.studentDetail.isAppUser == 1) {
                    _this.selectedStudentbwappstatus = true;
                }
                else {
                    _this.selectedStudentbwappstatus = false;
                }
                if (data.studentDetail.isBlocked == 0) {
                    _this.selectedStudentaccountstatus = true;
                }
                else if (data.studentDetail.isBlocked == 1) {
                    _this.selectedStudentaccountstatus = false;
                }
                else {
                    _this.selectedStudentaccountstatus = true;
                }
                _this.selectedStudentfathername = data.studentDetail.parentName;
                _this.selectedStudentfathercontact = data.studentDetail.mobileNumber;
                _this.selectedStudentfatheroccupation = data.studentDetail.fatherOccupation;
                _this.selectedStudentfatherqualification = data.studentDetail.fatherQualification;
                _this.selectedStudentfatheraddress = data.studentDetail.address;
                _this.selectedStudentmothername = data.studentDetail.motherName;
                _this.selectedStudentmothercontact = data.studentDetail.motherPhone;
                _this.selectedStudentmotheroccupation = data.studentDetail.motherOccupation;
                _this.selectedStudentmotherqualification = data.studentDetail.motherQualification;
                _this.selectedStudentmotheraddress = data.studentDetail.motherAddress;
                _this.selectedStudentguardianname = data.studentDetail.guardianName;
                _this.selectedStudentguardiancontact = data.studentDetail.guardianPhone;
                _this.selectedStudentguardianoccupation = data.studentDetail.guardianOccupation;
                _this.selectedStudentguardianqualification = data.studentDetail.guardianQualification;
                _this.selectedStudentguardianaddress = data.studentDetail.guardianAddress;
                _this.selectedStudentEditclassid = _this.selectedStudentclassid;
                _this.selectedStudentEditbwappstatus = _this.selectedStudentbwappstatus;
                _this.selectedStudentEditaccountstatus = _this.selectedStudentaccountstatus;
                _this.selectedStudentEditfathername = _this.selectedStudentfathername;
                _this.selectedStudentEditfathercontact = _this.selectedStudentfathercontact;
                _this.selectedStudentEditfatheroccupation = _this.selectedStudentfatheroccupation;
                _this.selectedStudentEditfatherqualification = _this.selectedStudentfatherqualification;
                _this.selectedStudentEditfatheraddress = _this.selectedStudentfatheraddress;
                _this.selectedStudentEditmothername = _this.selectedStudentmothername;
                _this.selectedStudentEditmothercontact = _this.selectedStudentmothercontact;
                _this.selectedStudentEditmotheroccupation = _this.selectedStudentmotheroccupation;
                _this.selectedStudentEditmotherqualification = _this.selectedStudentmotherqualification;
                _this.selectedStudentEditmotheraddress = _this.selectedStudentmotheraddress;
                _this.selectedStudentEditguardianname = _this.selectedStudentguardianname;
                _this.selectedStudentEditguardiancontact = _this.selectedStudentguardiancontact;
                _this.selectedStudentEditguardianoccupation = _this.selectedStudentguardianoccupation;
                _this.selectedStudentEditguardianqualification = _this.selectedStudentguardianqualification;
                _this.selectedStudentEditguardianaddress = _this.selectedStudentguardianaddress;
                //$('.view-selected-student').click();
            }, function (error) { return console.log(error); }, function () {
            });
        }
    };
    //update selected student details
    GroupsComponent.prototype.updateSelectedStudentDetails = function (editstudentName, editstudentSurname, editstudentStudentEmail, editstudentStudentContactnNumber, editstudentStudentDOB, editstudentStudentGenderMale, editstudentStudentGenderFemale, editstudentStudentClassId, editstudentStudentAppStausConnected, editstudentStudentAppStausDisConnected, editstudentStudentAccountActive, editstudentStudentAccountDeActive, editstudentFatherName, editstudentFatherContact, editstudentFatherOccupation, editstudentFatherQualifiation, editstudentFatherAddress, editstudentMotherName, editstudentMotherContact, editstudentMotherOccupation, editstudentMotherQualifiation, editstudentMotherAddress, editstudentGuardianName, editstudentGuardianContact, editstudentGuardianOccupation, editstudentGuardianQualifiation, editstudentGuardianAddress) {
        var _this = this;
        var studentGender;
        var studentBluewingsAppStatus;
        var studentAccountStatus;
        //customToastr('Updated','success');
        //gender
        if ($('.statusGenderMale').hasClass('active')) {
            studentGender = 1;
        }
        else if ($('.statusGenderFemale').hasClass('active')) {
            studentGender = 0;
        }
        //appStatus
        if ($("#appStatusConected").prop('checked') == true) {
            studentBluewingsAppStatus = 1;
        }
        else if ($("#appStatusDisConneted").prop('checked') == true) {
            studentBluewingsAppStatus = 0;
        }
        //accountStatus
        if ($("#accountStatusActive").prop('checked') == true) {
            studentAccountStatus = 0;
        }
        else if ($("#accountStatusDeActive").prop('checked') == true) {
            studentAccountStatus = 1;
        }
        if (!this.formValidator.validateIsData(editstudentName)) {
            customToastr('Student name is missing!', 'error');
        }
        else if (!this.formValidator.validateIsData(editstudentFatherName)) {
            customToastr('Enter student father name!', 'error');
        }
        else if (!this.formValidator.validateIsData(editstudentStudentDOB)) {
            customToastr('Enter student father name!', 'error');
        }
        else if (!this.formValidator.validateIsData(editstudentFatherContact)) {
            customToastr('Enter student father contact number!', 'error');
        }
        else if (!this.formValidator.validateIsData(editstudentFatherAddress)) {
            customToastr('Enter student father address!', 'error');
        }
        else if (!this.formValidator.validateIsData(editstudentMotherName)) {
            customToastr('Enter student mother name!', 'error');
        }
        else {
            var d = editstudentStudentDOB.split("/");
            editstudentStudentDOB = "" + d[2] + "-" + d[1] + "-" + d[0] + "";
            this.loader_edit_student = true;
            var admissionObj = {
                studentId: this.selectedStudentListId,
                studentName: editstudentName,
                classId: editstudentStudentClassId,
                parentName: editstudentFatherName,
                studentEmail: editstudentStudentEmail,
                dateOfBirth: editstudentStudentDOB,
                mobileNumber: editstudentFatherContact,
                address: editstudentFatherAddress,
                surname: editstudentSurname,
                studentPhone: editstudentStudentContactnNumber,
                fatherOccupation: editstudentFatherOccupation,
                fatherQualification: editstudentFatherQualifiation,
                motherName: editstudentMotherName,
                motherPhone: editstudentMotherContact,
                motherOccupation: editstudentMotherOccupation,
                motherQualification: editstudentMotherQualifiation,
                motherAddress: editstudentMotherAddress,
                guardianName: editstudentGuardianName,
                guardianPhone: editstudentGuardianContact,
                guardianOccupation: editstudentGuardianOccupation,
                guardianQualification: editstudentGuardianQualifiation,
                gender: studentGender,
                isAppUser: studentBluewingsAppStatus,
                isBlocked: studentAccountStatus,
            };
            console.log("Get updated data student: ", admissionObj);
            //update student service
            this.groupservice.updateStudentDetails(admissionObj, this.getAuthToken)
                .subscribe(function (data) {
                _this.loader_edit_student = false;
                if (data.status == true) {
                    customToastr('Student details updated', 'success');
                    _this.getGroupsClassCategories(_this.getSchoolId);
                    _this.getAllShoolClass(_this.getSchoolId);
                }
                else {
                    customToastr('Please try again!!', 'error');
                }
            }, function (error) { return console.log(error); }, function () {
            });
        }
    };
    //select category function
    GroupsComponent.prototype.selectcategoryFunction = function (selectedCategoryId) {
        var _this = this;
        this.selectedCategory = selectedCategoryId;
        //destroy the datatable suppose to append data no need in server process [also set destroy in the function]
        //this.destroyDataTable('groups-student-list');
        this.initDataTableServerProcess("groups-student-list", this.getSchoolId, this.getAuthToken, this.academicId, this.selectedCategory, "null", "null");
        this.selectStudentFromList();
        //make selection disabled
        if (selectedCategoryId != 0) {
            this.selectClassStatus = false;
            this.schoolClasses = [];
            this.schoolClassesSections = [];
            //call get class service with cat id
            this.groupservice.groupsAvailableClass(this.getSchoolId, this.selectedCategory, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                for (var i = 0; i < data.className.length; i++) {
                    _this.schoolClasses.push(data.className[i]);
                }
                console.log("School classes: ", _this.schoolClasses);
                _this.loader = false;
                _this.preLoader = true;
            }, function (error) { return console.log(error); }, function () {
            });
        }
        else {
            this.schoolClasses = [];
            this.schoolClassesSections = [];
            this.selectClassStatus = true;
            this.selectClassSectionStatus = true;
            this.selectedCategory = null;
            this.selectedClass = null;
            this.selectedSection = null;
        }
    };
    //select category function for add student
    GroupsComponent.prototype.selectcategoryFunctionAdd = function (selectedCategoryId) {
        var _this = this;
        this.selectedCategoryAdd = null;
        this.selectedClassAdd = null;
        this.selectedSectionAdd = null;
        this.selectedCategoryAdd = selectedCategoryId;
        //make selection disabled
        if (selectedCategoryId != 0) {
            this.loaderAddStudent = true;
            this.selectClassStatusAdd = false;
            this.schoolClassesAdd = [];
            this.schoolClassesSectionsAdd = [];
            //call get class service with cat id
            this.groupservice.groupsAvailableClass(this.getSchoolId, this.selectedCategoryAdd, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                try {
                    for (var i = 0; i < data.className.length; i++) {
                        _this.schoolClassesAdd.push(data.className[i]);
                    }
                    console.log("School classes ADded: ", _this.schoolClassesAdd);
                    _this.loaderAddStudent = false;
                }
                catch (e) {
                    _this.loaderAddStudent = false;
                }
            }, function (error) {
                console.log(error);
                _this.loaderAddStudent = false;
            }, function () {
            });
        }
        else {
            this.loaderAddStudent = false;
            this.schoolClassesAdd = [];
            this.schoolClassesSectionsAdd = [];
            this.selectClassStatusAdd = true;
            this.selectClassSectionStatusAdd = true;
            this.selectedCategoryAdd = null;
            this.selectedClassAdd = null;
            this.selectedSectionAdd = null;
        }
    };
    //select category function for add student
    GroupsComponent.prototype.selectcategoryFunctionEdit = function (selectedCategoryId) {
        var _this = this;
        this.selectedCategoryEdit = null;
        this.selectedClassEdit = null;
        this.selectedSectionEdit = null;
        this.selectedCategoryEdit = selectedCategoryId;
        //make selection disabled
        if (selectedCategoryId != 0) {
            this.schoolClassesEdit = [];
            this.schoolClassesSectionsEdit = [];
            //call get class service with cat id
            this.groupservice.groupsAvailableClass(this.getSchoolId, this.selectedCategoryEdit, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                try {
                    for (var i = 0; i < data.className.length; i++) {
                        _this.schoolClassesEdit.push(data.className[i]);
                    }
                    console.log("School classes: ", _this.schoolClassesEdit);
                }
                catch (e) {
                }
            }, function (error) {
                console.log(error);
            }, function () {
            });
        }
        else {
            this.schoolClassesEdit = [];
            this.schoolClassesSectionsEdit = [];
            this.selectedCategoryEdit = null;
            this.selectedClassEdit = null;
            this.selectedSectionEdit = null;
        }
    };
    //select class function
    GroupsComponent.prototype.selectclassFunction = function (selectedClassId) {
        var _this = this;
        this.selectedClass = selectedClassId;
        //make class section selection disabled
        if (selectedClassId != 0 || this.selectedCategory != 0) {
            this.selectClassSectionStatus = false;
            this.schoolClassesSections = [];
            //this.initDataTableServerProcess("groups-student-list",this.getSchoolId,this.getAuthToken,this.academicId,this.selectedCategory,this.selectedClass,"null");
            this.initDataTableServerProcess("groups-student-list", this.getSchoolId, this.getAuthToken, this.academicId, this.selectedCategory, this.selectedClass, "null");
            //call get class service with cat id
            this.groupservice.groupsAvailableClassSection(this.selectedClass, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                for (var i = 0; i < data.length; i++) {
                    _this.schoolClassesSections.push(data[i]);
                }
                console.log("School sections: ", _this.schoolClassesSections);
            }, function (error) { return console.log(error); }, function () {
            });
        }
        else {
            this.schoolClassesSections = [];
            this.selectClassSectionStatus = true;
            this.selectedClass = null;
            this.selectedSection = null;
        }
    };
    //select class function for add student
    GroupsComponent.prototype.selectclassFunctionAdd = function (selectedClassId) {
        var _this = this;
        this.selectedClassAdd = null;
        this.selectedSectionAdd = null;
        this.selectedClassAdd = selectedClassId;
        //make class section selection disabled
        if (selectedClassId != 0 && this.selectedCategoryAdd != 0) {
            this.selectClassSectionStatusAdd = false;
            this.schoolClassesSectionsAdd = [];
            this.loaderAddStudent = true;
            //call get class service with cat id
            this.groupservice.groupsAvailableClassSection(this.selectedClassAdd, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                try {
                    for (var i = 0; i < data.length; i++) {
                        _this.schoolClassesSectionsAdd.push(data[i]);
                    }
                    console.log("School schoolClassesSectionsAdd DATA: ", data);
                    console.log("School schoolClassesSectionsAdd: ", _this.schoolClassesSectionsAdd);
                    _this.loaderAddStudent = false;
                }
                catch (e) {
                    _this.loaderAddStudent = false;
                }
            }, function (error) {
                console.log(error);
                _this.loaderAddStudent = false;
            }, function () {
            });
        }
        else {
            this.loaderAddStudent = false;
            this.schoolClassesSectionsAdd = [];
            this.selectClassSectionStatusAdd = true;
            this.selectedClassAdd = null;
            this.selectedSectionAdd = null;
        }
    };
    //select class function for add student
    GroupsComponent.prototype.selectclassFunctionEdit = function (selectedClassId) {
        var _this = this;
        this.selectedClassEdit = null;
        this.selectedSectionEdit = null;
        this.selectedClassEdit = selectedClassId;
        //make class section selection disabled
        if (selectedClassId != 0 && this.selectedCategoryAdd != 0) {
            this.schoolClassesSectionsEdit = [];
            //call get class service with cat id
            this.groupservice.groupsAvailableClassSection(this.selectedClassEdit, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                try {
                    for (var i = 0; i < data.length; i++) {
                        _this.schoolClassesSectionsEdit.push(data[i]);
                    }
                }
                catch (e) {
                }
            }, function (error) {
                console.log(error);
            }, function () {
            });
        }
        else {
            this.schoolClassesSectionsEdit = [];
            this.selectedClassEdit = null;
            this.selectedSectionEdit = null;
        }
    };
    //select section function
    GroupsComponent.prototype.selectsectionFunction = function (selectedSectionId) {
        this.selectedSection = selectedSectionId;
        //this.loader = true;
        //$('#example').empty();
        //$('#example').DataTable().destroy();
        //this.initDataTable(this.academicId,this.selectedCategory,this.selectedClass,this.selectedSection);
        this.initDataTableServerProcess("groups-student-list", this.getSchoolId, this.getAuthToken, this.academicId, this.selectedCategory, this.selectedClass, this.selectedSection);
        //get list of students with category id
        /* this.groupservice.groupsStudentList(this.getSchoolId,this.academicId,this.selectedCategory,this.selectedClass,this.selectedSection,this.countFrom,this.lengthof,localStorage.getItem('usertoken'))
           .subscribe(
             data => {
               console.log("Student list by [Section NEW]: ",data);
               this.studentListArray = [];
               for(var i=0; i<data.studentsList.length; i++){
                 this.studentListArray.push(data.studentsList[i]);
               }
               if(this.studentListArray.length == 0){
                 $(".dataTables_empty").removeClass('hidden');
               }else{
                 $(".dataTables_empty").addClass('hidden');
               }
               console.log("Student List Array section: ",this.studentListArray);
               this.loader = false;
             },
             error => console.log(error),
             () => {}
           );*/
    };
    //select section function for add student
    GroupsComponent.prototype.selectsectionFunctionAdd = function (selectedSectionId) {
        this.selectedSectionAdd = selectedSectionId;
        console.log("Selected section Id: ", selectedSectionId);
    };
    GroupsComponent.prototype.selectsectionFunctionEdit = function (selectedSectionId) {
        this.selectedSectionEdit = selectedSectionId;
        console.log("Selected section Id: ", selectedSectionId);
    };
    //select designation function
    GroupsComponent.prototype.selectdesignationFunction = function (selectedDesignationId) {
        var _this = this;
        this.destroyDataTable('staff-list-table');
        this.staffListArray = [];
        //this.loader = true;
        this.selectedDesignation = selectedDesignationId;
        //get list of staff with designation id
        this.groupservice.groupsStaffList(this.getSchoolId, this.selectedDesignation, this.staffStatus, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            if (data && data.staffsList && data.staffsList.length > 0) {
                _this.staffListArray = data.staffsList;
                /* for(var i=0; i<data.staffsList.length; i++){
                 this.staffListArray.push(data.staffsList[i]);
                 }*/
                var h = _this;
                setTimeout(function () {
                    h.initializeDataTable('staff-list-table');
                }, 100);
            }
            else {
                _this.initializeDataTable('staff-list-table');
            }
        }, function (error) { return console.log(error); }, function () { });
    };
    //select staff status function [is bloked or not]
    GroupsComponent.prototype.selectstaffStatusFunction = function (selectedStatusId) {
        var _this = this;
        this.destroyDataTable('staff-list-table');
        this.staffListArray = [];
        //this.loader = true;
        this.staffStatus = selectedStatusId;
        //get list of staff with designation id
        this.groupservice.groupsStaffList(this.getSchoolId, this.selectedDesignation, this.staffStatus, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            if (data && data.staffsList && data.staffsList.length > 0) {
                _this.staffListArray = data.staffsList;
                /*for(var i=0; i<data.staffsList.length; i++){
                 this.staffListArray.push(data.staffsList[i]);
                 }*/
                var h = _this;
                setTimeout(function () {
                    h.initializeDataTable('staff-list-table');
                }, 100);
            }
            else {
                _this.initializeDataTable('staff-list-table');
            }
        }, function (error) { return console.log(error); }, function () { });
    };
    //get staff designation list
    GroupsComponent.prototype.getStaffQualificationList = function (schoolId) {
        var _this = this;
        this.staffQualificationListArray = [];
        this.groupservice.groupsStaffQualifList(schoolId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            for (var i = 0; i < data.staffQualification.length; i++) {
                _this.staffQualificationListArray.push(data.staffQualification[i]);
            }
        }, function (error) { return console.log(error); }, function () { });
    };
    //view staff from table
    GroupsComponent.prototype.listSelectedStaff = function (getStaffId) {
        var _this = this;
        this.selectedStaffId = getStaffId;
        //offcanvas call :-)
        $('#offcanvas-view-staff_trigger').click();
        console.log("Selected staff Id: ", getStaffId);
        //get staff details with staff id
        this.groupservice.groupsStaffListDetail(getStaffId, this.getAuthToken)
            .subscribe(function (data) {
            _this.selectedStaffName = data.staffDetail.staffName;
            _this.selectedStaffDOB = data.staffDetail.dateOfBirth;
            if (data.staffDetail.gender == 0) {
                _this.selectedStaffGender = true;
            }
            else if (data.staffDetail.gender == 1) {
                _this.selectedStaffGender = false;
            }
            else {
                _this.selectedStaffGender = false;
            }
            if (data.staffDetail.isAppUser == 0) {
                _this.selectedStaffAppStatus = false;
            }
            else if (data.staffDetail.isAppUser == 1) {
                _this.selectedStaffAppStatus = true;
            }
            else {
                _this.selectedStaffAppStatus = false;
            }
            if (data.staffDetail.isBlocked == 0) {
                _this.selectedStaffAccStatus = false;
            }
            else if (data.staffDetail.isBlocked == 1) {
                _this.selectedStaffAccStatus = true;
            }
            else {
                _this.selectedStaffAccStatus = false;
            }
            _this.selectedStaffQualification = data.staffDetail.qualification;
            _this.selectedStaffDesignation = data.staffDetail.designationId;
            _this.selectedStaffEmpId = data.staffDetail.employmentId;
            _this.selectedStaffDOJ = data.staffDetail.dateOfJoining;
            _this.selectedStaffContact = data.staffDetail.mobileNumber;
            _this.selectedStaffEmail = data.staffDetail.emailId;
            _this.selectedStaffAddress = data.staffDetail.address;
            _this.editSelectedStaffName = _this.selectedStaffName;
            _this.editSelectedStaffDOB = _this.selectedStaffDOB;
            _this.editSelectedStaffGender = data.staffDetail.gender;
            _this.editSelectedStaffAppStatus = data.staffDetail.isAppUser;
            _this.editSelectedStaffAccStatus = data.staffDetail.isBlocked;
            _this.editSelectedStaffQualification = _this.selectedStaffQualification;
            _this.editSelectedStaffDesignation = _this.selectedStaffDesignation;
            _this.editSelectedStaffEmpId = _this.selectedStaffEmpId;
            _this.editSelectedStaffDOJ = _this.selectedStaffDOJ;
            _this.editSelectedStaffContact = _this.selectedStaffContact;
            _this.editSelectedStaffEmail = _this.selectedStaffEmail;
            _this.editSelectedStaffAddress = _this.selectedStaffAddress;
        }, function (error) { return console.log(error); }, function () { });
    };
    GroupsComponent.prototype.editStaffQualification = function (qualificcationId) {
        console.log("Edit qual id: ", qualificcationId);
        this.editSelectedStaffQualification = qualificcationId;
    };
    GroupsComponent.prototype.editStaffDesignation = function (designationId) {
        console.log("Edit desig id: ", designationId);
        this.editSelectedStaffDesignation = designationId;
    };
    GroupsComponent.prototype.updateStaffDetails = function () {
        this.editSelectedStaffDOJ = $('.edit-staff-doj').val();
        this.editSelectedStaffDOB = $('.staff-edit-dob').val();
        this.groupservice.groupsStaffUpdate(this.editSelectedStaffName, this.editSelectedStaffDOB, this.editSelectedStaffGender, this.editSelectedStaffAppStatus, this.editSelectedStaffAccStatus, this.editSelectedStaffQualification, this.editSelectedStaffDesignation, this.editSelectedStaffDOJ, this.editSelectedStaffContact, this.editSelectedStaffEmail, this.editSelectedStaffAddress, this.selectedStaffId, this.getAuthToken)
            .subscribe(function (data) {
            console.log("staff update status===========>>>> ", data);
            if (data.update == true) {
                customToastr('Staff detail updated', 'success');
            }
            else {
                customToastr('Failed!!! Please try again', 'error');
            }
        }, function (error) { return console.log(error); }, function () { });
        // console.log("Edit staff get staff id: ",this.selectedStaffId)
        // console.log("staff eidt name",this.editSelectedStaffName);
        // console.log("staff eidt dob",this.editSelectedStaffDOB);
        // console.log("staff eidt gender",this.editSelectedStaffGender);
        // console.log("staff eidt app status",this.editSelectedStaffAppStatus);
        // console.log("staff eidt acc status",this.editSelectedStaffAccStatus);
        // console.log("staff eidt qualif",this.editSelectedStaffQualification);
        // console.log("staff eidt desig",this.editSelectedStaffDesignation);
        // console.log("staff eidt emp id",this.editSelectedStaffEmpId);
        // console.log("staff eidt doj",this.editSelectedStaffDOJ);
        // console.log("staff eidt contact",this.editSelectedStaffContact);
        // console.log("staff eidt email",this.editSelectedStaffEmail);
        // console.log("staff eidt address",this.editSelectedStaffAddress);
    };
    GroupsComponent.prototype.addStaffSelectedQualification = function (getQualificationId) {
        console.log("Get add staff qualification: ", getQualificationId);
        this.addSelectedStaffQualificationId = getQualificationId;
        for (var i in this.staffQualificationListArray) {
            if (this.staffQualificationListArray[i].pkStaffQualificationId == getQualificationId) {
                console.log(this.staffQualificationListArray[i].Qualification);
                this.addSelectedStaffQualification = this.staffQualificationListArray[i].Qualification;
            }
        }
    };
    GroupsComponent.prototype.addStaffSelectedDesignation = function (getDesignation) {
        console.log("Get add staff designation: ", getDesignation);
        this.addSelectedStaffDesignation = getDesignation;
    };
    GroupsComponent.prototype.addNewStaffDetail = function () {
        this.addSelectedStaffDOB = $('.addstaffDOB').val();
        this.addSelectedStaffGender = $('.addstaffGender label.active input').val();
        this.addSelectedStaffAppStatus = 0;
        this.addSelectedStaffAccStatus = 0;
        this.addSelectedStaffDOJ = $('.addstaffDOJ').val();
        console.log("1");
        if (this.formValidator.validateIsData(this.addSelectedStaffName)
            && this.formValidator.validateIsData(this.addSelectedStaffQualification)
            && this.formValidator.validateIsData(this.addSelectedStaffDesignation)
            && this.formValidator.validateIsData(this.addSelectedStaffEmpId)
            && this.formValidator.validateIsData(this.addSelectedStaffAddress)
            && this.formValidator.validateIsData(this.addSelectedStaffContact)
            && this.formValidator.validateIsData(this.addSelectedStaffEmail)) {
            console.log("2");
            if (!this.formValidator.validateIsData(this.addSelectedStaffAddress)) {
                this.addSelectedStaffAddress = null;
            }
            if (!this.formValidator.validateIsData(this.addSelectedStaffDOB)) {
                this.addSelectedStaffDOB = null;
            }
            else {
                var dob = this.addSelectedStaffDOB.split("/");
                if (!this.formValidator.isValidDateStr(dob[2] + "-" + dob[1] + "-" + dob[0])) {
                    customToastr("Please enter valid date of birth", 'error');
                    return;
                }
                else {
                    var d = this.addSelectedStaffDOB.split("/");
                    this.addSelectedStaffDOB = d[2] + "-" + d[1] + "-" + d[0];
                }
            }
            if (!this.formValidator.validateIsData(this.addSelectedStaffDOJ)) {
                this.addSelectedStaffDOJ = null;
            }
            else {
                var doj = this.addSelectedStaffDOJ.split("/");
                if (!this.formValidator.isValidDateStr(doj[2] + "-" + doj[1] + "-" + doj[0])) {
                    customToastr("Please enter valid date of joining", 'error');
                    return;
                }
                else {
                    var d = this.addSelectedStaffDOJ.split("/");
                    this.addSelectedStaffDOJ = d[2] + "-" + d[1] + "-" + d[0];
                }
            }
            if (!this.formValidator.validateIsEmail(this.addSelectedStaffEmail.toString())) {
                customToastr("Please enter valid email id", 'error');
                return;
            }
            if (!this.formValidator.validateMobileNumber(this.addSelectedStaffContact)) {
                customToastr("Please enter a valid mobile number (10 digits only)", 'error');
                return;
            }
            if (this.addSelectedStaffQualification == 'add-new') {
                customToastr("Please select a valid qualification", 'error');
                return;
            }
            if (this.addSelectedStaffDesignation == 'add-new') {
                customToastr("Please select a valid designation", 'error');
                return;
            }
            $(".disable-element").attr("disabled", true);
            $("#add-staff-button").text("Please wait...");
            console.log("3");
            this.groupservice.groupsAddNewStaff(this.addSelectedStaffName, this.addSelectedStaffEmpId, this.addSelectedStaffDOB, this.addSelectedStaffGender, this.addSelectedStaffAppStatus, this.addSelectedStaffAccStatus, this.addSelectedStaffQualification, this.addSelectedStaffQualificationId, this.addSelectedStaffDesignation, this.addSelectedStaffDOJ, this.addSelectedStaffContact, this.addSelectedStaffEmail, this.addSelectedStaffAddress, this.getSchoolId, this.getAuthToken)
                .subscribe(function (data) {
                $(".disable-element").attr("disabled", false);
                $("#add-staff-button").text("Add details");
                console.log("staff add status===========>>>> ", data);
                if (data.addStaff == true) {
                    customToastr('New staff added!', 'success');
                    materialadmin.AppOffcanvas._handleOffcanvasOpen($(''));
                }
                else if (data.message == 'STAFF_ALREADY_EXIST') {
                    customToastr("Staff already exists with same mobile number or email id, please check it", 'error');
                }
                else {
                    customToastr('Failed!!! Please try again', 'error');
                }
            }, function (error) { return console.log(error); }, function () {
                $(".disable-element").attr("disabled", false);
                $("#add-staff-button").text("Add details");
            });
        }
        else {
            $(".disable-element").attr("disabled", false);
            $("#add-staff-button").text("Add details");
            customToastr("Please fill all the mandatory fields", 'error');
        }
    };
    GroupsComponent.prototype.addDesigSuperadminAccess = function (event) {
        if (event.target.checked) {
            console.log("checked");
            this.addNewDesignationAdminStatus = 1;
        }
        else {
            console.log("not checked");
            this.addNewDesignationAdminStatus = 0;
        }
    };
    GroupsComponent.prototype.addDesigTeacherAccess = function (event) {
        if (event.target.checked) {
            console.log("checked");
            this.addNewDesignationTeacherStatus = 1;
        }
        else {
            console.log("not checked");
            this.addNewDesignationTeacherStatus = 0;
        }
    };
    GroupsComponent.prototype.addDesigDriverAccess = function (event) {
        if (event.target.checked) {
            console.log("checked");
            this.addNewDesignationDriverStatus = 1;
        }
        else {
            console.log("not checked");
            this.addNewDesignationDriverStatus = 0;
        }
    };
    //add new designation
    GroupsComponent.prototype.addNewStaffDesignation = function () {
        var _this = this;
        this.addNewDesignationDefaultStatus = 1;
        console.log("New desig title: ", this.addNewDesignationTitle);
        if (this.formValidator.validateIsData(this.addNewDesignationTitle)) {
            if (this.addNewDesignationAdminStatus == null) {
                this.addNewDesignationAdminStatus = 0;
            }
            if (this.addNewDesignationTeacherStatus == null) {
                this.addNewDesignationTeacherStatus = 0;
            }
            if (this.addNewDesignationDriverStatus == null) {
                this.addNewDesignationDriverStatus = 0;
            }
            this.groupservice.groupsStaffAdddesignation(this.getSchoolId, this.addNewDesignationTitle, this.addNewDesignationAdminStatus, this.addNewDesignationTeacherStatus, this.addNewDesignationDriverStatus, this.getAuthToken)
                .subscribe(function (data) {
                console.log("staff add desig status===========>>>> ", data);
                if (data.addDesignation == true) {
                    customToastr('Designation successfully added', 'success');
                    //materialadmin.AppOffcanvas._handleOffcanvasOpen($(''));
                    _this.addNewDesignationTitle = "";
                    _this.superAdminAccess = false;
                    _this.classTeacherAccess = false;
                    _this.driverAccess = false;
                    _this.editDesignationState = false;
                    _this.getSchoolDesignationList(_this.getSchoolId);
                }
                else if (data.message) {
                    customToastr(data.message, 'error');
                }
                else {
                    customToastr('Failed to add designation, Please try again', 'error');
                }
            }, function (error) { return console.log(error); }, function () { });
        }
        else {
            customToastr("Please provide valid designation name", 'error');
        }
    };
    //add new designation
    GroupsComponent.prototype.editDesignationSubmit = function () {
        var _this = this;
        console.log("edit desig title: ", this.addNewDesignationTitle);
        if (this.formValidator.validateIsData(this.addNewDesignationTitle)) {
            this.groupservice.groupsStaffEditDesignation(this.editingDesignationId, this.addNewDesignationTitle, this.addNewDesignationAdminStatus, this.addNewDesignationTeacherStatus, this.addNewDesignationDriverStatus, this.getAuthToken)
                .subscribe(function (data) {
                console.log("groupsStaffEditDesignation", data);
                if (data.status) {
                    customToastr('Designation successfully updated', 'success');
                    _this.editDesignationState = false;
                    _this.addNewDesignationTitle = "";
                    _this.superAdminAccess = false;
                    _this.classTeacherAccess = false;
                    _this.driverAccess = false;
                    _this.getSchoolDesignationList(_this.getSchoolId);
                }
                else if (data.message) {
                    customToastr(data.message, 'error');
                }
                else {
                    customToastr('Failed to update designation, Please try again', 'error');
                }
            }, function (error) { return console.log(error); }, function () { });
        }
        else {
            customToastr("Please provide valid designation name", 'error');
        }
    };
    GroupsComponent.prototype.editDesignation = function (designation) {
        this.editDesignationState = true;
        this.addNewDesignationTitle = designation.designation;
        this.superAdminAccess = designation.isSuperAdmin == 1;
        this.classTeacherAccess = designation.isClassTeacher == 1;
        this.driverAccess = designation.isDriver == 1;
        this.editingDesignationId = designation.designationId;
        $("#groupbutton9").focus();
    };
    GroupsComponent.prototype.cancelEditDesignation = function () {
        this.addNewDesignationTitle = "";
        this.superAdminAccess = false;
        this.classTeacherAccess = false;
        this.driverAccess = false;
        this.editDesignationState = false;
    };
    GroupsComponent.prototype.editStaffQualificationList = function () {
        var _this = this;
        if (this.formValidator.validateIsData(this.addNewQualificationTitle)) {
            this.groupservice.groupsStaffEditQualification(this.pkStaffQualificationId, this.addNewQualificationTitle, this.getAuthToken)
                .subscribe(function (data) {
                if (data.status) {
                    customToastr('Qualification updated successfully', 'success');
                    _this.addNewQualificationTitle = "";
                    _this.editQualificationState = false;
                    _this.getStaffQualificationList(_this.getSchoolId);
                }
                else if (data.message) {
                    customToastr(data.message, 'error');
                }
                else {
                    customToastr('Failed to update qualification, Please try again', 'error');
                }
            }, function (error) { return console.log(error); }, function () { });
        }
        else {
            customToastr("Please provide qualification name to update", 'error');
        }
    };
    GroupsComponent.prototype.cancelEditQualification = function () {
        this.editQualificationState = false;
        this.addNewQualificationTitle = '';
    };
    GroupsComponent.prototype.startEditQualification = function (qualification) {
        this.editQualificationState = true;
        this.addNewQualificationTitle = qualification.Qualification;
        this.pkStaffQualificationId = qualification.pkStaffQualificationId;
        $("#groupbutton9").focus();
    };
    //add new staff qualification
    GroupsComponent.prototype.addNewStaffQualification = function () {
        var _this = this;
        console.log("New qual:", this.addNewQualificationTitle);
        if (this.formValidator.validateIsData(this.addNewQualificationTitle)) {
            this.groupservice.groupsStaffAddqualification(this.getSchoolId, this.addNewQualificationTitle, this.getAuthToken)
                .subscribe(function (data) {
                if (data.addQualification == true) {
                    customToastr('Qualification added successfully', 'success');
                    _this.addNewQualificationTitle = "";
                    _this.getStaffQualificationList(_this.getSchoolId);
                }
                else if (data.message) {
                    customToastr(data.message, 'error');
                }
                else {
                    customToastr('Failed to add qualification, Please try again', 'error');
                }
            }, function (error) { return console.log(error); }, function () { });
        }
        else {
            customToastr("Please provide qualification name to add", 'error');
        }
    };
    //import student procceed file upload
    GroupsComponent.prototype.importstudent_proceedsheet = function ($event) {
        var _this = this;
        console.log("proceed sheet");
        // get the file name, possibly with path (depends on browser)
        var filename = $("#file-bulkupload").val();
        console.log("get file name: ", filename);
        // check if file extn is the required one
        var extension = filename.replace(/^.*\./, '');
        if (extension == filename) {
            extension = '';
        }
        else {
            extension = extension.toLowerCase();
        }
        if (extension == 'xls') {
            this.proceedxlx = true;
            //service
            var fileList = $event.target.files;
            if (fileList.length > 0) {
                var file = fileList[0];
                var formData = new FormData();
                formData.append('uploadfile', file, file.name);
                this.http.post(this._url + '/uploadxlsjson', formData)
                    .map(function (res) { return res.json(); })
                    .catch(function (error) { return Observable.throw(error); })
                    .subscribe(function (data) {
                    _this.Exceldata = data.ExcelJSONPath;
                    console.log('success: ', data);
                    console.log('ExcelJSON path: ', _this.Exceldata);
                    _this.displayExcelToTable();
                }, function (error) { return console.log("Error happened:", error); });
            }
        }
        else {
            customToastr('File format should be in xls format. Download the template file and use it.', 'error');
            this.proceedxlx = false;
        }
    };
    GroupsComponent.prototype.displayExcelToTable = function () {
        function renderTable(xhrdata) {
            var cols = [];
            var exampleRecord = xhrdata[0];
            var keys = Object.keys(exampleRecord);
            keys.forEach(function (k) {
                cols.push({
                    title: k,
                    data: k
                });
            });
            var table = $('#table-importstudent').DataTable({
                "dom": 'lCfrtip',
                "order": [],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                columns: cols,
                "language": {
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
            table.rows.add(xhrdata).draw();
        }
        //xhr call to retrieve data
        var xhrcall = $.ajax(this.Exceldata);
        //promise syntax to render after xhr completes
        if (xhrcall.done(renderTable)) {
            console.log("Table rendered");
        }
    };
    GroupsComponent.prototype.importstudent_modalproceed = function () {
        var table = $('#table-importstudent').DataTable();
        var allData = table.columns().data();
        console.log("Columns: ", allData);
        //excel sheet to table validation goes here
        //dob validation [columns(1)]
        var dob = [];
        for (var i = 0; i < table.columns(1).data()[0].length; i++) {
            dob.push(table.columns(1).data()[0][i]);
            if (!$.isNumeric(('' + table.columns(1).data()[0][i] + '').replace(/\//g, ''))) {
                if (!this.formValidator.isValidDate(table.columns(1).data()[0][i])) {
                    $("#table-importstudent td").each(function () {
                        //console.log($(this).html());
                        if ($(this).html() == table.columns(1).data()[0][i]) {
                            $(this).addClass("import-error");
                        }
                    });
                }
            }
        }
        //admission date validation [columns(6)]
        var doa = [];
        for (var i = 0; i < table.columns(6).data()[0].length; i++) {
            doa.push(table.columns(6).data()[0][i]);
            if (!$.isNumeric(('' + table.columns(6).data()[0][i] + '').replace(/\//g, ''))) {
                if (!this.formValidator.isValidDate(table.columns(6).data()[0][i])) {
                    $("#table-importstudent td").each(function () {
                        //console.log($(this).html());
                        if ($(this).html() == table.columns(6).data()[0][i]) {
                            $(this).addClass("import-error");
                        }
                    });
                }
            }
        }
        //class number validation [columns(8)]
        var classnumber = [];
        var classData = table.columns(8).data()[0];
        for (var i = 0; i < classData.length; i++) {
            classnumber.push(classData[i]);
            console.log("CLass Number in XLS: ", classData[i]);
            if ($.inArray(classData[i], this.schoolAllClass) == -1) {
                // not found
                $("#table-importstudent td").each(function () {
                    //console.log($(this).html());
                    if ($(this).html() == classData[i]) {
                        $(this).addClass("import-error");
                    }
                });
            }
        }
        console.log("Class number array: ", classnumber);
    };
    GroupsComponent.prototype.proceedImportData = function () {
        $('#table-importstudent').empty();
        $('#table-importstudent').DataTable().destroy();
    };
    //token auth
    GroupsComponent.prototype.isValidToken = function () {
        var _this = this;
        var isValidToken = false;
        var isValidSchool = 0;
        this.loginservice.isValidToken(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            isValidToken = data.isValid;
            console.log("Token Validation in login page: ", isValidToken);
            if (isValidToken == true) {
                //this.router.navigate(['/dashboard']);
                //school id validation///////////////////////////////////////////////
                _this.loginservice.isValidSchoolId(localStorage.getItem('schoolinfo'))
                    .subscribe(function (data) {
                    isValidSchool = data.schoolExist;
                    _this.getSchoolId = localStorage.getItem('schoolinfo');
                    if (isValidSchool != 1) {
                        _this.router.navigate(['/login']);
                    }
                    else {
                        console.log("Stay in page (Group)");
                        _this.initScript();
                        _this.getGroupsClassCategories(_this.getSchoolId);
                        _this.getGroupsAcademicList(_this.getSchoolId);
                        _this.getSchoolDesignationList(_this.getSchoolId);
                        _this.getAllShoolClass(_this.getSchoolId);
                        _this.getStaffQualificationList(_this.getSchoolId);
                    }
                }, function (error) { return console.log(error); }, function () { });
            }
            else {
                _this.router.navigate(['/login']);
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    /*data table operation*/
    /*var url = 'http://www.json-generator.com/api/json/get/cuPctKnQaG?indent=3';
  
     var table = $('#datatable1').DataTable({
     'processing': true,
     'serverSide': true,
     'ajax': {
     type: 'POST',
     'url': url,
     'data': function (d) {
     console.log("DDDDD: ",d);
     console.log("D start: ",d.start);
     console.log("D length: ",d.length);
     //console.log("JSON: ",JSON.stringify( d ));
     //return JSON.stringify( d );
     }
     }
  
     });*/
    /*////////*/
    GroupsComponent.prototype.initScript = function () {
        $(".after").empty();
        $("#device-breakpoints").remove();
        //$('.after').append("<script type='text/javascript' src='inline.bundle.js'></script><script type='text/javascript' src='scripts.bundle.js'></script>");
        $(".after").append("<script>$('.groups').addClass('active');$('.add-qualification').on('change', function() {var value = $(this).val();if(value == 'add-new'){$('#add-qualification-new').popover('show');$('select option[value=0]').attr('selected',true);$('.close-add-qualification').click(function(){$('#add-qualification-new').popover('hide');});}});$('.add-designation').on('change', function() {var value = $(this).val();if(value == 'add-new'){$('#add-designation-new').popover('show');$('select option[value=0]').attr('selected',true);$('.close-add-designation').click(function(){$('#add-designation-new').popover('hide');});}});function customToastr(message,type){toastr.options.hideDuration = 0;toastr.clear();toastr.options.closeButton = true;toastr.options.progressBar = false;toastr.options.debug = false;toastr.options.positionClass = 'toast-top-right';toastr.options.showDuration = 330;toastr.options.hideDuration = 330;toastr.options.timeOut = 5000;toastr.options.extendedTimeOut = 1000;toastr.options.showEasing = 'swing';toastr.options.hideEasing = 'swing';toastr.options.showMethod = 'slideDown';toastr.options.hideMethod = 'slideUp';toastr[type](message, '');return 0;} $('.proceed-import').click(function(){setTimeout(function(){$('.particles-bg1').particleground({dotColor: '#f3f3f3',lineColor: '#ffffff',lineWidth: .3});}, 300);}); $('.bulkUploadStudent').click(function(){setTimeout(function(){$('.particles-bg').particleground({dotColor: '#f3f3f3',lineColor: '#ffffff',lineWidth: .3});}, 300);});</" + "script>");
    };
    /*Data table operation*/
    GroupsComponent.prototype.destroyDataTable = function (table) {
        $('#' + table).DataTable().destroy();
    };
    GroupsComponent.prototype.initializeDataTable = function (table) {
        setTimeout(function () {
            $('#' + table).DataTable();
            //this.scriptInit();
            // $('.after .append-offcanvas').remove();
            // $('.after').append("<div class='append-offcanvas'><script src='assets/js/core/source/AppOffcanvas.js'></"+"script></div>");
        }, 500);
    };
    GroupsComponent.prototype.initializeDataTableNoPagination = function (table) {
        setTimeout(function () {
            $('#' + table).DataTable({
                "paging": false,
                "ordering": false,
                "info": false
            });
        }, 500);
    };
    /*///////////////////*/
    GroupsComponent.prototype.initializeCategoryData = function (initialCatid) {
        var _this = this;
        setTimeout(function () {
            //select category first for displaying initial data
            $("#groups-category option[value=" + initialCatid + "]").prop('selected', true);
            //call datatable here
        }, 500);
        setTimeout(function () { return _this.selectcategoryFunction(initialCatid); }, 500);
    };
    GroupsComponent.prototype.initializeStaffListData = function (initialCatid) {
        var _this = this;
        setTimeout(function () {
            //select category first for displaying initial data
            $("#staff-category option[value=" + initialCatid + "]").prop('selected', true);
            //call datatable here
        }, 500);
        setTimeout(function () { return _this.selectdesignationFunction(initialCatid); }, 500);
    };
    //dynamic datatable server process//////////////////////
    GroupsComponent.prototype.initDataTableServerProcess = function (tableid, schoolId, authToken, academicId, categoryId, classId, sectionId) {
        $('#' + tableid).DataTable({
            destroy: true,
            language: {
                "processing": "<h3 style='color: #2196f3;font-size: 2em;font-weight: 100;'>Please wait...</h3>"
            },
            "processing": true,
            "serverSide": true,
            "ajax": {
                beforeSend: function (request) {
                    request.setRequestHeader("Content-Type", 'application/json');
                    request.setRequestHeader("authToken", authToken);
                },
                url: AppSettings.API_ENDPOINT + '/studentlist',
                type: "POST",
                'data': function (d) {
                    // console.log(d.order);
                    // console.log("Data: ",d);
                    // console.log("Data draw: ",d.draw);
                    // console.log("Data columns: ",d.columns);
                    // console.log("Data order: ",d.order);
                    // console.log("Data start: ",d.start);
                    // console.log("Data length: ",d.length);
                    return JSON.stringify({ "schoolId": localStorage.getItem('schoolinfo'), "academicId": academicId, "categoryId": categoryId, "classId": classId, "sectionId": sectionId, "countFrom": d.start, "lengthOf": d.length });
                } /*,
                success: function(d){
                console.log("Success dataaaaaaaa: ",d);
                //alert("Success");
                },
                error: function(error){
                console.log("Errorr dataaaaaaaa: ",error);
                alert("Error");
                },*/
            },
            "columns": [
                /*{
                  "class":          "studentId",
                  "data":           "studentId",
                },*/
                { "class": "studentId", "data": "rollNumber" },
                { "data": "studentName" },
                { "data": "parentName" },
                { "data": "mobileNumber" },
                { "data": "dateOfBirth" },
                { "data": "gender" },
                { "data": "isAppUser" }
            ]
        });
        /*///////////////*/
    };
    /*/////////////////////////////////////////////////////////////////////////////////*/
    GroupsComponent.prototype.ngOnInit = function () {
        this.isValidToken();
        // $(".dataTables_empty").addClass('hidden');
        // $(".paginate_button").removeClass('disabled');
        //this.initDataTable(this.academicId,categoryId,classId,sectionId);
    };
    /*Sample datatable /////////////////////////////////////////////*/
    GroupsComponent.prototype.initDataTable = function (academicId, categoryId, classId, sectionId) {
        console.log("initDataTable !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        console.log("Init DT auth token: ", localStorage.getItem('usertoken'));
        /*data table test*/
        $('#example').DataTable({
            destroy: true,
            language: {
                /*"processing": "<img src='https://www.criminalwatchdog.com/images/assets/loading.gif' class='img-responsive' style='margin: 0 auto;width: 75px;'>"*/
                "processing": "<h3 style='color: #2196f3;font-size: 2em;font-weight: 100;'>Please wait...</h3>"
            },
            "processing": true,
            "serverSide": true,
            //"ajax": "http://www.json-generator.com/api/json/get/bUgJovrYVu?indent=2",
            "ajax": {
                beforeSend: function (request) {
                    request.setRequestHeader("Content-Type", 'application/json');
                    request.setRequestHeader("authToken", localStorage.getItem('usertoken'));
                },
                url: AppSettings.API_ENDPOINT + '/studentlist',
                //url: "http://www.json-generator.com/api/json/get/bUgJovrYVu?indent=2",  //with draw
                //url: "http://www.json-generator.com/api/json/get/bILDTXwAya?indent=2",  //without draw
                type: "POST",
                //dataType: 'json',
                'data': function (d) {
                    console.log(d.order);
                    console.log("Data: ", d);
                    //console.log("Data json stringify: ",JSON.stringify( d ));
                    console.log("Data draw: ", d.draw);
                    console.log("Data columns: ", d.columns);
                    console.log("Data order: ", d.order);
                    console.log("Data start: ", d.start);
                    console.log("Data length: ", d.length);
                    //return JSON.stringify( d );
                    return JSON.stringify({ "schoolId": localStorage.getItem('schoolinfo'), "academicId": academicId, "categoryId": categoryId, "classId": classId, "sectionId": sectionId, "countFrom": d.start, "lengthOf": d.length });
                    // return ({"schoolId": 20,"academicId":36,"categoryId": 4,"classId": "null","sectionId": "null","countFrom": d.start,"lengthOf":d.length});
                } /*,
                success: function(d){
                  console.log("Success dataaaaaaaa: ",d);
                  //alert("Success");
                },
                error: function(error){
                  console.log("Errorr dataaaaaaaa: ",error);
                  alert("Error");
                },*/
            },
            "columns": [
                { "data": "rollNumber" },
                { "data": "studentName" },
                { "data": "parentName" },
                { "data": "mobileNumber" },
                { "data": "dateOfBirth" },
                { "data": "gender" },
                { "data": "isAppUser" }
            ]
        });
        /*///////////////*/
    };
    /*/////////////////////////////////////////////////////////////////////////////////*/
    GroupsComponent.prototype.getAllShoolClass = function (schoolId) {
        var _this = this;
        //call get all classes in school for validating
        this.groupservice.groupsAvailableAllClass(schoolId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            //console.log("Available classes in the school: ",data.className);
            try {
                for (var i = 0; i < data.className.length; i++) {
                    _this.schoolAllClass.push(data.className[i].classname);
                }
            }
            catch (e) {
            }
            console.log("Available classes in the school: ", _this.schoolAllClass);
        }, function (error) {
            console.log(error);
        }, function () {
        });
    };
    GroupsComponent.prototype.getGroupsClassCategories = function (schoolId) {
        var _this = this;
        console.log("Get school cat school id: ", schoolId);
        this.groupservice.groupsClassCategories(schoolId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            for (var i = 0; i < data.schoolCategory.length; i++) {
                //console.log(data.schoolCategory[i].categoryName);
                _this.schoolCategories.push(data.schoolCategory[i]);
                if (data.schoolCategory.length - 1 == i && _this.schoolCategories.length != 0) {
                    console.log("First category Id: ", _this.schoolCategories[0].categoryId);
                    _this.initializeCategoryData(_this.schoolCategories[0].categoryId);
                }
            }
            console.log("School class categories: ", _this.schoolCategories);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    GroupsComponent.prototype.getGroupsAcademicList = function (schoolId) {
        var _this = this;
        this.groupservice.groupsStudentAcademicList(schoolId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            for (var i = 0; i < data.academicList.length; i++) {
                _this.schoolAcademicList.push(data.academicList[i]);
                if (data.academicList[i].InActive == 0) {
                    _this.schoolAcademicCurrent.push(data.academicList[i]);
                    _this.academicId = data.academicList[i].pkAcademicId;
                    _this.schoolAcademicCurrentYear = data.academicList[i].Year;
                }
            }
            //console.log("School Id: ",this.getSchoolId);
            console.log("School  Academic list: ", _this.schoolAcademicList);
            console.log("School  Academic Current: ", _this.schoolAcademicCurrent);
            console.log("School  Academic Current id: ", _this.academicId);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    //get school designation list
    GroupsComponent.prototype.getSchoolDesignationList = function (schoolId) {
        var _this = this;
        this.schoolDesignationList = [];
        this.groupservice.groupsSchoolDesignationList(schoolId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Designation list: ", data);
            for (var i = 0; i < data.designationList.length; i++) {
                _this.schoolDesignationList.push(data.designationList[i]);
                if (data.designationList.length - 1 == i) {
                    _this.initializeStaffListData(data.designationList[0].designationId);
                }
            }
            console.log("School designation list: ", _this.schoolDesignationList);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    //add new student function
    GroupsComponent.prototype.addNewStudentValidate = function () {
        /*if($( "#genderMale" ).hasClass( "active" )){
          this.addNewStudentgender = 1;
        }else if($( "#genderFemale" ).hasClass( "active" )){
          this.addNewStudentgender = 0;
        }*/
        this.addNewStudentgender = 1;
        //date of birth
        this.studentDOB = $('.studentDOB').val();
        //admission date
        this.studentAdmissionDate = $('.studentAdmissionDate').val();
        // console.log("Student name entered: ",this.studentName);
        // console.log("Student DOB: ",this.studentDOB);
        // console.log("Student Gender: ",this.addNewStudentgender);
        // console.log("Student assign class section id: ",this.selectedSectionAdd);
        // console.log("Student adm date: ", this.studentAdmissionDate);
        // console.log("Student adm number: ", this.studentAdmissionNumber);
        //
        // console.log("Student Parent Name: ", this.studentParentName);
        // console.log("Student Parent Contact: ", this.studentParentContact);
        // console.log("Student Parent Email: ", this.studentParentEmail);
        // console.log("Student Address: ", this.studentAddress);
        // console.log("Student Academic Id: ", this.academicId);
        // console.log("Student SchoolId: ", this.studentSchoolId);
        this.studentGender = this.addNewStudentgender;
        this.studentClassId = this.selectedSectionAdd;
        this.studentAcademicId = this.academicId;
        /*<!-- 'info' 'error' 'warning' 'success' -->*/
        //error validator
        if (!this.formValidator.validateIsData(this.studentName)) {
            customToastr('Please enter student name!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.studentDOB)) {
            customToastr('DOB is missing!', 'error');
        }
        else if (!$.isNumeric(('' + this.studentDOB + '').replace(/\//g, ''))) {
            customToastr('Invalid date of birth!', 'error');
        }
        else if (this.selectedSectionAdd == 0 || this.selectedSectionAdd == null) {
            customToastr('Class is not valid, Please select!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.studentAdmissionDate)) {
            customToastr('Please select admission date!', 'error');
        }
        else if (!$.isNumeric(('' + this.studentAdmissionDate + '').replace(/\//g, ''))) {
            customToastr('Invalid admission date!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.studentAdmissionNumber)) {
            customToastr('Enter admission number!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.studentParentName)) {
            customToastr('Parent name is missing!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.studentParentContact)) {
            customToastr('Please fill the contact number!', 'error');
        }
        else if (!$.isNumeric('' + this.studentParentContact + '') || this.studentParentContact.length > 15) {
            customToastr('Invalid contact number!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.studentAddress)) {
            customToastr('Student address is missing!', 'error');
        }
        else if (this.studentParentEmail.length > 0 && !this.formValidator.validateIsEmail(this.studentParentEmail)) {
            customToastr('Enter valid email!', 'error');
        }
        else {
            this.addNewStudent();
        }
    };
    GroupsComponent.prototype.addNewStudent = function () {
        this.loaderAddStudent = true;
        customToastr('New student added!!!', 'success');
        /* this.groupservice.addNewStudentDetails(this.studentName,this.studentDOB,this.studentGender,this.studentClassId,this.studentAdmissionDate,this.studentAdmissionNumber,this.studentParentName,this.studentParentContact,this.studentParentEmail,this.studentAddress,this.studentAcademicId,this.studentSchoolId,localStorage.getItem('usertoken'))
           .subscribe(
             data => {
               this.addNewStudentStatus = data.success;
               console.log("New student add status: ",this.addNewStudentStatus);
               this.loaderAddStudent = false;
     
               if(this.addNewStudentStatus == 1){
                 this.addNewStudentStatus == 0;
                 customToastr('New student added!!!','success');
     
                 $('#add-stname').val("");$('.studentDOB').val("");$('#add-admdate').val("");$('#add-admnum').val("");$('#st-pname').val("");$('#st-cnum').val("");$('#st-cemail').val("");$('#st-address').val("");
     
                 this.studentName = this.studentDOB = this.studentAdmissionDate = this.studentAdmissionNumber = this.studentParentName = this.studentParentContact = this.studentParentEmail = this.studentAddress = "";
     
               }else {
                 customToastr('Failed to add the student. Please try again!!!','error');
               }
     
             },
             error => console.log(error),
             () => {
             }
           );*/
    };
    GroupsComponent.prototype.addNewStuDataClear = function () {
        this.addNewStudentName = "";
        this.addNewStudentSurName = "";
        this.addNewStudentDOB = "";
        this.addNewStudentBloodGroup = "";
        this.addNewStudentReligion = "";
        this.addNewStudentCaste = "";
        this.addNewStudentNationality = "";
        this.addNewStudentEmail = "";
        this.addNewStudentNumber = "";
        this.addNewStudentAademicid = this.academicId;
        this.addNewStudentAdmissionBase = "";
        this.addNewStudentAdmissionStream = "";
        this.addNewStudentAdmissionNumber = "";
        this.addNewStudentClassId = "";
        this.addNewStudentFatherName = "";
        this.addNewStudentFatherNumber = "";
        this.addNewStudentFatherOccupation = "";
        this.addNewStudentFatherQualification = "";
        this.addNewStudentFatherAddress = "";
        this.addNewStudentMotherName = "";
        this.addNewStudentMotherNumber = "";
        this.addNewStudentMotherOccupation = "";
        this.addNewStudentMotherQualification = "";
        this.addNewStudentMotherSameFatherAddress = "";
        this.addNewStudentMotherAddress = "";
        this.addNewStudentGuardianName = "";
        this.addNewStudentGuardianNumber = "";
        this.addNewStudentGuardianOccupation = "";
        this.addNewStudentGuardianQualification = "";
        this.addNewStudentGuardianSameFatherAddress = "";
        this.addNewStudentGuardianAddress = "";
    };
    //new one////////////////////////////////////////////////////////////////////
    GroupsComponent.prototype.addNewStudentButton = function () {
        //particle background
        $('#particles-add-student').particleground({ dotColor: '#f3f3f3', lineColor: '#ffffff', lineWidth: .3 });
    };
    //get today date
    GroupsComponent.prototype.getTodayDate = function () {
        var getDateNow = new Date();
        var getMonth = getDateNow.getUTCMonth() + 1;
        var getDay = getDateNow.getUTCDate();
        var getYear = getDateNow.getUTCFullYear();
        // this.todayDate = getYear+"/"+getMonth+"/"+getDay;
        return (getDay + "/" + getMonth + "/" + getYear);
        // console.log("Current Date: ",getDateNow);
        // console.log("Current getMonth: ",getMonth);
        // console.log("Current getDay: ",getDay);
        // console.log("Current getYear: ",getYear);
        // console.log("Today: ",this.todayDate);
    };
    GroupsComponent.prototype.addNewStudentFinish = function () {
        this.addNewStudentDOB = $('.addNewStudentDOB').val();
        this.addNewStudentAademicid = this.academicId;
        var addNewStudentAdmissionDate = this.getTodayDate();
        if (this.addNewStudentGender == 'male') {
            this.addNewStudentGender = "1";
        }
        else if (this.addNewStudentGender == 'female') {
            this.addNewStudentGender = "0";
        }
        if (this.addNewStudentMotherSameFatherAddress == true.toString()) {
            this.addNewStudentMotherAddress = this.addNewStudentFatherAddress;
        }
        if (this.addNewStudentGuardianSameFatherAddress == true.toString()) {
            this.addNewStudentGuardianAddress = this.addNewStudentFatherAddress;
        }
        if (!this.formValidator.validateIsData(this.addNewStudentName)) {
            customToastr('Please enter student name!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.addNewStudentGender)) {
            customToastr('Select student gender!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.addNewStudentDOB)) {
            customToastr('Select student DOB!', 'error');
        }
        else if (!$.isNumeric(('' + this.addNewStudentDOB + '').replace(/\//g, ''))) {
            customToastr('Invalid date of birth!', 'error');
        }
        else if (this.addNewStudentBloodGroup == null || !this.formValidator.validateIsData(this.addNewStudentBloodGroup)) {
            customToastr('Select blood group!', 'error');
        }
        else if (this.addNewStudentNationality == null || !this.formValidator.validateIsData(this.addNewStudentNationality)) {
            customToastr('Select Nationality!', 'error');
        }
        else if (this.addNewStudentAdmissionBase == null || !this.formValidator.validateIsData(this.addNewStudentAdmissionBase)) {
            customToastr('Select admission base!', 'error');
        }
        else if (this.addNewStudentAdmissionStream == null || !this.formValidator.validateIsData(this.addNewStudentAdmissionStream)) {
            customToastr('Select Admission stream!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.addNewStudentAdmissionNumber)) {
            customToastr('Enter admission number!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.addNewStudentClassId) || this.addNewStudentClassId == null) {
            customToastr('Select class!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.addNewStudentFatherName)) {
            customToastr('Enter father name!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.addNewStudentFatherNumber)) {
            customToastr('Enter father contact number!', 'error');
        }
        else if (!$.isNumeric(('' + this.addNewStudentFatherNumber + '').replace(/\//g, ''))) {
            customToastr('Invalid father contact number!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.addNewStudentFatherAddress)) {
            customToastr('Enter father address!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.addNewStudentMotherName)) {
            customToastr('Enter mother name!', 'error');
        }
        else if (!this.formValidator.validateIsData(this.addNewStudentMotherNumber)) {
            customToastr('Enter mother contact number!', 'error');
        }
        else if (!$.isNumeric(('' + this.addNewStudentMotherNumber + '').replace(/\//g, ''))) {
            customToastr('Invalid mother contact number!', 'error');
        }
        else {
            var admissionObj = {
                schoolId: this.getSchoolId,
                registrationId: this.addNewStudentAdmissionNumber,
                studentName: this.addNewStudentAdmissionNumber,
                classId: this.addNewStudentClassId,
                parentName: this.addNewStudentFatherName,
                studentEmail: this.addNewStudentEmail,
                emailID: this.addNewStudentEmail,
                dateOfBirth: this.addNewStudentDOB,
                mobileNumber: this.addNewStudentFatherNumber,
                dateOfAdmission: addNewStudentAdmissionDate,
                address: this.addNewStudentFatherAddress,
                fkAcademicId: this.addNewStudentAademicid,
                academicId: this.addNewStudentAademicid,
                surname: this.addNewStudentSurName,
                bloodGroup: this.addNewStudentBloodGroup,
                religion: this.addNewStudentReligion,
                cast: this.addNewStudentCaste,
                nationality: this.addNewStudentNationality,
                studentPhone: this.addNewStudentNumber,
                fatherOccupation: this.addNewStudentFatherOccupation,
                fatherQualification: this.addNewStudentFatherQualification,
                fatherEmail: '',
                motherName: this.addNewStudentMotherName,
                motherPhone: this.addNewStudentMotherNumber,
                motherOccupation: this.addNewStudentMotherOccupation,
                motherQualification: this.addNewStudentMotherQualification,
                motherEmail: '',
                motherAddress: this.addNewStudentMotherAddress,
                guardianName: this.addNewStudentGuardianName,
                guardianPhone: this.addNewStudentGuardianNumber,
                guardianOccupation: this.addNewStudentGuardianOccupation,
                guardianQualification: this.addNewStudentGuardianQualification,
                guardianEmail: '',
                gender: this.addNewStudentGender,
                admissionBase: this.addNewStudentAdmissionBase,
                admissionStream: this.addNewStudentAdmissionStream,
                studentImage: "/image/placeholder.png",
                stopId: '',
                busId: ''
            };
            var imageData = $(".cropme-admission img").attr('src');
            admissionObj.studentImage = imageData;
            this.callNewAdmissionService(admissionObj);
        }
        console.log("addNewStudentName: ", this.addNewStudentName);
        console.log("addNewStudentSurName: ", this.addNewStudentSurName);
        console.log("addNewStudentGender: ", this.addNewStudentGender);
        console.log("addNewStudentDOB: ", this.addNewStudentDOB);
        console.log("addNewStudentBloodGroup: ", this.addNewStudentBloodGroup);
        console.log("addNewStudentReligion: ", this.addNewStudentReligion);
        console.log("addNewStudentCaste: ", this.addNewStudentCaste);
        console.log("addNewStudentNationality: ", this.addNewStudentNationality);
        console.log("addNewStudentEmail: ", this.addNewStudentEmail);
        console.log("addNewStudentNumber: ", this.addNewStudentNumber);
        console.log("addNewStudentAademicid: ", this.addNewStudentAademicid);
        console.log("addNewStudentAdmissionBase: ", this.addNewStudentAdmissionBase);
        console.log("addNewStudentAdmissionStream: ", this.addNewStudentAdmissionStream);
        console.log("addNewStudentAdmissionNumber: ", this.addNewStudentAdmissionNumber);
        console.log("addNewStudentClassId: ", this.addNewStudentClassId);
        console.log("addNewStudentFatherName: ", this.addNewStudentFatherName);
        console.log("addNewStudentFatherNumber: ", this.addNewStudentFatherNumber);
        console.log("addNewStudentFatherOccupation: ", this.addNewStudentFatherOccupation);
        console.log("addNewStudentFatherQualification: ", this.addNewStudentFatherQualification);
        console.log("addNewStudentFatherAddress: ", this.addNewStudentFatherAddress);
        console.log("addNewStudentMotherName: ", this.addNewStudentMotherName);
        console.log("addNewStudentMotherNumber: ", this.addNewStudentMotherNumber);
        console.log("addNewStudentMotherOccupation: ", this.addNewStudentMotherOccupation);
        console.log("addNewStudentMotherQualification: ", this.addNewStudentMotherQualification);
        console.log("addNewStudentMotherSameFatherAddress: ", this.addNewStudentMotherSameFatherAddress);
        console.log("addNewStudentMotherAddress: ", this.addNewStudentMotherAddress);
        console.log("addNewStudentGuardianName: ", this.addNewStudentGuardianName);
        console.log("addNewStudentGuardianNumber: ", this.addNewStudentGuardianNumber);
        console.log("addNewStudentGuardianOccupation: ", this.addNewStudentGuardianOccupation);
        console.log("addNewStudentGuardianQualification: ", this.addNewStudentGuardianQualification);
        console.log("addNewStudentGuardianSameFatherAddress: ", this.addNewStudentGuardianSameFatherAddress);
        console.log("addNewStudentGuardianAddress: ", this.addNewStudentGuardianAddress);
    };
    GroupsComponent.prototype.callNewAdmissionService = function (admissionObj) {
        var _this = this;
        console.log("Finishing admission process..........");
        this.admissionservice.createNewAdmission(admissionObj, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            //callback(data);
            //TODO: Check Status & show status message
            console.log("Finish status: ", data.status);
            if (data.status == true) {
                customToastr('New admission success!!!', 'success');
                _this.addNewStuDataClear();
            }
            else {
                customToastr('Something went wrong! Please try again!!!', 'error');
            }
        }, function (error) {
            //console.log(error)
            //callback(false)
            //TODO: Show error message
            console.log("Error status: ", error);
        });
    };
    GroupsComponent.prototype.importStudentSelectCategory = function (getcatId) {
        var _this = this;
        this.getClassInImport = [];
        this.getSectionInImport = [];
        this.getImportStudentsClassId = 0;
        //get class with cat id
        this.groupservice.groupsAvailableClass(this.getSchoolId, getcatId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Classes in import: ", data);
            for (var i = 0; i < data.className.length; i++) {
                _this.getClassInImport.push(data.className[i]);
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    GroupsComponent.prototype.importStudentSelectClass = function (getclassId) {
        var _this = this;
        this.getSectionInImport = [];
        this.getImportStudentsClassId = 0;
        //get class section with class id
        this.groupservice.groupsAvailableClassSection(getclassId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Sections in import: ", data);
            for (var i = 0; i < data.length; i++) {
                _this.getSectionInImport.push(data[i]);
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    GroupsComponent.prototype.importStudentSelectSection = function (getSectionId) {
        this.getImportStudentsClassId = getSectionId;
        console.log("import student get class section id: ", this.getImportStudentsClassId);
        //file process
        var filename = $("#studentBulkUploadSelectFile").val();
        console.log("get file name: ", filename);
        // check if file extn is the required one
        var extension = filename.replace(/^.*\./, '');
        if (extension == filename) {
            extension = '';
        }
        else {
            extension = extension.toLowerCase();
        }
        if (extension == 'xls') {
            console.log("Get File format: ", extension);
            this.proceedxlx = true;
        }
        else {
            //console.log("File format should be in XLS format, Get File format: ",extension);
            //customToastr('File format should be in xls format. Download the template file and use it.','error');
            this.proceedxlx = false;
        }
        if (this.proceedxlx == false) {
            this.getImportStudentProceedStatus = false;
        }
        else {
            this.getImportStudentProceedStatus = true;
        }
    };
    GroupsComponent.prototype.importStudentProceed = function () {
        var _this = this;
        //particles
        $('.particles-bg1').particleground({
            dotColor: '#f3f3f3',
            lineColor: '#ffffff',
            lineWidth: .3
        });
        //file process
        var filename = $("#studentBulkUploadSelectFile").val();
        console.log("get file name: ", filename);
        // check if file extn is the required one
        var extension = filename.replace(/^.*\./, '');
        if (extension == filename) {
            extension = '';
        }
        else {
            extension = extension.toLowerCase();
        }
        if (extension == 'xls') {
            console.log("Get File format: ", extension);
            this.proceedxlx = true;
        }
        else {
            //console.log("File format should be in XLS format, Get File format: ",extension);
            //customToastr('File format should be in xls format. Download the template file and use it.','error');
            this.proceedxlx = false;
        }
        //validation
        if (this.getImportStudentsClassId == 0) {
            this.getImportStudentProceedStatus = false;
            customToastr('Please select the class!!!', 'error');
        }
        else if (this.proceedxlx == false) {
            this.getImportStudentProceedStatus = false;
            //importStudentProceedCanvas
            customToastr('File format should be in xls format. Download the template file and use it.', 'error');
        }
        else {
            this.getImportStudentProceedStatus = true;
            this.loader_import_student = false;
            this.fileList[0].schoolId = this.getSchoolId;
            this.fileList[0].classId = this.getImportStudentsClassId;
            //file processing
            console.log("File event: ", this.fileList);
            console.log("File: ", this.fileList[0]);
            if (this.fileList.length > 0) {
                var file = this.fileList[0];
                var formData = new FormData();
                formData.append('uploadfile', file, file.name);
                console.log("file: File:  ", file);
                this.destroyDataTable('new-admission-import-table');
                this.importStudentListArray = [];
                this.http.post(this._url + '/uploadxlsjson?schoolId=' + this.getSchoolId + "&classId=" + this.getImportStudentsClassId, formData)
                    .map(function (res) { return res.json(); })
                    .catch(function (error) { return Observable.throw(error); })
                    .subscribe(function (data) {
                    //this.Exceldata = data.ExcelJSONPath;
                    _this.loader_import_student = true;
                    console.log('success: ', data);
                    for (var i = 0; i < data.length; i++) {
                        _this.importStudentListArray.push(data[i]);
                        if (data.length - 1 == i) {
                            _this.initializeDataTableNoPagination('new-admission-import-table');
                        }
                    }
                    //console.log('ExcelJSON path: ', this.Exceldata);
                    //importStudentListArray
                    /* if(data.staffsList.length-1  == i){
                     this.initializeDataTable('new-admission-import-table');
                     }*/
                }, function (error) { return console.log("Error happened:", error); });
            }
        }
    };
    GroupsComponent.prototype.resolveErrors = function () {
        $('#new-admission-import-table .import-error').each(function () {
            var getattr = $(this).attr('name');
            $(this).html("");
            if (getattr == 'dob' || getattr == 'admissiondate') {
                $(this).html("<div class='form-group' style='margin: 0;'><label for='admission-student-name'  style='opacity: initial;'>" + getattr + "</label><input type='text' class='form-control' style='color: white;font-size: 13px;border-color: white;height: 15px;'  data-provide='datepicker' data-date-format='dd/mm/yyyy' data-date-end-date='0d'></div>");
            }
            else if (getattr == 'parentcontact') {
                $(this).html("<div class='form-group' style='margin: 0;'><label for='admission-student-name'  style='opacity: initial;'>" + getattr + "</label><input class='form-control' type='number' style='color: white;font-size: 13px;border-color: white;height: 15px;'></div>");
            }
            else if (getattr == 'gender') {
                $(this).html("<div class='form-group' style='margin: 0;'><label for='admission-student-name'  style='opacity: initial;'>" + getattr + "</label><select  class='form-control' style='text-transform: initial;color: white;font-size: 13px;'><option value='m'>M</option><option value=''>F</option></select></div>");
            }
            else {
                $(this).html("<div class='form-group' style='margin: 0;'><label for='admission-student-name'  style='opacity: initial;'>" + getattr + "</label><input class='form-control' type='text' style='color: white;font-size: 13px;border-color: white;height: 15px;'></div>");
            }
            $(this).removeClass('import-error');
            $(this).addClass('import-edit');
        });
    };
    //resolve errors and proceed data
    GroupsComponent.prototype.resolveErrorsProceedData = function () {
        //validate get email function [jquery]
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        //check for duplicates in array [for more than twice]
        function moreThanOnce(array, searched) {
            var occurence = 0;
            for (var i = 0; i < array.length; i++) {
                if (array[i] === searched)
                    occurence++;
            }
            return occurence;
        }
        var getStudentClassIdArray = [];
        var getStudentAdmissionIdArray = [];
        $('#new-admission-import-table .import-edit').each(function () {
            var getAttr = $(this).attr("name");
            if (getAttr == 'gender') {
                var getInputValue = $(this).find('select').val();
            }
            else {
                var getInputValue = $(this).find('input').val();
            }
            $(this).html("");
            $(this).html(getInputValue);
            $(this).removeClass('import-edit');
            //to get all imported student classid
            $('.import-students-classids').each(function () {
                console.log("Get all classId: ", $(this).html());
                if ($(this).html() != '') {
                    getStudentClassIdArray.push($(this).html());
                }
            });
            //to get all imported student admisssion id
            $('.import-students-admissionids').each(function () {
                console.log("Get all admission id: ", $(this).html());
                if ($(this).html() != '') {
                    getStudentAdmissionIdArray.push($(this).html());
                }
            });
            //is data validation
            if (getInputValue == "" || getInputValue == null || getInputValue == undefined || getInputValue.replace(/\s/g, '').length == 0) {
                $(this).addClass('import-error');
            }
            else {
                //$(this).removeClass('import-edit');
                if (getAttr == 'classid') {
                    //getInputValue
                    //getStudentClassIdArray
                    //console.log("innnnnnnnnnnnnnnnn clid: ",getStudentClassIdArray);
                    if (moreThanOnce(getStudentClassIdArray, getInputValue) > 1) {
                        $(this).addClass('import-error');
                    }
                }
                if (getAttr == 'admissionid') {
                    //getInputValue
                    //getStudentAdmissionIdArray
                    //console.log("innnnnnnnnnnnnnnnn adid",getStudentAdmissionIdArray);
                    if (moreThanOnce(getStudentAdmissionIdArray, getInputValue) > 1) {
                        $(this).addClass('import-error');
                    }
                }
                else {
                    $(this).removeClass('import-edit');
                }
            }
            //email validation
            if (getAttr == 'parentemail') {
                if (validateEmail(getInputValue) != true) {
                    $(this).addClass('import-error');
                }
                else {
                    $(this).removeClass('import-edit');
                }
            }
        }).promise().done(function () {
            var errorValue = 0;
            console.log("All was done");
            $('#new-admission-import-table .import-error').each(function () {
                errorValue++;
            });
            console.log("Errors: ", errorValue);
            $('.resolve-proceed-data1').show();
            $('.resolve-proceed-data2').hide();
            //if no errors
            if (errorValue == 0) {
                //table to json
                var $table = $("#new-admission-import-table"), rows = [], header = [];
                $table.find("thead th").each(function () {
                    header.push($(this).html());
                });
                $table.find("tbody tr").each(function () {
                    var row = {};
                    $(this).find("td").each(function (i) {
                        var key = header[i], value = $(this).html();
                        row[key] = value;
                    });
                    rows.push(row);
                });
                //console.log(JSON.stringify(rows));
                $('.get-json-value').html();
                $('.get-json-value').html(JSON.stringify(rows));
                $('.resolve-proceed-data1').hide();
                $('.resolve-proceed-data2').show();
            }
        });
    };
    //validate for duplicate data in temp table
    GroupsComponent.prototype.validateImportStudentByDuplicate = function () {
        var _this = this;
        this.loader_import_student = false;
        var getJsonStringData = $('.get-json-value').html();
        var getSchoolId = this.getSchoolId;
        var getImportStudentsClassId = this.getImportStudentsClassId;
        var getCurrentAcademicId = this.academicId;
        console.log("Get json string data in fun: ", getJsonStringData);
        console.log("Get imported class id: ", getImportStudentsClassId);
        //insert json data in to temp db for validate duplicate data
        this.groupservice.validateJsonDataOnProceed(getJsonStringData, getSchoolId, getImportStudentsClassId, getCurrentAcademicId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            _this.loader_import_student = true;
            console.log("data: ", data);
            customToastr('Import studendet success!!!', 'success');
            $('.import-student-canvas').click();
            var getcatId = "";
            _this.importStudentSelectCategory(getcatId);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    //get imported file
    GroupsComponent.prototype.getImportFile = function ($event) {
        this.fileList = $event.target.files;
        if (this.getImportStudentsClassId == 0) {
            this.getImportStudentProceedStatus = false;
        }
        else {
            this.getImportStudentProceedStatus = true;
        }
    };
    GroupsComponent = __decorate([
        Component({
            selector: 'app-groups',
            templateUrl: './groups.component.html',
            styleUrls: [
                'assets/css/theme-default/bootstrap.css?1422792965',
                'assets/css/theme-default/materialadmin.css?1425466319',
                'assets/css/theme-default/font-awesome.min.css?1422529194',
                'assets/css/theme-default/material-design-iconic-font.min.css?1421434286',
                'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
                'assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858',
                'assets/css/theme-default/libs/toastr/toastr.css?1425466569',
                '_crop/css/style.css',
                '_crop/css/style-example.css',
                '_crop/css/jquery.Jcrop.css',
                '_upload/css/component.css',
                './groups.component.css'
            ],
            encapsulation: ViewEncapsulation.None,
            providers: [GroupService, FormValidator, LoginService, AdmissionService]
        }), 
        __metadata('design:paramtypes', [NgZone, AdmissionService, Router, GroupService, FormValidator, Http, ElementRef, Renderer, LoginService, ElementRef])
    ], GroupsComponent);
    return GroupsComponent;
}());
//# sourceMappingURL=F:/BW_PR/src/app/groups/groups.component.js.map