var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
export var FormValidator = (function () {
    function FormValidator() {
    }
    //check is there any data
    FormValidator.prototype.validateIsData = function (data) {
        if (data && data.replace(/\s/g, '').length != 0) {
            return true;
        }
        else {
            return false;
        }
    };
    //validate email address
    FormValidator.prototype.validateIsEmail = function (email) {
        if (email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        else
            return false;
    };
    //validate date in yyyy-mm-dd format
    FormValidator.prototype.isValidDate = function (dateString) {
        var regEx = /^\d{4}-\d{2}-\d{2}$/;
        return dateString.match(regEx) != null;
    };
    //validate date in dd/mm/yyyy format
    FormValidator.prototype.isValidDateStr = function (strDate) {
        var myDateStr = new Date(strDate);
        if (!isNaN(myDateStr.getMonth())) {
            return true;
        }
        return false;
    };
    FormValidator.prototype.validateMobileNumber = function (mobileNumber) {
        var pattern = /^[7-9][0-9]{9}$/;
        if (pattern.test(mobileNumber)) {
            return true;
        }
        else {
            return false;
        }
    };
    FormValidator = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [])
    ], FormValidator);
    return FormValidator;
}());
//# sourceMappingURL=F:/BW_PR/src/app/form.validator.js.map