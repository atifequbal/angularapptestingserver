var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import 'rxjs/Rx';
import { AppSettings } from "../../config/config";
export var SettingService = (function () {
    function SettingService(http) {
        this.http = http;
        this._url = AppSettings.API_ENDPOINT;
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        //this.headers.append('authToken','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJJZCI6MjAsImFkbWluSWQiOjF9.mQdPeis69REyybDGNFyVj2WODS3NUo7z_suPndupw0A');
        this.headers.append('authToken', localStorage.getItem('usertoken'));
    }
    SettingService.prototype.fetchSchoolDetails = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/settings/schoolDetails', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.fetchOverViewDetails = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/settings/overview', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.fetchAccountDetails = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/settings/account', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.fetchinactivestudent = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/settings/deactivatedStudentList', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.fetchinactivestaff = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/settings/deactivatedStaffList', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.fetchFeatureList = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/settings/FeatureList', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.fetchUpdateFeatureList = function (id, status) {
        var json = JSON.stringify({ pkSchoolFeatureId: id, ActiveStatus: status });
        var params = json;
        return this.http.post(this._url + '/settings/UpdateFeature', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.fetchUpdateUserList = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/settings/UserList', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.fetchSearchUserList = function (a) {
        var json = JSON.stringify({ search: a });
        var params = json;
        return this.http.post(this._url + '/settings/searchUser', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.updateSchoolDetails = function (schoolname, mobnumber, mailid, add, area, state, pincode, description, statusparent, statustaff) {
        var json = JSON.stringify({ SchoolName: schoolname, mobNumber: mobnumber, email: mailid, address: add, locality: area, state: state, pinCode: pincode, Description: description, ParentmessageLimit: statusparent, staffMessageLimit: statustaff });
        var params = json;
        return this.http.post(this._url + '/settings/updateSchoolDetails', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.addAdminUserDetails = function (mailid, username, psswd, cnfmpassword, mobnumber, accesstype, flist) {
        var json = JSON.stringify({ emailId: mailid, username: username, password: psswd, cnfmpassword: cnfmpassword, PhoneNumber: mobnumber, AccessType: accesstype, featurelist: flist });
        var params = json;
        return this.http.post(this._url + '/settings/addNewUser', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.getAdminUserDetails = function (schooladminid) {
        var json = JSON.stringify({ pkSchoolAdminsId: schooladminid });
        var params = json;
        return this.http.post(this._url + '/settings/getUserDetails', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.getAdminUserFeatureDetails = function (schooladmin) {
        var json = JSON.stringify({ pkSchoolAdminsId: schooladmin });
        var params = json;
        return this.http.post(this._url + '/settings/getUserFeatureDetails', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.updateAdminUserDetails = function (mailid, mobnumber, accesstype, status, fuserlst, schoolId) {
        var json = JSON.stringify({ emailId: mailid, PhoneNumber: mobnumber, AccessType: accesstype, AdminAccessStatus: status, featurelist: fuserlst, SchoolAdminsId: schoolId });
        var params = json;
        return this.http.post(this._url + '/settings/UpdateUserFeatureDetails', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.getAdminUserFeatureList = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/settings/UserFeatureList', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.reactivateStudent = function (id) {
        var json = JSON.stringify({ studentId: id });
        var params = json;
        return this.http.post(this._url + '/settings/ReactiveStudent', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.reactivatestaff = function (id) {
        var json = JSON.stringify({ staffId: id });
        var params = json;
        return this.http.post(this._url + '/settings/ReactiveStaff', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.deleteAdminUserDetails = function (deleteAdminUser) {
        var json = JSON.stringify({ pkSchoolAdminsId: deleteAdminUser });
        var params = json;
        return this.http.post(this._url + '/settings/DeleteUser', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService.prototype.activateAdmin = function (adminId) {
        var json = JSON.stringify({ pkSchoolAdminsId: adminId });
        var params = json;
        return this.http.post(this._url + '/settings/activateAdminUser', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    SettingService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [Http])
    ], SettingService);
    return SettingService;
}());
//# sourceMappingURL=F:/BW_PR/src/app/settings/settings.service.js.map