var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewEncapsulation } from '@angular/core';
import { SettingService } from "./settings.service";
import { Router } from "@angular/router";
import { LoginService } from "../login/login.service";
export var SettingsComponent = (function () {
    function SettingsComponent(router, loginservice, settingservice) {
        this.router = router;
        this.loginservice = loginservice;
        this.settingservice = settingservice;
        this.academicPermissions = {};
        //schooldetails
        this.schoolDetails = {};
        this.schoolName = "";
        this.schoolImage = "";
        this.accountType = "";
        this.totalStudent = "";
        this.phoneNumber = 0;
        this.address = "";
        this.emailId = "";
        this.parentChatMessageLimit = "";
        this.staffChatMessageLimit = "";
        this.description = "";
        //chat status
        this.parentChatStatus = true;
        this.staffChatStatus = true;
        this.parentChatMessageLimitCount = 0;
        this.staffChatMessageLimitCount = 0;
        this.parentChatStatusInput = true;
        this.staffChatStatusInput = true;
        //overviewdetails
        this.ActiveStaff = 0;
        this.InactiveStaff = 0;
        this.InactiveStudent = 0;
        this.ActiveStudent = 0;
        this.Employee = 0;
        this.AdminUser = 0;
        this.selecteduserId = 0;
        this.adminId = 0;
        //getaccdetails inactive student list
        this.studentName = "";
        this.parentName = "";
        this.mobileNumber = "";
        this.classNumber = "";
        this.reason = "";
        this.studentList = [];
        this.staffList = [];
        this.FeatureList = [];
        this.designation = "";
        this.staffName = "";
        this.PercentageActiveStaff = "";
        this.PercentageActiveStudent = "";
        this.PercentageInactiveStaff = "";
        this.PercentageInactiveStudent = "";
        //token
        this.getSchoolId = localStorage.getItem('schoolinfo');
        //featurelist
        this.bluewingsftrelst = [];
        //ActiveStatus = "";
        //updatefeaturelist
        this.pkSchoolFeatureId = "";
        //userlist
        this.userlst = [];
        //updateschool
        this.updateschool = {
            schoolName: "",
            phoneNumber: "",
            emailId: "",
            address: "",
            address2: "",
            locality: "",
            state: "",
            pincode: "",
            description: "",
            parentChatMessageLimit: "",
            staffChatMessageLimit: ""
        };
        //adduser
        this.adduser = { emailId: "", username: "", password: "", cnfmpassword: "", PhoneNumber: "", AccessType: "" };
        this.featurelist = true;
        this.getuserdetails = { emailId: "", adminPhoneNumber: "", pkSchoolAdminsId: "", AdminAccessStatus: "", AccessType: "" };
        //add user feature list
        this.getuserfeaturelist = [];
        this.getuserfeaturedetails = [];
        this.select = [];
        this.reactivatestudetails = { accountType: "", parentName: "", mobileNumber: "", emailId: "", reason: "", studentId: "" };
    }
    //token auth
    SettingsComponent.prototype.isValidToken = function () {
        var _this = this;
        var isValidToken = false;
        var isValidSchool = 0;
        this.loginservice.isValidToken(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            isValidToken = data.isValid;
            if (isValidToken == true) {
                _this.loginservice.isValidSchoolId(localStorage.getItem('schoolinfo'))
                    .subscribe(function (data) {
                    isValidSchool = data.schoolExist;
                    _this.getSchoolId = localStorage.getItem('schoolinfo');
                    if (isValidSchool != 1) {
                        _this.router.navigate(['/login']);
                    }
                    else {
                    }
                }, function (error) { return console.log(error); }, function () {
                });
            }
            else {
                _this.router.navigate(['/login']);
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.getSchoolDetails = function () {
        var _this = this;
        this.settingservice.fetchSchoolDetails()
            .subscribe(function (data) {
            _this.schoolName = data.schoolName;
            _this.schoolImage = data.schoolImage;
            _this.accountType = data.accountType;
            _this.totalStudent = data.totalStudent;
            _this.phoneNumber = data.phoneNumber;
            _this.address = data.address;
            _this.emailId = data.emailId;
            _this.parentChatMessageLimit = data.parentChatMessageLimit;
            _this.staffChatMessageLimit = data.staffChatMessageLimit;
            _this.description = data.description;
            _this.parentChatMessageLimitCount = data.parentChatMessageLimit;
            _this.staffChatMessageLimitCount = data.staffChatMessageLimit;
            if (data.parentChatMessageLimit == 'UNLIMITED') {
                _this.parentChatStatus = false;
                _this.parentChatStatusInput = false;
            }
            else {
                _this.parentChatStatus = true;
                _this.parentChatStatusInput = true;
            }
            if (data.staffChatMessageLimit == 'UNLIMITED') {
                _this.staffChatStatus = false;
                _this.staffChatStatusInput = false;
            }
            else {
                _this.staffChatStatus = true;
                _this.staffChatStatusInput = true;
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.getOverViewDetails = function () {
        var _this = this;
        this.settingservice.fetchOverViewDetails()
            .subscribe(function (data) {
            _this.ActiveStaff = data.ActiveStaff;
            _this.InactiveStaff = data.InactiveStaff;
            _this.InactiveStudent = data.InactiveStudent;
            _this.ActiveStudent = data.ActiveStudent;
            _this.Employee = data.InactiveAdminUser;
            _this.AdminUser = data.ActiveAdminUser;
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.getAccountDetails = function () {
        var _this = this;
        this.settingservice.fetchAccountDetails()
            .subscribe(function (data) {
            _this.ActiveStudent = data.ActiveStudent;
            _this.InactiveStudent = data.InactiveStudent;
            _this.ActiveStaff = data.ActiveStaff;
            _this.InactiveStaff = data.InactiveStaff;
            _this.PercentageActiveStaff = data.PercentageActiveStaff;
            _this.PercentageActiveStudent = data.PercentageActiveStudent;
            _this.PercentageInactiveStaff = data.PercentageInactiveStaff;
            _this.PercentageInactiveStudent = data.PercentageInactiveStudent;
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.getInactiveStudentList = function () {
        //$('#deactstudentable').DataTable().destroy();
        var _this = this;
        this.settingservice.fetchinactivestudent()
            .subscribe(function (data) {
            _this.studentList = data;
            setTimeout(function () {
                $('#deactstudentable').DataTable();
            }, 100);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.getInactiveStaffList = function () {
        //$('#deactstudentable').DataTable().destroy();
        var _this = this;
        this.settingservice.fetchinactivestaff()
            .subscribe(function (data) {
            _this.staffList = data;
            setTimeout(function () {
                $('#deactstafftable').DataTable();
            }, 100);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.getFeatureList = function () {
        var _this = this;
        this.destroyDataTable('feature-table');
        this.settingservice.fetchFeatureList()
            .subscribe(function (data) {
            var blueFeature = data[0];
            for (var i = 0; i < blueFeature.length; i++) {
                _this.bluewingsftrelst.push(blueFeature[i]);
                if (blueFeature.length - 1 == i) {
                    _this.initializeDataTable('feature-table');
                }
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.updateFeatureList = function (flist, status) {
        this.settingservice.fetchUpdateFeatureList(flist.pkSchoolFeatureId, status)
            .subscribe(function (data) {
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.destroyDataTable = function (table) {
        $('#' + table).DataTable().destroy();
    };
    SettingsComponent.prototype.initializeDataTable = function (table) {
        setTimeout(function () {
            $('#' + table).DataTable({
                "order": [[0, 'asc']],
            });
            //this.scriptInit();
        }, 500);
    };
    SettingsComponent.prototype.getUserList = function () {
        var _this = this;
        console.log("AAAAAAAAAA");
        this.destroyDataTable('userAdmin-table');
        this.userlst = [];
        this.settingservice.fetchUpdateUserList()
            .subscribe(function (data) {
            console.log("BBBBBBBBBBBBBB");
            var uList = data[0];
            for (var i = 0; i < uList.length; i++) {
                _this.userlst.push(uList[i]);
                if (uList.length - 1 == i) {
                    _this.initializeDataTable('userAdmin-table');
                }
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.searchUserList = function () {
        var _this = this;
        var a = $('#groupbutton10').val();
        this.settingservice.fetchSearchUserList(a)
            .subscribe(function (data) {
            _this.userlst = data[0];
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.getCurrentSchoolDetails = function () {
        var _this = this;
        this.settingservice.fetchSchoolDetails()
            .subscribe(function (data) {
            _this.updateschool = data;
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.staffChat = function (status) {
        this.staffChatStatusInput = status;
    };
    SettingsComponent.prototype.stuChat = function (status) {
        this.parentChatStatusInput = status;
    };
    SettingsComponent.prototype.updateLatestSchoolDetails = function () {
        var _this = this;
        $("#updatebtndisable").text("Please wait...");
        $("#updatebtndisable").attr('disabled', true);
        if (this.validateRequiredField(this.updateschool.schoolName)
            && this.validateRequiredField(this.updateschool.phoneNumber)
            && this.validateRequiredField(this.updateschool.emailId)
            && this.validateRequiredField(this.updateschool.address)
            && this.validateRequiredField(this.updateschool.locality)
            && this.validateRequiredField(this.updateschool.state)
            && this.validateRequiredField(this.updateschool.pincode)
            && this.validateRequiredField(this.updateschool.description)) {
            if (this.staffChatStatusInput == true) {
                this.updateschool.staffChatMessageLimit = $('#numchatlimitst').val();
            }
            else {
                this.updateschool.staffChatMessageLimit = "UNLIMITED";
            }
            if (this.parentChatStatusInput == true) {
                this.updateschool.parentChatMessageLimit = $('#numchatlimit').val();
            }
            else {
                this.updateschool.parentChatMessageLimit = "UNLIMITED";
            }
            if ($('#phonenum').val().length == 10) {
                this.settingservice.updateSchoolDetails(this.updateschool.schoolName, this.updateschool.phoneNumber, this.updateschool.emailId, this.updateschool.address, this.updateschool.locality, this.updateschool.state, this.updateschool.pincode, this.updateschool.description, this.updateschool.parentChatMessageLimit, this.updateschool.staffChatMessageLimit)
                    .subscribe(function (data) {
                    if (data.status == true) {
                        $("#updatebtndisable").text("Update Changes");
                        $("#updatebtndisable").attr('disabled', false);
                        customToastr("School details updated successfully", "success");
                        $(".backdrop").trigger("click");
                        _this.getSchoolDetails();
                    }
                    else {
                        customToastr("School details not updated successfully please try again", "error");
                    }
                }, function (error) { return console.log(error); }, function () {
                });
            }
            else {
                $("#updatebtndisable").text("Update Changes");
                $("#updatebtndisable").attr('disabled', false);
                customToastr("Mobile number should be 10 digits only", "error");
            }
        }
        else {
            $("#updatebtndisable").text("Update Changes");
            $("#updatebtndisable").attr('disabled', false);
            customToastr("Please fill All the fields ", "error");
        }
    };
    SettingsComponent.prototype.showHide = function (event) {
        if (event == "SUPER_ADMIN") {
            this.featurelist = false;
        }
        else if (event == "SUB_ADMIN") {
            this.featurelist = true;
            this.adminUserPopUpExistingDetails(this.getuserdetails, false);
        }
    };
    SettingsComponent.prototype.showHideAddUser = function (eventadduser) {
        if (eventadduser == "SUPER_ADMIN") {
            this.featurelist = false;
        }
        else if (eventadduser == "SUB_ADMIN") {
            this.featurelist = true;
        }
    };
    SettingsComponent.prototype.addUserDetails = function () {
        var _this = this;
        $("#adduserbuttondsble").text("Please wait...");
        $("#adduserbuttondsble").attr('disabled', true);
        var flist = [];
        if (this.adduser.AccessType == "SUB_ADMIN") {
            this.getuserfeaturelist.forEach(function (feature) {
                if (feature.select == true) {
                    flist.push({ pkSchoolFeatureId: feature.pkSchoolFeatureId, AdminAccessStatus: "ENABLED" });
                }
                else {
                    flist.push({ pkSchoolFeatureId: feature.pkSchoolFeatureId, AdminAccessStatus: "DISABLED" });
                }
            });
        }
        if (this.adduser.emailId != null && this.adduser.emailId != "" && this.adduser.emailId != undefined && this.adduser.username != null && this.adduser.username != "" && this.adduser.username != undefined && this.adduser.password != null && this.adduser.password != "" && this.adduser.password != undefined && this.adduser.cnfmpassword != null && this.adduser.cnfmpassword != "" && this.adduser.cnfmpassword != undefined && this.adduser.PhoneNumber != null && this.adduser.PhoneNumber != "" && this.adduser.PhoneNumber != undefined) {
            if ($('#adduserphnum').val().length == 10) {
                if (this.adduser.password != this.adduser.cnfmpassword) {
                    $("#adduserbuttondsble").text("ADD USER");
                    $("#adduserbuttondsble").attr('disabled', false);
                    customToastr("password do not match", "error");
                }
                else {
                    this.settingservice.addAdminUserDetails(this.adduser.emailId, this.adduser.username, this.adduser.password, this.adduser.cnfmpassword, this.adduser.PhoneNumber, this.adduser.AccessType, flist)
                        .subscribe(function (data) {
                        if (data.status == true) {
                            customToastr("New user added successfully", "success");
                            $("#adduser").trigger("click");
                            _this.getUserList();
                        }
                        else if (data.message == "EXIST") {
                            $("#adduserbuttondsble").text("ADD USER");
                            $("#adduserbuttondsble").attr('disabled', false);
                            customToastr("user already exist", "error");
                        }
                        else {
                            customToastr("user is not added successfully please try again ", "error");
                        }
                    }, function (error) { return console.log(error); }, function () {
                    });
                }
            }
            else {
                $("#adduserbuttondsble").text("ADD USER");
                $("#adduserbuttondsble").attr('disabled', false);
                customToastr("Mobile number should be 10 digits only ", "error");
            }
        }
        else {
            $("#adduserbuttondsble").text("ADD USER");
            $("#adduserbuttondsble").attr('disabled', false);
            customToastr("Please provide all details to add admin ", "error");
        }
    };
    SettingsComponent.prototype.editUserPopup = function () {
        $('#editUserModal').modal('show');
    };
    SettingsComponent.prototype.adminUserPopUpExistingDetails = function (uyt, isFirstTime) {
        var _this = this;
        this.getuserdetails = uyt;
        if (isFirstTime) {
            if (uyt.AccessType == "SUPER_ADMIN")
                this.featurelist = false;
            else if (uyt.AccessType == "SUB_ADMIN") {
                this.featurelist = true;
            }
        }
        $("#edituser").text("UPDATE USER");
        $('#edituser').attr('disabled', false);
        this.getuserdetails = JSON.parse(JSON.stringify(uyt));
        this.selecteduserId = uyt.pkSchoolAdminsId;
        this.settingservice.getAdminUserFeatureDetails(uyt.pkSchoolAdminsId)
            .subscribe(function (data) {
            _this.getuserfeaturelist = data;
            $("#edituserFeatures").html("");
            for (var i = 0; i < data.length; i++) {
                if (_this.getuserfeaturelist[i].AdminAccessStatus == "ENABLED") {
                    var h = $('<label class="checkbox-inline checkbox-styled checkbox-primary">' +
                        '<input type="checkbox" class="featureBox" id="updatefeature" value="option1" name="featured" ><span></span>' +
                        '</label>');
                    h.find("span").text(_this.getuserfeaturelist[i].BluewingsFeatureDescription);
                    h.find('input').attr("checked", true);
                    h.find('input').attr("fId", _this.getuserfeaturelist[i].pkSchoolFeatureId);
                    $("#edituserFeatures").append(h);
                }
                else {
                    var h = $('<label class="checkbox-inline checkbox-styled checkbox-primary">' +
                        '<input type="checkbox" class="featureBox" id="updatefeature" value="option1" name="featured" ><span></span>' +
                        '</label>');
                    h.find("span").text(_this.getuserfeaturelist[i].BluewingsFeatureDescription);
                    h.find('input').attr("checked", false);
                    h.find('input').attr("fId", _this.getuserfeaturelist[i].pkSchoolFeatureId);
                    $("#edituserFeatures").append(h);
                }
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.updateAdminUser = function () {
        var _this = this;
        $("#edituser").text("Please wait...");
        $("#edituser").attr('disabled', true);
        var fuserlst = [];
        $("#edituserFeatures").find(".featureBox").map(function (i, ele) {
            var f = $(ele);
            var id = f.attr("fId");
            var status = f.prop('checked');
            if (status) {
                fuserlst.push({ pkSchoolFeatureId: id, AdminAccessStatus: "ENABLED" });
            }
            else {
                fuserlst.push({ pkSchoolFeatureId: id, AdminAccessStatus: "DISABLED" });
            }
        });
        if (this.validateRequiredField(this.getuserdetails.emailId)
            && this.validateRequiredField(this.getuserdetails.adminPhoneNumber)) {
            if ($('#mobnum').val().length == 10) {
                this.settingservice.updateAdminUserDetails(this.getuserdetails.emailId, this.getuserdetails.adminPhoneNumber, this.getuserdetails.AccessType, this.getuserdetails.AdminAccessStatus, fuserlst, this.getuserdetails.pkSchoolAdminsId)
                    .subscribe(function (data) {
                    if (data.status == true) {
                        customToastr("user updated successfully", "success");
                        $("#edituserpopupclose").trigger("click");
                        _this.getUserList();
                    }
                    else {
                        $("#edituser").text("UPDATE USER");
                        $("#edituser").attr('disabled', false);
                        customToastr("user is not updated successfully please try again ", "error");
                    }
                }, function (error) { return console.log(error); }, function () {
                });
            }
            else {
                $("#edituser").text("UPDATE USER");
                $("#edituser").attr('disabled', false);
                customToastr("Mobile number should be 10 digits only ", "error");
            }
        }
        else {
            $("#edituser").text("UPDATE USER");
            $("#edituser").attr('disabled', false);
            customToastr("Please fill All the fields ", "error");
        }
    };
    SettingsComponent.prototype.deleteAdminUser = function () {
        var _this = this;
        this.settingservice.deleteAdminUserDetails(this.selecteduserId)
            .subscribe(function (data) {
            if (data.status == true) {
                customToastr("Admin user  deleted", "success");
                $("#deletepopupclose").trigger("click");
                _this.getUserList();
            }
            else {
                customToastr("Admin user in not deleted please try again ", "error");
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.deleteuserid = function (u) {
        this.selecteduserId = u.pkSchoolAdminsId;
    };
    SettingsComponent.prototype.activateUser = function (user) {
        this.adminId = user.pkSchoolAdminsId;
        console.log("Admin Id:", this.adminId);
    };
    SettingsComponent.prototype.activateAdminUser = function () {
        var _this = this;
        this.settingservice.activateAdmin(this.adminId)
            .subscribe(function (data) {
            if (data.status == true) {
                customToastr("Admin user  activated successfully!", "success");
                $("#activeAdminColse").trigger("click");
                _this.getUserList();
            }
            else {
                customToastr("Admin user in not activated, please try again!", "error");
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.openPopUp = function (isFirstTimeAdminUser) {
        var _this = this;
        $("#adduserbuttondsble").text("ADD USER");
        $("#adduserbuttondsble").attr('disabled', false);
        this.adduser = { emailId: "", username: "", password: "", cnfmpassword: "", PhoneNumber: "", AccessType: "SUPER_ADMIN" };
        this.settingservice.getAdminUserFeatureList()
            .subscribe(function (data) {
            _this.getuserfeaturelist = data;
        }, function (error) { return console.log(error); }, function () {
        });
    };
    SettingsComponent.prototype.reactivateStudentDetails = function (st, type) {
        st.accountType = type;
        this.reactivatestudetails = st;
    };
    SettingsComponent.prototype.reactivatestudent = function () {
        var _this = this;
        $("#reactive").text("Please wait...");
        $("#reactive").attr('disabled', true);
        if (this.reactivatestudetails.accountType == "STUDENT") {
            this.settingservice.reactivateStudent(this.reactivatestudetails.studentId)
                .subscribe(function (data) {
                if (data.status == true) {
                    customToastr("Student Reactivated successfully", "success");
                    $("#reactive").text("REACTIVATE");
                    $("#reactive").attr('disabled', false);
                    $(".reactivatepopupclose").trigger("click");
                    _this.getInactiveStudentList();
                }
                else {
                    customToastr("Student not Reactivated successfully please try again ", "error");
                }
            }, function (error) { return console.log(error); }, function () {
            });
        }
        else {
            this.settingservice.reactivatestaff(this.reactivatestudetails.studentId)
                .subscribe(function (data) {
                if (data.status == true) {
                    customToastr("Staff Reactivated successfully", "success");
                    $(".reactivatepopupclose").trigger("click");
                    _this.getInactiveStaffList();
                }
                else {
                    customToastr("Staff not Reactivated successfully please try again ", "error");
                }
            }, function (error) { return console.log(error); }, function () {
            });
        }
    };
    SettingsComponent.prototype.reactivateStaffDetails = function (staff, type) {
        this.reactivatestudetails = {
            accountType: type,
            parentName: staff.staffName,
            mobileNumber: staff.mobileNumber,
            emailId: staff.emailId,
            reason: staff.reason,
            studentId: staff.staffId
        };
    };
    SettingsComponent.prototype.ngOnInit = function () {
        var adminPermissionlist = localStorage.getItem('adminPermissionlist');
        if (adminPermissionlist) {
            this.academicPermissions = JSON.parse(adminPermissionlist);
        }
        // $(".after").empty();
        $("#device-breakpoints").remove();
        $(".after").append("<script>function customToastr(message,type){toastr.options.hideDuration = 0;toastr.clear();toastr.options.closeButton = true;toastr.options.progressBar = false;toastr.options.debug = false;toastr.options.positionClass = 'toast-top-right';toastr.options.showDuration = 330;toastr.options.hideDuration = 330;toastr.options.timeOut = 5000;toastr.options.extendedTimeOut = 1000;toastr.options.showEasing = 'swing';toastr.options.hideEasing = 'swing';toastr.options.showMethod = 'slideDown';toastr.options.hideMethod = 'slideUp';toastr[type](message, '');return 0;}</" + "script>");
        this.getSchoolDetails();
        this.getOverViewDetails();
        this.getAccountDetails();
        this.getFeatureList();
        this.getUserList();
        this.getCurrentSchoolDetails();
    };
    SettingsComponent.prototype.validateRequiredField = function (data) {
        if (data == null || data == "" || data == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    SettingsComponent = __decorate([
        Component({
            selector: 'app-settings',
            templateUrl: './settings.component.html',
            styleUrls: [
                'assets/css/theme-default/bootstrap.css?1422792965',
                'assets/css/theme-default/materialadmin.css?1425466319',
                'assets/css/theme-default/font-awesome.min.css?1422529194',
                'assets/css/theme-default/material-design-iconic-font.min.css?1421434286',
                'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
                'assets/css/theme-default/libs/toastr/toastr.css?1425466569',
                '_crop/css/style.css',
                '_crop/css/style-example.css',
                '_crop/css/jquery.Jcrop.css',
                './settings.component.css'],
            encapsulation: ViewEncapsulation.None,
            providers: [LoginService, SettingService]
        }), 
        __metadata('design:paramtypes', [Router, LoginService, SettingService])
    ], SettingsComponent);
    return SettingsComponent;
}());
//# sourceMappingURL=F:/BW_PR/src/app/settings/settings.component.js.map