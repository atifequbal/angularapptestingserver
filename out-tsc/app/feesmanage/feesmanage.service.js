var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import 'rxjs/Rx';
import { AppSettings } from "../../config/config";
export var FeeManagementService = (function () {
    function FeeManagementService(http) {
        this.http = http;
        this._url = AppSettings.API_ENDPOINT;
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        //this.headers.append('authToken','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJJZCI6MjEsImFkbWluSWQiOjJ9.wAg5Xa7oI998zK_QRAvOg43cYL_UlNRWhHipCdUafu0');
        this.headers.append('authToken', localStorage.getItem('usertoken'));
    }
    FeeManagementService.prototype.fetchFeeDashDetails = function () {
        var json = JSON.stringify({ schoolId: '20' });
        var params = json;
        return this.http.post(this._url + '/manageFees/totalfeesDetails', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getSchoolFee = function (dataFor, stdId) {
        var json = JSON.stringify({ today: new Date().toISOString() });
        var url = "";
        if (dataFor == "wholeclass") {
            url = '/manageFees/todayfeeByWholeClass';
            json = JSON.stringify({ today: new Date().toISOString() });
        }
        else if (dataFor == "weekwholeclass") {
            url = '/manageFees/weeklyFeeDetailsByWholeClass';
            var t = new Date();
            t.setDate(t.getDate() - 7);
            json = JSON.stringify({ weekStartdate: t.toISOString(), weekEnddate: new Date().toISOString() });
        }
        else if (dataFor == "weekforclass") {
            url = '/manageFees/weeklyFeeDetailsByClass';
            var t = new Date();
            t.setDate(t.getDate() - 7);
            json = JSON.stringify({ weekStartdate: t.toISOString(), weekEnddate: new Date().toISOString(), fkClassStandardsId: stdId });
        }
        else if (dataFor == "monthwholeclass") {
            url = '/manageFees/monthlyFeesDetailsByWholeClass';
            var t = new Date();
            t.setDate(t.getDate() - 30);
            json = JSON.stringify({ monthStart: t.toISOString(), monthEnd: new Date().toISOString() });
        }
        else if (dataFor == "monthforclass") {
            url = '/manageFees/monthlyFeeDetailByClass';
            var t = new Date();
            t.setDate(t.getDate() - 30);
            json = JSON.stringify({ monthStart: t.toISOString(), monthEnd: new Date().toISOString(), fkClassStandardsId: stdId });
        }
        else if (dataFor == "summarywholeclass") {
            url = '/manageFees/wholeSummaryFeeDetailsByWholeClass';
            json = JSON.stringify({});
        }
        else if (dataFor == "summaryforclass") {
            url = '/manageFees/wholeSummaryFeeDetailByClass';
            json = JSON.stringify({ fkClassStandardsId: stdId });
        }
        var params = json;
        return this.http.post(this._url + url, params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.todayFeeByClass = function (date, standardId) {
        var json = JSON.stringify({ today: date, fkClassStandardsId: standardId });
        var params = json;
        return this.http.post(this._url + '/manageFees/todayFeeDetailsByClass', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getAvailableClass = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/classManagement/listStandards', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getClassSections = function (standardClassId) {
        var json = JSON.stringify({ standardClassId: standardClassId });
        var params = json;
        return this.http.post(this._url + '/classManagement/classmanagement_standard_SectionList', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getClassSectionsNotInFeesBoard = function (standardClassId) {
        var json = JSON.stringify({ standardClassId: standardClassId });
        var params = json;
        return this.http.post(this._url + '/manageFees/feeboard/getclasssectionsnotinfeesboard', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getClassStandardsNotInFeesBoard = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/manageFees/feeboard/getclassstandardsnotinfeesboard', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.classStudentFeeDetails = function (classId) {
        var json = JSON.stringify({ classId: classId });
        var params = json;
        return this.http.post(this._url + '/manageFees/studentsFeesDetails', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.feeComponentList = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/manageFees/feeComponentList', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getClassCategories = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/manageFees/feeboard/category', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getFeeBoardFees = function (categoryId) {
        var json = JSON.stringify({ categoryId: categoryId });
        var params = json;
        return this.http.post(this._url + '/manageFees/feeboard', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.addFeeTypeComponent = function (Name, includeInTerm, academicId, AppliesTo, duedate) {
        var json = JSON.stringify({ Name: Name, includeInTerm: includeInTerm, academicId: academicId, AppliesTo: AppliesTo, duedate: duedate });
        var params = json;
        return this.http.post(this._url + '/manageFees/createNewfeeComponent', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.fetchAcademicList = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/academiclist', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.fetchSchoolFeeTerms = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/manageFees/feesTermNameList', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.createTerm = function (term) {
        var json = JSON.stringify(term);
        var params = json;
        return this.http.post(this._url + '/manageFees/addTermComponent', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.addFeeBoard = function (fee) {
        var json = JSON.stringify(fee);
        var params = json;
        return this.http.post(this._url + '/manageFees/feeboard/addNewFees', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getFeeBoardDetailsOfClassSection = function (classId) {
        var json = JSON.stringify({ classId: classId });
        var params = json;
        return this.http.post(this._url + '/manageFees/feeboard/categoryclassdetails', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.updateFeesBoard = function (reqData) {
        var json = JSON.stringify(reqData);
        var params = json;
        return this.http.post(this._url + '/manageFees/feesFeeBoardCategoryUpdateClassFees', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getTermFeeConfig = function (fkClassStandardsId) {
        var json = JSON.stringify({ fkClassStandardsId: fkClassStandardsId });
        var params = json;
        return this.http.post(this._url + '/manageFees/termfeeConfigurationList', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getTypeTermListWithPaidStatus = function (classId, feesTypeId) {
        var json = JSON.stringify({ classId: classId, feesTypeId: feesTypeId });
        var params = json;
        return this.http.post(this._url + '/manageFees/updatableListTermFeeConfiguration', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.updateTermConfig = function (obj) {
        var json = JSON.stringify(obj);
        var params = json;
        return this.http.post(this._url + '/manageFees/updateTermFeeConfiguration', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.searchStudent = function (search) {
        var json = JSON.stringify({ search: search });
        var params = json;
        return this.http.post(this._url + '/feeCollection/searchStudent', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getStudentFeeDashInfo = function (studentId, classId) {
        var json = JSON.stringify({ studentId: studentId, classId: classId });
        var params = json;
        return this.http.post(this._url + '/feeCollection/studentDetails', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getStudentFeeList = function (studentId, classId) {
        var json = JSON.stringify({ studentId: studentId, classId: classId });
        var params = json;
        return this.http.post(this._url + '/feeCollection/studentfeeslist', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.sendPaymentInfo = function (payment) {
        var json = JSON.stringify(payment);
        var params = json;
        return this.http.post(this._url + '/feeCollection/insertFeeCollection', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getFeeReceipt = function (paymentReceiptId) {
        var json = JSON.stringify({ paymentReceiptId: paymentReceiptId });
        var params = json;
        return this.http.post(this._url + '/feeCollection/getFeeReceipt', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getFeeHistory = function (studentId, classId, feeHistoryPageNumber) {
        var json = JSON.stringify({ studentId: studentId, classId: classId, page: feeHistoryPageNumber });
        var params = json;
        return this.http.post(this._url + '/feeCollection/studentFeeHistory', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.updateTerm = function (reqObj) {
        var json = JSON.stringify(reqObj);
        var params = json;
        return this.http.post(this._url + '/manageFees/updateTermComponent', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    /* discounts */
    FeeManagementService.prototype.fetchdiscount = function (AUTHTOKEN) {
        var json = JSON.stringify({});
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageFees/DiscountDetails', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.fetchtermfeeList = function (AUTHTOKEN) {
        var json = JSON.stringify({});
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageFees/feesTermNameList', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.fetchtypefeeList = function (AUTHTOKEN) {
        var json = JSON.stringify({});
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageFees/feesTypeNameList', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.addDiscount = function (dName, dAmount, termArray, typeArray, AUTHTOKEN) {
        var json = JSON.stringify({ discountName: dName, discountAmount: dAmount, feesTermArray: termArray, feesTypeArray: typeArray });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageFees/AddDiscountFee', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getStudentlistfordiscount = function (classId, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageFees/studentlistforDiscount', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.applyDiscount = function (studentId, discountId, classId, AUTHTOKEN) {
        var json = JSON.stringify({ studentIdList: studentId, schoolDiscountId: discountId, classId: classId });
        var params = json;
        return this.http.post(this._url + '/manageFees/applyDiscount', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    /* Fees settings related */
    FeeManagementService.prototype.getFeeModuleSettings = function (getInstamojoAcc) {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/feesSettings/getFeesModuleSettings?getInstamojoAcc=' + getInstamojoAcc, params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.updateFeeModuleSettings = function (feesModuleSetting, settingName) {
        var json = JSON.stringify({ feesModuleSetting: feesModuleSetting, settingName: settingName });
        var params = json;
        return this.http.post(this._url + '/feesSettings/disableFeesModuleSetting', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getClassListWithOnlinePaymentSettings = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/feesSettings/classListWithOnlinePaymentSettings', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.addOrRemoveClassOnlinePaySettings = function (settings) {
        var json = JSON.stringify({ settings: settings });
        var params = json;
        return this.http.post(this._url + '/feesSettings/addOrRemoveClassOnlinePaySettings', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getOnlinePaymentPurposes = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/feesSettings/getOnlinePaymentPurposes', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.addOnlinePaymentPurpose = function (purpose) {
        var json = JSON.stringify({ purpose: purpose });
        var params = json;
        return this.http.post(this._url + '/feesSettings/addOnlinePaymentPurpose', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.deleteOnlinePaymentPurpose = function (purposeId) {
        var json = JSON.stringify({ purposeId: purposeId });
        var params = json;
        return this.http.post(this._url + '/feesSettings/deleteOnlinePaymentPurpose', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getSchoolOnlineFeePaymentLimits = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/feesSettings/getSchoolOnlineFeePaymentLimits', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.updateSchoolOnlineFeePaymentLimits = function (minimum, maximum) {
        var json = JSON.stringify({ minimum: minimum, maximum: maximum });
        var params = json;
        return this.http.post(this._url + '/feesSettings/updateSchoolOnlineFeePaymentLimits', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.getInstamojoUserAccount = function () {
        var json = JSON.stringify({});
        var params = json;
        return this.http.post(this._url + '/feesSettings/getInstamojoUserAccount', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.createInstamojoAccount = function (username, email, phone, password) {
        var json = JSON.stringify({ username: username, email: email, phone: phone, password: password });
        var params = json;
        return this.http.post(this._url + '/feesSettings/createInstamojoAccount', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.loginInstamojoAccount = function (username, password) {
        var json = JSON.stringify({ userName: username, userPassword: password });
        var params = json;
        return this.http.post(this._url + '/feesSettings/login/instamojo', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService.prototype.addInstamojoBankAccount = function (bankHolderName, accountNumber, iFSCCode, userId) {
        var json = JSON.stringify({ bankHolderName: bankHolderName, accountNumber: accountNumber, iFSCCode: iFSCCode, userId: userId });
        var params = json;
        return this.http.post(this._url + '/feesSettings/insertBankInformation', params, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    FeeManagementService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [Http])
    ], FeeManagementService);
    return FeeManagementService;
}());
//# sourceMappingURL=F:/BW_PR/src/app/feesmanage/feesmanage.service.js.map