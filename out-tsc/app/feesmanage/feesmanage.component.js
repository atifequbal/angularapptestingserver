var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewEncapsulation } from '@angular/core';
import { FeeManagementService } from "./feesmanage.service";
import { FormValidator } from "../form.validator";
import { Router } from "@angular/router";
import { LoginService } from "../login/login.service";
export var FeesmanageComponent = (function () {
    function FeesmanageComponent(feeService, router, loginservice, formValidator) {
        this.feeService = feeService;
        this.router = router;
        this.loginservice = loginservice;
        this.formValidator = formValidator;
        this.getAuthToken = localStorage.getItem('usertoken');
        this.getSchoolId = localStorage.getItem('schoolinfo');
        this.months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        this.studentFeeDashDetails = {
            student: { studentId: null, studentName: "", parentName: "", classNumber: "", classDivision: "", rollNumber: "", classId: null },
            TermFees: { TermAmount: 0, Paid: 0, Discount: 0, Balance: 0 },
            TypeFees: { FeeTypeAmount: 0, Paid: 0, Discount: 0, Balance: 0 }
        };
        this.studentFeeList = { termFees: [], typeFees: [] };
        this.isSearchActive = true;
        this.resultsExist = false;
        this.submitText = "Submit";
        this.feesTermConfigButtonSubmitText = "Submit";
        this.feePaymentProceedText = "proceed Payment";
        this.feeDashDetails = {};
        this.standardFeeDetails = [];
        this.classStudentsFeeDetails = [];
        this.classStanderds = [];
        this.classStanderdsNotInFeesBoard = [];
        this.classSectionsNotInFeesBoard = [];
        this.classSections = [];
        this.classCategories = [];
        this.feeBoardData = [];
        this.termFeeConfigurations = [];
        this.termList = [];
        this.feeComponentList = [];
        this.feeComponentListAddFeeBoard = [];
        this.standardsFeeTable = null;
        this.termFeeConfigurationTable = null;
        this.classFeeTable = null;
        this.feeComponentTable = null;
        this.feeHistoryTable = null;
        this.feeBoardTable = null;
        this.feeTermTable = null;
        this.loaded = false;
        this.current = null;
        this.standardsTableInitial = null;
        this.now = null;
        this.element = null;
        this.selectedClass = {};
        this.academicList = [];
        this.feeTermList = [];
        this.studentList = [];
        this.initialDataCreateFeeType = { Name: "", includeInTerm: false, academicId: "", AppliesTo: "ALL_STUDENTS", duedate: "" };
        this.initialDataCreateFeeTerm = { TermName: "", academicId: "", duedate: "" };
        this.createFeeComponent = JSON.parse(JSON.stringify(this.initialDataCreateFeeType));
        this.createFeeTerm = JSON.parse(JSON.stringify(this.initialDataCreateFeeTerm));
        this.selectedClassSectionFeeBoard = { feeTypes: [], classId: null };
        this.editTermFeeConfig = { fkClassId: "", pkFeesTypeId: "", termFees: [], Amount: 0 };
        this.searchStudentText = "";
        this.eventData = { target: { id: "" } };
        this.selectedPayment = [];
        this.payableAmount = 0;
        this.paymentMode = "";
        this.selectedPaymentType = "";
        this.finalPaymentList = [];
        this.feeReceipt = [];
        this.feeCollectionHistoy = [];
        this.feeReceiptStd = {};
        this.lastSelectedStudent = {};
        this.editTermData = { DueDate: null };
        this.feeHistoryPageNumber = 1;
        this.isLastPageExist = true;
        this.discountlist = [];
        this.termlist = [];
        this.typelist = [];
        this.selectedRadioButton = "";
        this.selectedclassId = "";
        this.selectedStudent = "";
        this.selectedSectionId = "";
        this.sectionId = "";
        this.discount = { pkSchoolDiscountId: "" };
        this.addDiscount = { discountName: "", discountAmount: "", compdisc: [{ fkFeesTermId: "", fkFeesTypeId: "" }] };
        this.discountStudentList = [];
        this.applyDiscountStatus = [];
        this.classFeesSettings = [];
        this.onlinePaymentPurposes = [];
        this.paymentLimits = {};
        this.feesSettingsInfo = [];
        this.createPurpose = "";
        this.deletePurposeInfo = null;
        this.instamojoUser = { userId: "", username: "", emailId: "", phoneNumber: "", bankHolderName: "", bankAccountNumber: "", bankIFSCCode: "" };
        this.merchantAcccSts = "";
        this.createMerchant = { username: "", email: "", phone: "", password: "", confirmPassword: "" };
        this.loginInstamojo = { username: "", password: "" };
        this.bankDetailsUpdate = { bankHolderName: "", accountNumber: "", iFSCCode: "", userId: "" };
        this.academicPermissions = {};
        this.once = true;
    }
    FeesmanageComponent.prototype.ngOnInit = function () {
        var adminPermissionlist = localStorage.getItem('adminPermissionlist');
        if (adminPermissionlist) {
            this.academicPermissions = JSON.parse(adminPermissionlist);
        }
        this.isValidToken();
        $("body").append("<script src='assets/js/libs/toastr/toastr.js'></script><script>function customToastr(message,type){toastr.options.hideDuration = 0;/*toastr.clear();*/toastr.options.closeButton = true;toastr.options.progressBar = false;toastr.options.debug = false;toastr.options.positionClass = 'toast-top-right';toastr.options.showDuration = 330;toastr.options.hideDuration = 330;toastr.options.timeOut = 5000;toastr.options.extendedTimeOut = 1000;toastr.options.showEasing = 'swing';toastr.options.hideEasing = 'swing';toastr.options.showMethod = 'slideDown';toastr.options.hideMethod = 'slideUp';toastr[type](message, '');return 0;}</" + "script>");
        $("#device-breakpoints").remove();
        this.standardsTableInitial = $('#datatable100').clone();
        this.now = new Date().toISOString();
        this.getFeeDashDetails();
        this.getClassStandards(true);
        var h = this;
        $("body").on("click", "#editTerm", function () {
            $("#offcanvas-manage-termFee_open").trigger("click");
            h.editTermData = JSON.parse($(this).attr("termData"));
            if (h.editTermData.DueDate) {
                var d = new Date(h.editTermData.DueDate);
                h.editTermData.DueDate = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();
                $("#dueDate_term").val(h.editTermData.DueDate);
            }
            $("#dueDate_term").datepicker({ autoclose: true });
        });
        $("body").on('click', '#removeterm', function () {
            $("#delete_data_popup").trigger("click");
            var termData = JSON.parse($(this).attr("termData"));
            $("#delete_text").text("Are you sure you want to delete term : " + termData.TermName + "?");
            $("#delete_data_popup").attr('data_id', termData.pkFeesTermId);
            $("#delete_data_popup").attr('data_type', "term");
        });
        $('#term').click(function () {
            $('.term-item').show();
            $('.typeComponent').hide();
        });
        $('#type').click(function () {
            $('.typeComponent').show();
            $('.term-item').hide();
        });
        $("#previous_add_fee").on("click", function () {
            $("#open_step_1_add_fee_board").trigger("click");
        });
        $("#add-class-setting-online-button").on("click", function () {
            $("#add-class-setting-online-div").show();
        });
        $("#add-class-setting-online-cancel").on('click', function () {
            $("#add-class-setting-online-div").hide();
        });
    };
    //token auth
    FeesmanageComponent.prototype.isValidToken = function () {
        var _this = this;
        var isValidToken = false;
        var isValidSchool = 0;
        this.loginservice.isValidToken(this.getAuthToken)
            .subscribe(function (data) {
            isValidToken = data.isValid;
            if (isValidToken == true) {
                _this.loginservice.isValidSchoolId(_this.getSchoolId)
                    .subscribe(function (data) {
                    isValidSchool = data.schoolExist;
                    if (isValidSchool != 1) {
                        _this.router.navigate(['/login']);
                    }
                    else {
                        _this.getFeeDashDetails();
                        _this.getClassStandards(true);
                    }
                }, function (error) { return console.log(error); }, function () { });
            }
            else {
                _this.router.navigate(['/login']);
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.fetchFeeDetailOfStandard = function () {
        var stdId = $("#selectStandard").val();
        var interval = $("#selectInterval").val();
        this.loaded = false;
        if (interval == "TODAY") {
            if (stdId == '') {
                this.getFeeDetails("wholeclass", null);
            }
            else {
                this.todayFeeDetailByClass(new Date().toISOString(), stdId);
            }
        }
        else if (interval == "WEEK") {
            if (stdId == '') {
                this.getFeeDetails("weekwholeclass", null);
            }
            else {
                this.getFeeDetails("weekforclass", stdId);
            }
        }
        else if (interval == "MONTH") {
            if (stdId == '') {
                this.getFeeDetails("monthwholeclass", null);
            }
            else {
                this.getFeeDetails("monthforclass", stdId);
            }
        }
        else if (interval == "SUMMARY") {
            if (stdId == '') {
                this.getFeeDetails("summarywholeclass", null);
            }
            else {
                this.getFeeDetails("summaryforclass", stdId);
            }
        }
    };
    FeesmanageComponent.prototype.getFeeDashDetails = function () {
        var _this = this;
        this.feeService.fetchFeeDashDetails()
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                _this.feeDashDetails = data[0];
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.getClassStandards = function (selectFirst) {
        var _this = this;
        this.feeService.getAvailableClass()
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                _this.classStanderds = data.schoolStandards;
                if (selectFirst) {
                    _this.getFeeDetails("summaryforclass", _this.classStanderds[0].pkClassStandardsId);
                    var h = _this;
                    setTimeout(function () {
                        $("#sel_" + h.classStanderds[0].pkClassStandardsId).attr('selected', true);
                    }, 100);
                }
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.getClassSections = function (classStandardId) {
        var _this = this;
        this.feeService.getClassSections(classStandardId)
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                _this.classSections = data[0];
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.clearIfStandardsTableAvailable = function () {
        if (this.standardsFeeTable) {
            this.standardsFeeTable.clear().draw();
            this.standardsFeeTable.destroy(false);
            this.standardsFeeTable = null;
        }
    };
    FeesmanageComponent.prototype.getFeeDetails = function (dataFor, stdId) {
        var _this = this;
        this.feeService.getSchoolFee(dataFor, stdId)
            .subscribe(function (data) {
            var rows = [];
            _this.clearIfStandardsTableAvailable();
            if (data.errorcode) {
            }
            else {
                _this.standardFeeDetails = data;
            }
            var h = _this;
            setTimeout(function () {
                h.applyStandardsFeeTable();
            }, 100);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.todayFeeDetailByClass = function (date, stdId) {
        var _this = this;
        this.feeService.todayFeeByClass(date, stdId)
            .subscribe(function (data) {
            _this.clearIfStandardsTableAvailable();
            if (data.errorcode) {
            }
            else {
                _this.standardFeeDetails = JSON.parse(JSON.stringify(data));
            }
            var h = _this;
            setTimeout(function () {
                h.applyStandardsFeeTable();
            }, 100);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.applyStandardsFeeTable = function () {
        if (this.standardsFeeTable == null) {
            this.standardsFeeTable = $('#datatable100').DataTable({
                "dom": 'lCfrtip',
                "order": [],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "oLanguage": {
                    "sEmptyTable": "There are no details to show"
                },
                "language": {
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
        }
    };
    FeesmanageComponent.prototype.applyFeeTermConfigTable = function () {
        if (this.termFeeConfigurationTable == null) {
            this.termFeeConfigurationTable = $('#termfeeconfiguration_new').DataTable({
                "dom": 'lCfrtip',
                "order": [],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "oLanguage": {
                    "sEmptyTable": "There are no details to show"
                },
                "language": {
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
        }
    };
    FeesmanageComponent.prototype.applyFeeHistoryTable = function () {
        if (this.feeHistoryTable == null) {
            this.feeHistoryTable = $('#feeHistoryTable').DataTable({
                "dom": 'lCfrtip',
                "order": [],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "oLanguage": {
                    "sEmptyTable": "There are no details to show"
                },
                "language": {
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
            setTimeout(function () {
                $("#dataTables_info").addClass("hide");
                $("#feeHistoryTable_paginate").addClass("hide");
            }, 100);
        }
    };
    FeesmanageComponent.prototype.applyClassFeeTable = function () {
        if (this.classFeeTable == null) {
            this.classFeeTable = $('#datatable03classfee').DataTable({
                "dom": 'lCfrtip',
                "order": [],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "oLanguage": {
                    "sEmptyTable": "There are no details to show"
                },
                "language": {
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
        }
    };
    FeesmanageComponent.prototype.applyFeeComponentTable = function () {
        if (this.feeComponentTable == null) {
            this.feeComponentTable = $('#components-fee').DataTable({
                "dom": 'lCfrtip',
                "bLengthChange": false,
                "bFilter": true,
                "order": [],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "oLanguage": {
                    "sEmptyTable": "There are no details to show"
                },
                "language": {
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
        }
    };
    FeesmanageComponent.prototype.applyFeesBoardTable = function () {
        if (this.feeBoardTable == null) {
            this.feeBoardTable = $('#fee-details').DataTable({
                "dom": 'lCfrtip',
                "order": [],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "oLanguage": {
                    "sEmptyTable": "There are no details to show"
                },
                "language": {
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
        }
    };
    FeesmanageComponent.prototype.applyFeesTermTable = function () {
        if (this.feeTermTable == null) {
            this.feeTermTable = $('#term-component-table').DataTable({
                "dom": 'lCfrtip',
                "order": [],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "oLanguage": {
                    "sEmptyTable": "There are no details to show"
                },
                "language": {
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
        }
    };
    FeesmanageComponent.prototype.viewStudentFees = function (fee) {
        var _this = this;
        $("#open_view_class_fee").trigger("click");
        $("#offcanvas-view-class-fee").show();
        this.selectedClass = fee;
        this.applyClassFeeTable();
        this.classFeeTable.clear().draw();
        this.feeService.classStudentFeeDetails(fee.classId)
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                _this.classStudentsFeeDetails = data;
                var rows = [];
                _this.classStudentsFeeDetails.forEach(function (item) {
                    var gender = item.gender == 0 ? 'FEMALE' : 'MALE';
                    rows.push([item.studentName, gender, item.PendingAmount, item.DiscountAmount, item.CollectedAmount]);
                });
                _this.classFeeTable.rows.add(rows).draw();
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    /* Feeboard related */
    FeesmanageComponent.prototype.openFeeComponentTab = function () {
        var _this = this;
        this.applyFeeComponentTable();
        this.feeComponentTable.clear().draw();
        this.feeService.feeComponentList()
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                _this.feeComponentList = data;
                var rows = [];
                _this.feeComponentList.forEach(function (item) {
                    var isIncludedInTerm = item.isIncludedInTerm ? "YES" : "NO";
                    var appliesTo = item.AppliesTo;
                    if (appliesTo == "ALL_STUDENTS") {
                        appliesTo = "All Students";
                    }
                    else if (appliesTo == "NEW_STUDENTS") {
                        appliesTo = "New Students";
                    }
                    else if (appliesTo == "OLD_STUDENTS") {
                        appliesTo = "Old Students";
                    }
                    var html = '<a href="#offcanvas-manage-feeComponent" class="section-manage"><i class="fa fa-edit fa-lg manage-component"></i></a> <a href="#" class="section-manage"><i class="fa fa-remove fa-lg"></i></a>';
                    rows.push([item.Name, isIncludedInTerm, appliesTo]);
                });
                _this.feeComponentTable.rows.add(rows).draw();
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.openFeeBoardTab = function () {
        var _this = this;
        this.feeService.getClassCategories()
            .subscribe(function (data) {
            if (data.errorcode) {
                _this.classCategories = [];
            }
            else {
                _this.classCategories = data;
                if (data && data.length > 0) {
                    _this.getFeeBoard(data[0].categoryId);
                }
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.viewFeeBoardForCategory = function () {
        var categoryId = $("#selectClassCategory").val();
        if (categoryId != '') {
            this.getFeeBoard(categoryId);
        }
    };
    FeesmanageComponent.prototype.getFeeBoard = function (categoryId) {
        var _this = this;
        if (this.feeBoardTable != null) {
            this.feeBoardTable.destroy();
            this.feeBoardTable = null;
        }
        this.feeService.getFeeBoardFees(categoryId)
            .subscribe(function (data) {
            if (data.errorcode) {
                _this.feeBoardData = [];
            }
            else {
                var h = _this;
                _this.feeBoardData = data;
            }
            var h = _this;
            setTimeout(function () {
                h.applyFeesBoardTable();
            }, 100);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.getAcademicList = function () {
        var _this = this;
        $("#duedateDatePicker").datepicker({ autoclose: true });
        this.createFeeComponent = JSON.parse(JSON.stringify(this.initialDataCreateFeeType));
        this.feeService.fetchAcademicList()
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                data = data.academicList;
                var started = false;
                var h = _this;
                _this.academicList = [];
                if (data && data.length > 0) {
                    data.forEach(function (ac) {
                        if (ac.InActive == 0) {
                            started = true;
                        }
                        if (started) {
                            h.academicList.push(ac);
                        }
                    });
                }
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.addFeeType = function (feeType) {
        var _this = this;
        feeType.duedate = $("#duedateDatePicker").val();
        var valid = false;
        if (feeType.Name != '' && feeType.academicId != '') {
            valid = true;
        }
        else {
            customToastr("Please provide fees component name & Academic year", 'error');
        }
        if (valid) {
            if (feeType.includeInTerm == false) {
                if (feeType.duedate == '') {
                    valid = false;
                    customToastr("Please provide due date", 'error');
                }
                else {
                    var d = feeType.duedate.split("/");
                    feeType.duedate = d[2] + "-" + d[1] + "-" + d[0];
                    valid = true;
                }
            }
            else
                valid = true;
        }
        if (feeType.AppliesTo && feeType.AppliesTo == '') {
            feeType.AppliesTo = "ALL_STUDENTS";
        }
        else if (!feeType.AppliesTo) {
            feeType.AppliesTo = "ALL_STUDENTS";
        }
        if (valid) {
            feeType.includeInTerm = feeType.includeInTerm ? 1 : 0;
            if (feeType.duedate == '') {
                feeType.duedate = null;
            }
            this.feeService.addFeeTypeComponent(feeType.Name, feeType.includeInTerm, feeType.academicId, feeType.AppliesTo, feeType.duedate)
                .subscribe(function (data) {
                if (data.errorcode) {
                    customToastr("Fees component not create. Please try again", 'error');
                }
                else {
                    if (data.status) {
                        customToastr("Fees component successfully created", 'success');
                        $("#add-new-fee-component").hide();
                        _this.openFeeComponentTab();
                    }
                    else if (data.message == "AlreadyExist") {
                        customToastr("Fees component already exist with same name", "error");
                    }
                }
            }, function (error) { return console.log(error); }, function () {
            });
        }
    };
    FeesmanageComponent.prototype.openTermsTab = function () {
        var _this = this;
        this.applyFeesTermTable();
        this.feeTermTable.clear().draw();
        this.feeService.fetchSchoolFeeTerms()
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                var rows = [];
                _this.feeTermList = data;
                var h = _this;
                _this.feeTermList.forEach(function (tr) {
                    var due = new Date(tr.DueDate);
                    var t = due.getDate() + "-" + h.months[due.getMonth()] + "-" + due.getFullYear();
                    var html = '<dtv><a style="cursor: pointer" id="editTerm" class="section-manage"><i class="fa fa-edit fa-lg manage-term"></i></a> &nbsp;&nbsp;&nbsp; <a style="cursor: pointer" id="removeterm" class="section-manage"><i class="fa fa-trash fa-lg"></i></a></div>';
                    var htmlDom = $(html);
                    $(htmlDom.find("#editTerm")).attr("termData", JSON.stringify(tr));
                    $(htmlDom.find("#removeterm")).attr("termData", JSON.stringify(tr));
                    rows.push([tr.TermName, t, htmlDom[0].outerHTML]);
                });
                _this.feeTermTable.rows.add(rows).draw();
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.openCreateTerm = function () {
        $("#termDueDate").datepicker({ autoclose: true });
        this.createFeeTerm = JSON.parse(JSON.stringify(this.initialDataCreateFeeTerm));
        this.getAcademicList();
    };
    FeesmanageComponent.prototype.createTerm = function (createFeeTerm) {
        var _this = this;
        this.initialDataCreateFeeTerm = { TermName: "", academicId: "", duedate: "" };
        createFeeTerm.DueDate = $("#termDueDate").val();
        if (createFeeTerm.TermName != '' && createFeeTerm.academicId != '' && createFeeTerm.DueDate != '') {
            var d = createFeeTerm.DueDate.split("/");
            createFeeTerm.DueDate = d[2] + "-" + d[1] + "-" + d[0];
            this.feeService.createTerm(createFeeTerm)
                .subscribe(function (data) {
                if (data.errorcode) {
                    customToastr("Failed to create term. Please try again");
                }
                else {
                    if (!data.status) {
                        customToastr(data.message, 'error');
                    }
                    else {
                        $(".modal-backdrop").trigger("click");
                        customToastr("Term Successfully created", 'success');
                        _this.openTermsTab();
                    }
                }
            }, function (error) { return console.log(error); }, function () {
            });
        }
        else {
            customToastr("Please provide all term details", 'error');
        }
    };
    FeesmanageComponent.prototype.getClassStandardsNotInFeesBoard = function () {
        var _this = this;
        this.feeService.getClassStandardsNotInFeesBoard()
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                _this.classStanderdsNotInFeesBoard = data;
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.getClassSectionsNotInFeesBoard = function (classStandardId) {
        var _this = this;
        this.feeService.getClassSectionsNotInFeesBoard(classStandardId)
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                _this.classSectionsNotInFeesBoard = data;
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.openAddFeeBoard = function () {
        this.getClassStandardsNotInFeesBoard();
    };
    FeesmanageComponent.prototype.standardSelected = function () {
        var stdId = $("#class_standard_selected").val();
        if (stdId != '') {
            this.getClassSectionsNotInFeesBoard(stdId);
        }
    };
    FeesmanageComponent.prototype.addFeeBoardNext = function () {
        var _this = this;
        $("#open_step_2_add_fee_board").trigger("click");
        this.feeService.feeComponentList()
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                _this.feeComponentListAddFeeBoard = data;
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.addFeeBoard = function () {
        var _this = this;
        $(".disable_buttons").attr("disabled", true);
        this.submitText = "Pls.. Wait";
        var obj = {
            classId: $("#selectSection").val(),
            fkFeesTypeIdList: []
        };
        this.feeComponentListAddFeeBoard.forEach(function (it) {
            if (!it.Amount) {
                it.Amount = 0;
            }
            obj.fkFeesTypeIdList.push({ feeTypeId: it.pkFeesTypeId, amount: it.Amount });
        });
        if (obj.classId != '' && obj.fkFeesTypeIdList.length > 0) {
            this.feeService.addFeeBoard(obj)
                .subscribe(function (data) {
                $(".disable_buttons").attr("disabled", false);
                $("#previous_add_fee").attr("disabled", false);
                _this.submitText = "Submit";
                if (data.errorcode) {
                    customToastr("Failed to add fees to a class. Please try again", 'error');
                }
                else {
                    customToastr("Class fee detail successfully added", 'success');
                    $("#close_add_fee_board").trigger("click");
                    _this.feeComponentListAddFeeBoard = [];
                    _this.classStanderds = [];
                    _this.classSections = [];
                }
            }, function (error) { return console.log(error); }, function () {
            });
        }
        else {
            $(".disable_buttons").attr("disabled", false);
            $("#previous_add_fee").attr("disabled", false);
            customToastr("invalid details given!!. Pls select which class this fees belongs to", 'error');
        }
    };
    FeesmanageComponent.prototype.cancelFeeBoardUpdate = function () {
        $("#open_edit_fee_board").trigger("click");
    };
    FeesmanageComponent.prototype.editFeeBoard = function (fee) {
        var _this = this;
        $("#open_edit_fee_board").trigger("click");
        this.feeService.getFeeBoardDetailsOfClassSection(fee.classId)
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                fee.AcademicYear = data[0].AcademicYear;
                fee.TotalStudents = data[0].TotalStudent;
                fee.classStandard = data[0].class;
                _this.selectedClassSectionFeeBoard = JSON.parse(JSON.stringify(fee));
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.updateFeeBoard = function (updatedInfo) {
        var obj = { classId: this.selectedClassSectionFeeBoard.classId, fkFeesTypeIdList: [] };
        this.selectedClassSectionFeeBoard.feeTypes.forEach(function (t) {
            if (t.updated) {
                obj.fkFeesTypeIdList.push({
                    feeTypeId: t.pkFeesTypeId,
                    updateamount: t.Amount,
                    Name: t.Name
                });
            }
        });
        if (obj.fkFeesTypeIdList.length > 0) {
            this.feeService.updateFeesBoard(obj)
                .subscribe(function (data) {
                if (data.errorcode) {
                }
                else {
                    var textS = "";
                    var textF = "";
                    for (var i = 0; i < data.statuses.length; i++) {
                        if (data.statuses[i].status) {
                            textS += "<br> " + data.statuses[i].name + " : " + data.statuses[i].error;
                        }
                        else
                            textF += "<br> " + data.statuses[i].name + " : " + data.statuses[i].error;
                    }
                    if (textS != '') {
                        customToastr(textS, 'success');
                    }
                    if (textF != '') {
                        customToastr(textF, 'error');
                    }
                    else {
                        $(".backdrop").trigger("click");
                    }
                }
            }, function (error) { return console.log(error); }, function () {
            });
        }
        else {
            $(".backdrop").trigger("click");
            customToastr("Nothing to update", 'error');
        }
    };
    FeesmanageComponent.prototype.openTermConfigTab = function () {
        //this.applyFeeTermConfigTable();
        this.getClassStandards(false);
        this.termList = [];
    };
    FeesmanageComponent.prototype.getClassTermConfig = function () {
        var _this = this;
        var stdId = $("#selectclassStandard").val();
        if (stdId != '') {
            if (this.termFeeConfigurationTable != null) {
                this.termFeeConfigurationTable.destroy();
                this.termFeeConfigurationTable = null;
            }
            this.feeService.getTermFeeConfig(stdId)
                .subscribe(function (data) {
                if (data.errorcode) {
                }
                else {
                    if (data && data.length > 0) {
                        _this.termFeeConfigurations = data;
                        if (!(_this.termList && _this.termList.length > 0)) {
                            _this.termList = JSON.parse(JSON.stringify(data[0].termFees));
                        }
                    }
                    else {
                        _this.termFeeConfigurations = [];
                    }
                    var h = _this;
                    setTimeout(function () {
                        h.applyFeeTermConfigTable();
                    }, 150);
                }
            }, function (error) { return console.log(error); }, function () {
            });
        }
    };
    FeesmanageComponent.prototype.getTypeTermListWithPaidStatus = function (classId, feesTypeId, callback) {
        this.feeService.getTypeTermListWithPaidStatus(classId, feesTypeId)
            .subscribe(function (data) {
            callback(data);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.openEditTermConfig = function (fee) {
        $("#open_edit_term_config").trigger("click");
        this.editTermFeeConfig = JSON.parse(JSON.stringify(fee));
    };
    FeesmanageComponent.prototype.updateTermFeeConfig = function () {
        var _this = this;
        this.feesTermConfigButtonSubmitText = "Pls.. Wait";
        $(".disable_buttons").attr("disabled", true);
        var reqObj = {
            classId: this.editTermFeeConfig.fkClassId,
            feeTypeId: this.editTermFeeConfig.pkFeesTypeId,
            termData: []
        };
        var updatedAmount = 0;
        this.editTermFeeConfig.termFees.forEach(function (ter) {
            if (ter.updated) {
                reqObj.termData.push(ter);
            }
            if (ter.PaymentAmount) {
                updatedAmount = updatedAmount + parseInt(ter.PaymentAmount);
            }
        });
        if (updatedAmount != this.editTermFeeConfig.Amount) {
            customToastr("Sum of term amount should be equal to ₹" + this.editTermFeeConfig.Amount, "error");
            this.feesTermConfigButtonSubmitText = "Submit";
            $(".disable_buttons").attr("disabled", false);
        }
        else if (reqObj.termData.length > 0) {
            this.feeService.updateTermConfig(reqObj)
                .subscribe(function (data) {
                _this.feesTermConfigButtonSubmitText = "Submit";
                $(".disable_buttons").attr("disabled", false);
                $("#offcanvas_updatetermfee_cancel_button").trigger("click");
                customToastr("Term configuration successfully updated", 'success');
                _this.getClassTermConfig();
            }, function (error) { return console.log(error); }, function () {
                _this.feesTermConfigButtonSubmitText = "Submit";
                $(".disable_buttons").attr("disabled", false);
            });
        }
        else {
            this.feesTermConfigButtonSubmitText = "Submit";
            $(".disable_buttons").attr("disabled", false);
            customToastr("No changes to update...!", 'error');
        }
    };
    FeesmanageComponent.prototype.parseDatewww = function (dueDate) {
        if (dueDate) {
            var d = new Date(dueDate);
            return "" + d.getDate() + "-" + this.months[d.getMonth()] + "-" + d.getFullYear();
        }
        return "--";
    };
    FeesmanageComponent.prototype.searchStudent = function () {
        var _this = this;
        this.isSearchActive = true;
        var text = $("#groupbutton10").val();
        text = text.trim();
        if (text != '') {
            this.feeService.searchStudent(text)
                .subscribe(function (data) {
                _this.resultsExist = true;
                _this.studentList = data;
            }, function (error) { return console.log(error); }, function () {
                _this.resultsExist = false;
            });
        }
    };
    FeesmanageComponent.prototype.fetchFeeCollectionInfo = function (student) {
        var _this = this;
        this.lastSelectedStudent = student;
        this.feeService.getStudentFeeDashInfo(student.studentId, student.classId)
            .subscribe(function (data) {
            _this.studentFeeDashDetails = data;
            _this.isSearchActive = false;
        }, function (error) { return console.log(error); }, function () {
        });
        this.feeService.getStudentFeeList(student.studentId, student.classId)
            .subscribe(function (data) {
            _this.isSearchActive = false;
            _this.studentFeeList = data;
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.handleKeyboardEvents = function (event) {
        if (event.key == 'Enter' && event.target.id == 'groupbutton10') {
            this.searchStudent();
        }
    };
    FeesmanageComponent.prototype.makePayment = function (type) {
        var data = [];
        if (type == 'TERM') {
            this.studentFeeList.termFees.forEach(function (t) {
                if (t.selected) {
                    data.push({ termId: t.fkFeesTermId, Name: t.TermName, Amount: t.feesTermAmount, Balance: t.Balance, Discount: t.discount, Paid: t.Paid });
                }
            });
            if (data.length > 0) {
                $("#transection").find("a").trigger("click");
                this.showPaymentDetails(data, type);
            }
        }
        if (type == 'TYPE') {
            this.studentFeeList.typeFees.forEach(function (t) {
                if (t.selected) {
                    data.push({ typeId: t.fkFeesTypeId, Name: t.Name, Amount: t.feesTypeAmount, Balance: t.Balance, Discount: t.Discount, Paid: t.Paid });
                }
            });
            if (data.length > 0) {
                $("#transection").find("a").trigger("click");
                this.showPaymentDetails(data, type);
            }
        }
    };
    FeesmanageComponent.prototype.showPaymentDetails = function (data, type) {
        this.selectedPayment = data;
        this.selectedPaymentType = type;
        var balance = 0;
        this.selectedPayment.forEach(function (t) {
            balance = balance + t.Balance;
        });
        this.payableAmount = balance;
        $("#payableAmount").val(this.payableAmount);
        $("#calculatePayableAmount").text(this.payableAmount);
    };
    FeesmanageComponent.prototype.calculatePayableAmount = function () {
        var balance = 0;
        this.selectedPayment.forEach(function (t) {
            balance = balance + t.Balance;
        });
        this.payableAmount = balance;
        if (this.once) {
            this.once = false;
            $("#payableAmount").val(this.payableAmount);
        }
        return balance;
    };
    FeesmanageComponent.prototype.proceedToPaymentMode = function () {
        var payAmount = $("#payableAmount").val();
        if ($.isNumeric(payAmount)) {
            var paymentList = [];
            var h = this;
            payAmount = parseInt(payAmount);
            if (payAmount > 0 && payAmount <= this.payableAmount) {
                this.selectedPayment.forEach(function (t) {
                    if (payAmount > 0) {
                        if (t.Balance >= payAmount) {
                            if (h.selectedPaymentType == "TERM") {
                                console.log("13");
                                paymentList.push({ termId: t.termId, Balance: payAmount, Discount: 0 });
                            }
                            else {
                                console.log("14");
                                paymentList.push({ typeId: t.typeId, Balance: payAmount, Discount: 0 });
                            }
                            payAmount = 0;
                        }
                        else {
                            if (h.selectedPaymentType == "TERM") {
                                paymentList.push({ termId: t.termId, Balance: t.Balance, Discount: 0 });
                                payAmount = payAmount - t.Balance;
                            }
                            else {
                                paymentList.push({ typeId: t.typeId, Balance: t.Balance, Discount: 0 });
                                payAmount = payAmount - t.Balance;
                            }
                        }
                    }
                });
                this.finalPaymentList = paymentList;
                $("#paymentDetailTabHeader, #payment-details").removeClass("active");
                $("#payment-mode, #mode-of-payment").addClass("active");
            }
            else if (payAmount <= 0 || payAmount > this.payableAmount) {
                customToastr("Please pay an amount between Rs.1 to Rs." + this.payableAmount, 'error');
            }
        }
        else {
            customToastr("Invalid amount", 'error');
        }
    };
    FeesmanageComponent.prototype.backToPaymentDetails = function () {
        $("#paymentDetailTabHeader, #payment-details").addClass("active");
        $("#payment-mode, #mode-of-payment").removeClass("active");
    };
    FeesmanageComponent.prototype.changePaymentMode = function (mode) {
        if (mode == "DD") {
            $("#dd_date").datepicker({ autoclose: true });
            $("#payByDD").addClass("show");
            $("#payByDD").removeClass("hide");
            $("#payBycheque").addClass("hide");
            $("#payBycheque").removeClass("show");
        }
        else if (mode == "Cheque") {
            $("#cheque_date").datepicker({ autoclose: true });
            $("#payByDD").addClass("hide");
            $("#payByDD").removeClass("show");
            $("#payBycheque").addClass("show");
            $("#payBycheque").removeClass("hide");
        }
        else {
            $("#payByDD").addClass("hide");
            $("#payByDD").removeClass("show");
            $("#payBycheque").addClass("hide");
            $("#payBycheque").removeClass("show");
        }
        this.paymentMode = mode;
    };
    FeesmanageComponent.prototype.proceedToPayment = function (paymentList) {
        this.feePaymentProceedText = "Pls.. Wait";
        $(".disable_buttons").attr("disabled", true);
        this.paymentMode = this.paymentMode || "Cash";
        var reqObj = {
            discount: 0,
            fkClassId: this.studentFeeDashDetails.student.classId,
            termFeesArr: [],
            feesTypeArr: [],
            fkStudentId: this.studentFeeDashDetails.student.studentId,
            ModeOfPayment: this.paymentMode,
            BankName: null,
            ModeOfPayDate: null,
            DDNumber: null,
            MICRCode: null,
            ChequeNumber: null
        };
        if (this.selectedPaymentType == "TERM") {
            reqObj.termFeesArr = JSON.parse(JSON.stringify(paymentList));
        }
        else {
            reqObj.feesTypeArr = JSON.parse(JSON.stringify(paymentList));
        }
        if (this.paymentMode == "DD") {
            var ddBankBranch = $("#dd_bankandbranch").val();
            var ddDate = $("#dd_date").val();
            var ddNumber = $("#dd_number").val();
            var ddMicrCode = $("#dd_micrcode").val();
            if (ddBankBranch != '' && ddDate != '' && ddNumber != '' && ddMicrCode != '') {
                var d = ddDate.split("/");
                ddDate = d[2] + "-" + d[1] + "-" + d[0];
                reqObj.BankName = ddBankBranch;
                reqObj.ModeOfPayDate = ddDate;
                reqObj.DDNumber = ddNumber;
                reqObj.MICRCode = ddMicrCode;
                this.sendPaymentInfo(reqObj);
            }
            else {
                customToastr("Please fill the DD details", 'error');
                this.feePaymentProceedText = "proceed Payment";
                $(".disable_buttons").attr("disabled", false);
            }
        }
        else if (this.paymentMode == "Cheque") {
            var chequeBankBranch = $("#cheque_bankandbranch").val();
            var chequeDate = $("#cheque_date").val();
            var chequeNumber = $("#cheque_number").val();
            if (chequeBankBranch != '' && chequeDate != '' && chequeNumber != '') {
                var d = chequeDate.split("/");
                chequeDate = d[2] + "-" + d[1] + "-" + d[0];
                reqObj.BankName = chequeBankBranch;
                reqObj.ModeOfPayDate = chequeDate;
                reqObj.ChequeNumber = chequeNumber;
                this.sendPaymentInfo(reqObj);
            }
            else {
                customToastr("Please fill the Cheque details", 'error');
                this.feePaymentProceedText = "proceed Payment";
                $(".disable_buttons").attr("disabled", false);
            }
        }
        else {
            this.sendPaymentInfo(reqObj);
        }
    };
    FeesmanageComponent.prototype.sendPaymentInfo = function (paymentDetails) {
        var _this = this;
        this.feeService.sendPaymentInfo(paymentDetails)
            .subscribe(function (data) {
            if (data.status) {
                _this.getFeeReceipt(data.status);
            }
            else {
                _this.feePaymentProceedText = "proceed Payment";
                $(".disable_buttons").attr("disabled", false);
            }
        }, function (error) { return console.log(error); }, function () {
            _this.feePaymentProceedText = "proceed Payment";
            $(".disable_buttons").attr("disabled", false);
        });
    };
    FeesmanageComponent.prototype.getFeeReceipt = function (receiptId) {
        var _this = this;
        this.feeService.getFeeReceipt(receiptId)
            .subscribe(function (data) {
            _this.feeReceipt = data;
            _this.feeReceiptStd = data[0];
            _this.feePaymentProceedText = "proceed Payment";
            $(".disable_buttons").attr("disabled", false);
            $("#openFeeReceipt").trigger("click");
            $("#tab1").addClass("active");
            $("#tab2").removeClass("active");
            $("#tab3").removeClass("active");
            _this.fetchFeeCollectionInfo(_this.lastSelectedStudent);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.getTotalAmount = function () {
        var amount = 0;
        this.feeReceipt.forEach(function (item) {
            amount += item.Amount;
        });
        return amount;
    };
    FeesmanageComponent.prototype.formatDate = function (date) {
        if (date && date != '') {
            date = new Date();
        }
        var d = new Date(date);
        return "" + d.getDate() + "-" + this.months[d.getMonth()] + "-" + d.getFullYear();
    };
    FeesmanageComponent.prototype.getFeeCollectionHistory = function (studentId, classId, resetPage) {
        var _this = this;
        if (resetPage == "YES") {
            this.feeHistoryPageNumber = 1;
        }
        if (this.feeHistoryTable != null) {
            this.feeHistoryTable.destroy();
            this.feeHistoryTable = null;
        }
        this.feeService.getFeeHistory(studentId, classId, this.feeHistoryPageNumber)
            .subscribe(function (data) {
            if (!data.errorcode) {
                _this.feeCollectionHistoy = data;
                _this.isLastPageExist = data.length == 10;
                $(".fees_history_next").attr("disabled", !_this.isLastPageExist);
            }
            else
                _this.feeCollectionHistoy = [];
            var h = _this;
            setTimeout(function () {
                h.applyFeeHistoryTable();
            }, 100);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.showFeeReceipt = function (receiptId) {
        var _this = this;
        this.feeService.getFeeReceipt(receiptId)
            .subscribe(function (data) {
            _this.feeReceipt = data;
            _this.feeReceiptStd = data[0];
            $("#openFeeReceipt").trigger("click");
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.nextPage = function () {
        if (this.isLastPageExist) {
            this.feeHistoryPageNumber = this.feeHistoryPageNumber + 1;
            this.getFeeCollectionHistory(this.studentFeeDashDetails.student.studentId, this.studentFeeDashDetails.student.classId, this.feeHistoryPageNumber);
        }
        else {
            $(".fees_history_next").attr("disabled", false);
        }
        if (this.feeHistoryPageNumber >= 1) {
            $(".fees_history_prev").attr("disabled", false);
        }
    };
    FeesmanageComponent.prototype.previousPage = function () {
        if (this.feeHistoryPageNumber >= 1) {
            this.feeHistoryPageNumber = this.feeHistoryPageNumber - 1;
            if (this.feeHistoryPageNumber == 0) {
                $(".fees_history_prev").attr("disabled", true);
            }
            else
                $(".fees_history_prev").attr("disabled", false);
            this.getFeeCollectionHistory(this.studentFeeDashDetails.student.studentId, this.studentFeeDashDetails.student.classId, this.feeHistoryPageNumber);
        }
        else {
            $(".fees_history_prev").attr("disabled", true);
        }
    };
    FeesmanageComponent.prototype.updateTerm = function (term) {
        var duedate = $("#dueDate_term").val();
        if (term.TermName != "" && duedate != '') {
            var d = duedate.split("/");
            var reqObj = {
                name: term.TermName,
                duedate: d[2] + "-" + d[1] + "-" + d[0],
                pkFeesTermId: term.pkFeesTermId
            };
            this.feeService.updateTerm(reqObj)
                .subscribe(function (data) {
                if (data.message == "Success") {
                    customToastr("Term successfully updated", 'error');
                    $(".backdrop").trigger("click");
                }
                else if (data.message = "AlreadyExist") {
                    customToastr("term already exists", 'error');
                }
                else {
                    customToastr("Fees term not updated. Please try again", 'error');
                }
            }, function (error) { return console.log(error); }, function () {
            });
        }
        else {
            customToastr("Please provide required details", 'error');
        }
    };
    FeesmanageComponent.prototype.cancelTermUpdate = function () {
        //$(".backdrop").trigger("click");
        $("#offcanvas-manage-termFee_open").trigger("click");
    };
    FeesmanageComponent.prototype.discountDetails = function () {
        var _this = this;
        this.feeService.fetchdiscount(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            _this.discountlist = data;
            if (data.length == 0) {
                $('#discountAvail').show();
            }
            else {
                $('#discountAvail').hide();
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.termfee = function () {
        var _this = this;
        this.selectedRadioButton = "term";
        this.feeService.fetchtermfeeList(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            _this.termlist = data;
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.typefee = function () {
        var _this = this;
        this.selectedRadioButton = "type";
        this.feeService.fetchtypefeeList(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            _this.typelist = data[0];
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.aplyDiscountpopup = function (d) {
        this.discount = d;
        $("#applydiscountbutton").attr('disabled', false);
        $('#apply-discount').modal('show');
    };
    FeesmanageComponent.prototype.adddiscount = function (addDiscount) {
        var _this = this;
        $("#addDiscountbutton").attr('disabled', true);
        var i;
        var termId = [];
        var typeId = [];
        if (this.selectedRadioButton == "term") {
            if (this.termlist && this.termlist.length > 0) {
                for (i = 0; i < this.termlist.length; i++) {
                    if (this.termlist[i].selected == true) {
                        termId.push(this.termlist[i]);
                    }
                }
            }
        }
        else {
            if (this.typelist && this.typelist.length > 0) {
                for (i = 0; i < this.typelist.length; i++) {
                    if (this.typelist[i].selected == true) {
                        typeId.push(this.typelist[i]);
                    }
                }
            }
        }
        if ((termId.length > 0 || typeId.length > 0) && addDiscount.discountName != null && addDiscount.discountName != "" && addDiscount.discountName != undefined && addDiscount.discountAmount != null && addDiscount.discountAmount != "" && addDiscount.discountAmount != undefined) {
            this.feeService.addDiscount(addDiscount.discountName, addDiscount.discountAmount, termId, typeId, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                $("#addDiscountbutton").attr('disabled', false);
                if (data.status == true) {
                    customToastr('Discount is added successfully', 'success');
                    //............
                    $("#popupAddDiscount").trigger("click");
                    _this.discountDetails();
                }
                else {
                    customToastr('Discount is not added please try again', 'error');
                }
            }, function (error) { return console.log(error); }, function () {
                $("#addDiscountbutton").attr('disabled', false);
            });
        }
        else {
            $("#addDiscountbutton").attr('disabled', false);
            customToastr("Please provide all details required to create discount", 'error');
        }
    };
    FeesmanageComponent.prototype.classSection = function () {
        var id = $("#classStand").val();
        if (id != '') {
            $("#viewStudent").removeClass("disabled");
            this.getClassSections(id);
        }
        else {
            $("#viewStudent").addClass("disabled");
        }
    };
    FeesmanageComponent.prototype.viewStudents = function () {
        var selectSection = $("#selectSectionId").val();
        this.sectionId = selectSection;
        if (selectSection != '') {
            this.studentListforDiscount(selectSection);
        }
    };
    FeesmanageComponent.prototype.studentListforDiscount = function (classId) {
        var _this = this;
        this.selectedclassId = classId;
        this.feeService.getStudentlistfordiscount(classId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            _this.discountStudentList = data[0];
            setTimeout(function () {
                $('#reportTable').DataTable();
            }, 100);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.selectAll = function () {
        var isSel = $("#selectAll").prop("checked");
        this.discountStudentList.forEach(function (st) {
            st.select = isSel;
        });
        this.selectedStudent = "select";
    };
    FeesmanageComponent.prototype.applyDiscountToStudent = function (applyDiscount) {
        var _this = this;
        $("#applydiscountbutton").attr('disabled', true);
        var studentIdList = [];
        if (this.discountStudentList && this.discountStudentList.length > 0) {
            for (var i = 0; i < this.discountStudentList.length; i++) {
                if (this.discountStudentList[i].select == true) {
                    studentIdList.push(this.discountStudentList[i]);
                }
            }
        }
        if (studentIdList.length > 0) {
            this.feeService.applyDiscount(studentIdList, this.discount.pkSchoolDiscountId, this.sectionId, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                if (data.status == true) {
                    customToastr('Discount is applied to the selected student successfully', 'success');
                    //............
                    $("#addDiscountbutton").attr('disabled', false);
                    $("#closeApplyDiscountpopup").trigger("click");
                    _this.applyDiscountStatus = data.statuses;
                    setTimeout(function () {
                        $('#applyDiscountStatus').modal('toggle');
                    }, 100);
                }
                else {
                    $("#addDiscountbutton").attr('disabled', false);
                    customToastr('Discount is not applied please try again', 'error');
                }
            }, function (error) { return console.log(error); }, function () {
                $("#addDiscountbutton").attr('disabled', false);
            });
        }
        else {
            $("#addDiscountbutton").attr('disabled', false);
            customToastr("Please Select students to apply discount", 'error');
        }
    };
    FeesmanageComponent.prototype.addDiscountpopup = function () {
        $("#addDiscountbutton").attr('disabled', false);
    };
    FeesmanageComponent.prototype.delete = function (id, type) {
    };
    /* Fees settings related */
    FeesmanageComponent.prototype.openFeesSettingsTab = function () {
        this.loginInstamojo = { username: "", password: "" };
        this.getFeesSettings('yes');
    };
    FeesmanageComponent.prototype.getFeesSettings = function (getInstamojoAcc) {
        var _this = this;
        this.feeService.getFeeModuleSettings(getInstamojoAcc)
            .subscribe(function (data) {
            _this.feesSettingsInfo = data.feesSettings;
            _this.instamojoUser = data.instamojoUser;
            if (!_this.instamojoUser) {
                _this.createMerchant = { username: "", email: "", phone: "", password: "", confirmPassword: "" };
                _this.merchantAcccSts = 'CREATE';
            }
            else {
                _this.merchantAcccSts = 'SHOW_ACCOUNT';
            }
            var isFeesModuleEnable = false;
            var isOnlinePaymentEnabled = false;
            for (var i = 0; i < _this.feesSettingsInfo.length; i++) {
                if (_this.feesSettingsInfo[i].isFeesModuleEnable == "ENABLED" && _this.feesSettingsInfo[i].SettingName == "MOBILE_FEES_MODULE") {
                    $('#mobileFeesModule_enable').addClass('active');
                    $('#mobileFeesModule_enable').removeClass('not_active');
                    $('#mobileFeesModule_disable').addClass('not_active');
                    $('#mobileFeesModule_disable').removeClass('active');
                    isFeesModuleEnable = true;
                }
                else if (_this.feesSettingsInfo[i].isFeesModuleEnable == "DISABLED" && _this.feesSettingsInfo[i].SettingName == "MOBILE_FEES_MODULE") {
                    $('#mobileFeesModule_enable').removeClass('active');
                    $('#mobileFeesModule_enable').addClass('not_active');
                    $('#mobileFeesModule_disable').addClass('active');
                    $('#mobileFeesModule_disable').removeClass('not_active');
                }
                if (_this.feesSettingsInfo[i].isFeesModuleEnable == "ENABLED" && _this.feesSettingsInfo[i].SettingName == "MOBILE_FEES_PAYMENT") {
                    $('#mobileFeesPayment_enable').addClass('active');
                    $('#mobileFeesPayment_enable').removeClass('not_active');
                    $('#mobileFeesPayment_disable').addClass('not_active');
                    $('#mobileFeesPayment_disable').removeClass('active');
                    isOnlinePaymentEnabled = true;
                }
                else if (_this.feesSettingsInfo[i].isFeesModuleEnable == "DISABLED" && _this.feesSettingsInfo[i].SettingName == "MOBILE_FEES_PAYMENT") {
                    $('#mobileFeesPayment_enable').removeClass('active');
                    $('#mobileFeesPayment_enable').addClass('not_active');
                    $('#mobileFeesPayment_disable').addClass('active');
                    $('#mobileFeesPayment_disable').removeClass('not_active');
                }
                if (_this.feesSettingsInfo[i].isFeesModuleEnable == "ENABLED" && _this.feesSettingsInfo[i].SettingName == "ONLINE_PAYMENT_INDEPENDENT_TO_FEES_STRUCTURE") {
                    $('#onlinePaymentIndependentTo_enable').addClass('active');
                    $('#onlinePaymentIndependentTo_enable').removeClass('not_active');
                    $('#onlinePaymentIndependentTo_disable').addClass('not_active');
                    $('#onlinePaymentIndependentTo_disable').removeClass('active');
                    isFeesModuleEnable = true;
                }
                else if (_this.feesSettingsInfo[i].isFeesModuleEnable == "DISABLED" && _this.feesSettingsInfo[i].SettingName == "ONLINE_PAYMENT_INDEPENDENT_TO_FEES_STRUCTURE") {
                    $('#onlinePaymentIndependentTo_enable').removeClass('active');
                    $('#onlinePaymentIndependentTo_enable').addClass('not_active');
                    $('#onlinePaymentIndependentTo_disable').addClass('active');
                    $('#onlinePaymentIndependentTo_disable').removeClass('not_active');
                }
            }
            if (isFeesModuleEnable) {
                $("#mobileFeesPaymentdiv").css({ 'display': 'block' });
                if (isOnlinePaymentEnabled) {
                    $("#class_online_paymentdiv, #online_payment_setting_tabs").css({ 'display': 'block' });
                }
                else {
                    $("#class_online_paymentdiv, #online_payment_setting_tabs").css({ 'display': 'none' });
                }
            }
            else {
                $("#class_online_paymentdiv, #mobileFeesPaymentdiv, #online_payment_setting_tabs").css({ 'display': 'none' });
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.getInstamojoAcc = function () {
        var _this = this;
        this.feeService.getInstamojoUserAccount()
            .subscribe(function (data) {
            _this.instamojoUser = data;
            _this.merchantAcccSts = 'SHOW_ACCOUNT';
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.changeSetting = function (settingName, value) {
        var _this = this;
        if (this.formValidator.validateIsData(settingName)) {
            this.feeService.updateFeeModuleSettings(value, settingName)
                .subscribe(function (data) {
                customToastr("Setting updated", 'success');
                _this.getFeesSettings('no');
            }, function (error) { return console.log(error); }, function () {
            });
        }
        else {
            customToastr("Update setting failed please try again", 'error');
        }
    };
    FeesmanageComponent.prototype.getClassFeesSettings = function () {
        var _this = this;
        this.feeService.getClassListWithOnlinePaymentSettings()
            .subscribe(function (data) {
            _this.classFeesSettings = [];
            //this.classFeesSettings = data;
            var h = _this;
            data.forEach(function (it) {
                if (it.IsEnabled == 'ENABLED') {
                    it.selected = true;
                }
                else
                    it.selected = false;
                h.classFeesSettings.push(it);
            });
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.updateClassFeeSettings = function () {
        var reqData = [];
        $("#add-class-setting-online-save").text("Please wait..");
        $("#add-class-setting-online-cancel, #add-class-setting-online-save").attr("disabled", true);
        var updatedList = JSON.parse(JSON.stringify(this.classFeesSettings));
        if (updatedList.length > 0) {
            updatedList.forEach(function (it) {
                if (it.selected) {
                    reqData.push({ isEnabled: 'ENABLED', classId: it.classId, settingId: it.pkClassOnlinePaymentSettingId });
                }
                else {
                    reqData.push({ isEnabled: 'DISABLED', classId: it.classId, settingId: it.pkClassOnlinePaymentSettingId });
                }
            });
            this.feeService.addOrRemoveClassOnlinePaySettings(reqData)
                .subscribe(function (data) {
                $("#add-class-setting-online-save").text("Save");
                $("#add-class-setting-online-cancel, #add-class-setting-online-save").attr("disabled", false);
                if (data.status) {
                    customToastr("Class fee settings updated", 'success');
                    $("#add-class-setting-online-cancel").trigger('click');
                }
                else {
                    customToastr("Update class fee setting failed, Please try again", 'error');
                }
            }, function (error) { return console.log(error); }, function () {
                $("#add-class-setting-online-save").text("Save");
                $("#add-class-setting-online-cancel, #add-class-setting-online-save").attr("disabled", false);
            });
        }
        else {
            $("#add-class-setting-online-save").text("Save");
            $("#add-class-setting-online-cancel, #add-class-setting-online-save").attr("disabled", false);
            customToastr("Looks like there are no classes available", 'error');
        }
    };
    FeesmanageComponent.prototype.createInstamojoMerchantAccount = function (username, email, phone, password, confirmPassword, $event) {
        var _this = this;
        $("#create-merchant-button").text("PLease Wait..");
        $("#create-merchant-button").attr("disabled", true);
        if (this.formValidator.validateIsData(username) &&
            this.formValidator.validateIsEmail(email)
            && this.formValidator.validateIsData(phone) &&
            this.formValidator.validateIsData(password)) {
            if (password != confirmPassword) {
                $("#create-merchant-button").text("Create an account");
                $("#create-merchant-button").attr("disabled", false);
                customToastr("Password and Confirm password are mismatch", 'error');
                return;
            }
            this.feeService.createInstamojoAccount(username, email, phone, password)
                .subscribe(function (data) {
                $("#create-merchant-button").text("Create an account");
                $("#create-merchant-button").attr("disabled", false);
                if (data == true) {
                    customToastr("Merchant account successfully created", 'success');
                    _this.createMerchant = { username: "", email: "", phone: "", password: "", confirmPassword: "" };
                    _this.merchantAcccSts = 'SHOW_ACCOUNT';
                    _this.getInstamojoAcc();
                }
                else {
                    if (data.message) {
                        customToastr(data.message, 'error');
                    }
                    else {
                        for (var key in data) {
                            if (data[key] && data[key].length > 0) {
                                for (var i = 0; i < data[key].length; i++) {
                                    customToastr(data[key][i], 'error');
                                }
                            }
                        }
                    }
                }
            }, function (error) { return console.log(error); }, function () {
                $("#create-merchant-button").text("Create an account");
                $("#create-merchant-button").attr("disabled", false);
            });
        }
        else {
            $("#create-merchant-button").text("Create an account");
            $("#create-merchant-button").attr("disabled", false);
            customToastr("Please provide all valid details to create merchant account", 'error');
        }
    };
    FeesmanageComponent.prototype.logIntoMerchantAccount = function (username, password) {
        var _this = this;
        $("#instamojoSignIn").attr("disabled", true);
        $("#instamojoSignIn").text("Please Wait..");
        this.bankDetailsUpdate = { bankHolderName: "", accountNumber: "", iFSCCode: "", userId: "" };
        if (this.formValidator.validateIsData(username) && this.formValidator.validateIsData(password)) {
            this.feeService.loginInstamojoAccount(username, password)
                .subscribe(function (data) {
                $("#instamojoSignIn").attr("disabled", false);
                $("#instamojoSignIn").text("SIGN IN");
                if (data.error) {
                    customToastr("Login failed please try again", 'error');
                }
                else {
                    customToastr("Login success, Please provide bank account details", 'success');
                    _this.merchantAcccSts = 'BANK_INFO';
                }
            }, function (error) { return console.log(error); }, function () {
                $("#instamojoSignIn").attr("disabled", false);
                $("#instamojoSignIn").text("SIGN IN");
            });
        }
        else {
            $("#instamojoSignIn").attr("disabled", false);
            $("#instamojoSignIn").text("SIGN IN");
            customToastr("Missing UserName or password", 'error');
        }
    };
    FeesmanageComponent.prototype.updateBankDetails = function (bankHolderName, accountNumber, iFSCCode, userId) {
        var _this = this;
        userId = this.instamojoUser.userId;
        $("#saveBnakInfo").attr("disabled", true);
        $("#saveBnakInfo").text("Please Wait..");
        if (this.formValidator.validateIsData(bankHolderName) && this.formValidator.validateIsData(accountNumber) && this.formValidator.validateIsData(iFSCCode)) {
            this.feeService.addInstamojoBankAccount(bankHolderName, accountNumber, iFSCCode, userId)
                .subscribe(function (data) {
                $("#saveBnakInfo").attr("disabled", false);
                $("#saveBnakInfo").text("Save Information");
                if (data.error) {
                    customToastr("Bank details are not updated, Please try again", 'error');
                }
                else {
                    customToastr("Bank details are successfully updated", 'success');
                    _this.getInstamojoAcc();
                }
            }, function (error) { return console.log(error); }, function () {
                $("#saveBnakInfo").attr("disabled", false);
                $("#saveBnakInfo").text("Save Information");
            });
        }
        else {
            $("#saveBnakInfo").attr("disabled", false);
            $("#saveBnakInfo").text("Save Information");
            customToastr("Please provide bank details", 'error');
        }
    };
    FeesmanageComponent.prototype.getPaymentPurposes = function () {
        var _this = this;
        this.createPurpose = "";
        this.feeService.getOnlinePaymentPurposes()
            .subscribe(function (data) {
            if (data.length > 0) {
                _this.onlinePaymentPurposes = data;
            }
            else {
                _this.onlinePaymentPurposes = [];
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.addOnlinePaymentPurpose = function (purpose) {
        var _this = this;
        $("#createPurpose").text("Please Wait..");
        $("#createPurpose").attr("disabled", true);
        if (this.formValidator.validateIsData(purpose)) {
            this.feeService.addOnlinePaymentPurpose(purpose)
                .subscribe(function (data) {
                $("#createPurpose").text("Save purpose");
                $("#createPurpose").attr("disabled", false);
                if (data.status) {
                    customToastr("Payment purpose successfully added", 'success');
                    _this.getPaymentPurposes();
                }
                else {
                    customToastr('Payment purpose not added, Please try again', 'error');
                }
            }, function (error) { return console.log(error); }, function () {
                $("#createPurpose").text("Save purpose");
                $("#createPurpose").attr("disabled", false);
            });
        }
        else {
            $("#createPurpose").text("Save purpose");
            $("#createPurpose").attr("disabled", false);
            customToastr('Please provide payment purpose', 'error');
        }
    };
    FeesmanageComponent.prototype.deletePurpose_openPopup = function (p) {
        this.deletePurposeInfo = p;
    };
    FeesmanageComponent.prototype.deleteOnlinePaymentPurpose = function () {
        var _this = this;
        var purposeId = this.deletePurposeInfo.pkOnlinePayPurposeId;
        $("#savemovepurposepopup").text("Please wait..");
        $("#savemovepurposepopup, #closeremovepurposepopup").attr("disabled", true);
        this.feeService.deleteOnlinePaymentPurpose(purposeId)
            .subscribe(function (data) {
            $("#savemovepurposepopup, #closeremovepurposepopup").attr("disabled", false);
            $("#savemovepurposepopup").text("DELETE");
            if (data.status) {
                $("#closeremovepurposepopup").trigger('click');
                customToastr("Payment purpose successfully deleted", 'success');
                _this.getPaymentPurposes();
            }
            else {
                customToastr('Payment purpose not deleted, Please try again', 'error');
            }
        }, function (error) { return console.log(error); }, function () {
            $("#savemovepurposepopup, #closeremovepurposepopup").attr("disabled", false);
            $("#savemovepurposepopup").text("DELETE");
        });
    };
    FeesmanageComponent.prototype.getPaymentLimits = function () {
        var _this = this;
        this.feeService.getSchoolOnlineFeePaymentLimits()
            .subscribe(function (data) {
            if (data.status) {
                _this.paymentLimits = data;
            }
            else {
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    FeesmanageComponent.prototype.updatePaymentLimits = function (minimum, maximum) {
        $("#save_payment_limits").text("Please wait..");
        $("#save_payment_limits").attr("disabled", true);
        if ($.isNumeric(minimum) && $.isNumeric(maximum)) {
            var minimum_amount = parseInt(minimum);
            var maximum_amount = parseInt(maximum);
            this.feeService.updateSchoolOnlineFeePaymentLimits(minimum_amount, maximum_amount)
                .subscribe(function (data) {
                $("#save_payment_limits").text("Save Limits");
                $("#save_payment_limits").attr("disabled", false);
                if (data.status) {
                    customToastr("Online payment limit updated", 'success');
                }
                else {
                    if (data.message) {
                        customToastr(data.message, 'error');
                    }
                    else {
                        customToastr("Failed to update Online payment limits, Please try again", 'error');
                    }
                }
            }, function (error) { return console.log(error); }, function () {
                $("#save_payment_limits").text("Save Limits");
                $("#save_payment_limits").attr("disabled", false);
            });
        }
        else {
            customToastr("Please provide a valid amount", 'error');
            $("#save_payment_limits").text("Save Limits");
            $("#save_payment_limits").attr("disabled", false);
        }
    };
    FeesmanageComponent = __decorate([
        Component({
            selector: 'app-feesmanage',
            templateUrl: './feesmanage.component.html',
            styleUrls: ['assets/css/theme-default/bootstrap.css?1422792965',
                'assets/css/theme-default/materialadmin.css?1425466319',
                'assets/css/theme-default/font-awesome.min.css?1422529194',
                'assets/css/theme-default/libs/wizard/wizard.css?1425466601',
                'assets/css/theme-default/libs/toastr/toastr.css?1425466569',
                'assets/css/theme-default/material-design-iconic-font.min.css?1421434286',
                'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
                './feesmanage.component.css'],
            encapsulation: ViewEncapsulation.None,
            providers: [FeeManagementService, LoginService, FormValidator],
            host: {
                '(document:keydown)': 'handleKeyboardEvents($event)'
            }
        }), 
        __metadata('design:paramtypes', [FeeManagementService, Router, LoginService, FormValidator])
    ], FeesmanageComponent);
    return FeesmanageComponent;
}());
//# sourceMappingURL=F:/BW_PR/src/app/feesmanage/feesmanage.component.js.map