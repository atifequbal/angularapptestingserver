var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import { LoginService } from "../login/login.service";
export var ReportComponent = (function () {
    function ReportComponent(router, loginservice) {
        this.router = router;
        this.loginservice = loginservice;
    }
    //token auth
    ReportComponent.prototype.isValidToken = function () {
        var _this = this;
        var isValidToken = false;
        var isValidSchool = 0;
        this.loginservice.isValidToken(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            isValidToken = data.isValid;
            console.log("Token Validation in login page: ", isValidToken);
            if (isValidToken == true) {
                //this.router.navigate(['/dashboard']);
                //school id validation///////////////////////////////////////////////
                _this.loginservice.isValidSchoolId(localStorage.getItem('schoolinfo'))
                    .subscribe(function (data) {
                    isValidSchool = data.schoolExist;
                    _this.getSchoolId = localStorage.getItem('schoolinfo');
                    if (isValidSchool != 1) {
                        _this.router.navigate(['/login']);
                    }
                    else {
                        console.log("Stay in page (Dash board)");
                    }
                }, function (error) { return console.log(error); }, function () { });
            }
            else {
                _this.router.navigate(['/login']);
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    ReportComponent.prototype.ngOnInit = function () {
        this.isValidToken();
        $(".after").empty();
        $("#device-breakpoints").remove();
        $(".after").append("<script src='assets/js/libs/jquery/jquery-1.11.2.min.js'></" + "script><script src='assets/js/libs/jquery/jquery-migrate-1.2.1.min.js'></" + "script><script src='assets/js/libs/bootstrap/bootstrap.min.js'></" + "script><script src='assets/js/core/source/App.js'></" + "script><script src='assets/js/core/source/AppNavigation.js'></" + "script><script src='assets/js/core/source/AppOffcanvas.js'></" + "script><script src='assets/js/core/source/AppCard.js'></" + "script><script src='assets/js/core/source/AppForm.js'></" + "script><script src='assets/js/core/source/AppNavSearch.js'></" + "script><script src='assets/js/core/source/AppVendor.js'></" + "script><script src='assets/js/core/source/datepicker.js'></" + "script><script src='https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'></" + "script><script src='https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js'></" + "script><script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js'></" + "script><script src='http://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'></" + "script><script src='http://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'></" + "script><script src='http://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'></" + "script><script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js'></" + "script><script>$(document).ready(function () {$('#reportTable').DataTable();$('#attendanceTable').DataTable({dom: 'Bfrtip',buttons: ['excel', 'pdf']});});</" + "script>");
    };
    ReportComponent = __decorate([
        Component({
            selector: 'app-report',
            templateUrl: './report.component.html',
            styleUrls: [
                'assets/css/theme-default/bootstrap.css?1422792965',
                'assets/css/theme-default/materialadmin.css?1425466319',
                'assets/css/theme-default/font-awesome.min.css?1422529194',
                'assets/css/theme-default/material-design-iconic-font.min.css?1421434286',
                'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
                'css/datepicker.css',
                './report.component.css'],
            encapsulation: ViewEncapsulation.None,
            providers: [LoginService]
        }), 
        __metadata('design:paramtypes', [Router, LoginService])
    ], ReportComponent);
    return ReportComponent;
}());
//# sourceMappingURL=F:/BW_PR/src/app/report/report.component.js.map