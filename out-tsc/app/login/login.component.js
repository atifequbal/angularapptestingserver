var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { LoginService } from "./login.service";
import { Router } from "@angular/router";
export var LoginComponent = (function () {
    function LoginComponent(loginservice, router) {
        this.loginservice = loginservice;
        this.router = router;
        this.preLoader = false;
        this.getAuthToken = localStorage.getItem('usertoken');
        this.getSchoolId = localStorage.getItem('schoolinfo');
        this.loaderLogin = false;
    }
    LoginComponent.prototype.loginAuth = function () {
        var _this = this;
        LoginComponent.LOGIN_TOKEN = null;
        LoginComponent.SCHOOL_DATA = [];
        this.loaderLogin = true;
        console.log("Username: ", this.username);
        console.log("Password: ", this.password);
        //call get class service with cat id
        this.loginservice.loginValidate(this.username, this.password)
            .subscribe(function (data) {
            console.log("Login get data: ", data);
            if (data.token != undefined) {
                _this.loaderLogin = false;
                LoginComponent.LOGIN_TOKEN = data.token;
                LoginComponent.SCHOOL_DATA.push(data.schoolInfo);
                localStorage.setItem('usertoken', LoginComponent.LOGIN_TOKEN);
                localStorage.setItem('schoolinfo', data.schoolInfo.schoolId);
                localStorage.setItem('adminType', data.schoolInfo.adminType);
                var d = {};
                var arr = data.schoolInfo.adminPermissions;
                arr.forEach(function (item) {
                    d[item.BluewingsFeatureName] = item.ActiveStatus || item.AdminAccessStatus;
                });
                localStorage.setItem('adminPermissionlist', JSON.stringify(d));
                _this.router.navigate(['/dashboard']);
            }
            else {
                console.log("Error Login [Login page]");
                $('#modal1').modal('open');
                _this.loaderLogin = false;
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    //token auth
    LoginComponent.prototype.isValidToken = function () {
        var _this = this;
        var isValidToken = false;
        var isValidSchool = 0;
        this.loginservice.isValidToken(this.getAuthToken)
            .subscribe(function (data) {
            isValidToken = data.isValid;
            console.log("Token Validation in login page: ", isValidToken);
            if (isValidToken == true) {
                //this.router.navigate(['/dashboard']);
                //school id validation///////////////////////////////////////////////
                _this.loginservice.isValidSchoolId(_this.getSchoolId)
                    .subscribe(function (data) {
                    console.log("School info in login page: ", data);
                    isValidSchool = data.schoolExist;
                    if (isValidSchool == 1) {
                        _this.router.navigate(['/dashboard']);
                        _this.preLoader = true;
                    }
                    else {
                        _this.router.navigate(['/login']);
                        _this.preLoader = true;
                    }
                }, function (error) { return console.log(error); }, function () { });
            }
            else {
                //this.router.navigate(['/login']);
                _this.preLoader = true;
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    /* ngAfterViewInit(){
       console.log("Loaded!!!!!!!!!!!!!!!!!");
       this.preLoader = true;
     }*/
    LoginComponent.prototype.ngOnInit = function () {
        this.isValidToken();
        $(".after").append("<script type='text/javascript' src='app/login/jquery-2.1.1.min.js'></" + "script><script src='app/login/materialize.min.js'></" + "script><script type='text/javascript'>$('#modal1').modal();</" + "script>");
        /* <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
         <script>$('.modal').modal();</script>*/
        // is logged in & token is valid/////////////////////////////////////
        /*if(localStorage.getItem('usertoken') != null){
          this.loginservice.isValidToken(localStorage.getItem('usertoken'))
            .subscribe(
              data => {
    
                var isValid = data.isValid;
                console.log("Token Validation: ",isValid);
                if(data.isValid == true){
                  this.router.navigate(['/dashboard']);
                }else{
                  this.router.navigate(['/login']);
                }
              },
              error => console.log(error),
              () => {
    
              }
            );
          //
        }else{
          this.router.navigate(['/login']);
        }*/
        ///////////////////////////////////////////////////////////////
    };
    LoginComponent = __decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css'],
            providers: [LoginService]
        }), 
        __metadata('design:paramtypes', [LoginService, Router])
    ], LoginComponent);
    return LoginComponent;
}());
//# sourceMappingURL=F:/BW_PR/src/app/login/login.component.js.map