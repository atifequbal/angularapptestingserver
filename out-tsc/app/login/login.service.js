var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { AppSettings } from "../../config/config";
export var LoginService = (function () {
    function LoginService(http) {
        this.http = http;
        this._url = AppSettings.API_ENDPOINT;
    }
    LoginService.prototype.loginValidate = function (userName, password) {
        console.log("Service passed uname: ", userName);
        console.log("Service passed pwd: ", password);
        var json = JSON.stringify({ userName: userName, password: password });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this._url + '/login', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    LoginService.prototype.isValidToken = function (authToken) {
        console.log("Service passed token: ", authToken);
        var json = JSON.stringify({});
        var params = json;
        var headers = new Headers();
        headers.append('authToken', authToken);
        return this.http.post(this._url + '/login/findlogin', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    LoginService.prototype.isValidSchoolId = function (schoolId) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this._url + '/login/findSchool', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    LoginService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [Http])
    ], LoginService);
    return LoginService;
}());
//# sourceMappingURL=F:/BW_PR/src/app/login/login.service.js.map