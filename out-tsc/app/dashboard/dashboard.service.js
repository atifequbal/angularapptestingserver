var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import 'rxjs/Rx';
import { AppSettings } from "../../config/config";
export var DashboardService = (function () {
    //private AUTHTOKEN = LoginComponent.LOGIN_TOKEN
    function DashboardService(http) {
        this.http = http;
        this._url = AppSettings.API_ENDPOINT;
    }
    //fetch new admission list
    DashboardService.prototype.fetchNewAdmissionList = function (getSchoolId, academicId, getDate, AUTHTOKEN) {
        var json = JSON.stringify({ getSchoolId: getSchoolId, academicId: academicId, getDate: getDate });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/dashboard/newAdmissionList', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //fetch school activity
    DashboardService.prototype.fetchSchoolActivity = function (statusDate, AUTHTOKEN) {
        var json = JSON.stringify({ statusDate: statusDate });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/dashboard/schooldaystatus', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //fetch class feed
    DashboardService.prototype.fetchClassFeed = function (classId, statusDate, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classId, statusDate: statusDate.toString() });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/dashboard/feedfordate', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //fetch class feed
    DashboardService.prototype.onlineFeeCollectionHistory = function (date, endDate, AUTHTOKEN) {
        var json = JSON.stringify({ date: date, endDate: endDate, duration: date != endDate });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/feeCollection/schoolonlinefeehistory', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //fetch bw metrices /metrices for app version
    //http://myschool.bluewings.in/bluewings/metrics/counts?v=2
    DashboardService.prototype.fetchAppMetriceData = function () {
        // var json = JSON.stringify({classId: classId, statusDate: statusDate});
        //var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        //headers.append('authToken',AUTHTOKEN);
        return this.http.get('http://myschool.bluewings.in/bluewings/metrics/counts?v=2', { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //fetch dashboard data
    DashboardService.prototype.fetchDashboardData = function (getSchoolId, academicId, AUTHTOKEN) {
        var json = JSON.stringify({ getSchoolId: getSchoolId, academicId: academicId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/dashboard', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //for dash matrices
    DashboardService.prototype.fetchDashboardMetrices = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/matrices', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //for dashboard registrations
    DashboardService.prototype.fetchDashboardRegistration = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/registration', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //for recent broadcast
    DashboardService.prototype.fetchDashboardRecentBroadcast = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/recentBroadcast', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //fetch academic details
    DashboardService.prototype.fetchAcademicList = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/academiclist', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    DashboardService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [Http])
    ], DashboardService);
    return DashboardService;
}());
//# sourceMappingURL=F:/BW_PR/src/app/dashboard/dashboard.service.js.map