var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewEncapsulation } from '@angular/core';
import { DashboardService } from "./dashboard.service";
import { Router } from "@angular/router";
import { LoginService } from "../login/login.service";
import { HeaderService } from "../header/header.service";
export var DashboardComponent = (function () {
    function DashboardComponent(dashboardservice, router, loginservice, headerservice) {
        this.dashboardservice = dashboardservice;
        this.router = router;
        this.loginservice = loginservice;
        this.headerservice = headerservice;
        this.getAuthToken = localStorage.getItem('usertoken');
        this.getSchoolId = localStorage.getItem('schoolinfo');
        this.endDateDatePicker = new Date();
        this.preLoader = true;
        this.totalBroadcast = 0;
        this.totalStudents = 0;
        this.totalStaffs = 0;
        this.totalClasses = 0;
        this.totalRegStudents = 0;
        this.lengthRegStudentsArray = 0;
        this.regAverage = 0;
        this.studentRegistration = [[0, 0]];
        this.regTickValue = [[0, '']];
        this.activeStaffAppUser = 0;
        this.inActiveStaffAppUser = 0;
        this.activeParentAppUser = 0;
        this.inActiveParentAppUser = 0;
        this.recentBroadcast = [];
        this.isApproved = false;
        //new one
        this.admissionByStatus = true;
        this.paymentHistoryByStatus = true;
        this.getNewAdmissionListArray = [];
        this.getDateAdmission = 0;
        this.noresultadmission = true;
        this.loaderadmission = true;
        this.getSchoolStatusListArray = [];
        this.loaderactivity = true;
        this.viewActivityFeesCollected = 0;
        this.getgetClassFeedListArray = [];
        this.feedCount = false;
        this.onlineFeeCollection = [];
        this.onlineFeeCollectionTable = null;
    }
    //get school info
    DashboardComponent.prototype.getSchoolInfoFromHeader = function () {
        var _this = this;
        this.headerservice.schoolInfo(this.getSchoolId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Get school info on dashboard: ", data);
            // this.getSchoolData.push(data[0][0]);
            //
            // console.log("Get school data array : ----------- ",this.getSchoolData);
            //this.getSchoolData = data[0].;
            _this.getSchoolName = data.schoolName;
            _this.getSchoolImage = data.schoolImage;
            _this.getSchoolState = data.state;
            _this.getSchoolLocality = data.locality;
        }, function (error) { return console.log(error); }, function () { });
    };
    //New admission
    DashboardComponent.prototype.admissionByClick = function (getValue) {
        this.admissionByStatus = getValue;
        if (getValue == true) {
            this.getDateAdmission = 0;
            this.getNewAdmissionList();
        }
        $(".applyPicker").datepicker({ autoclose: true, endDate: new Date() });
    };
    DashboardComponent.prototype.paymentHistoryByClick = function (getValue) {
        this.paymentHistoryByStatus = getValue;
        setTimeout(function () {
            $(".applyPicker").datepicker({ autoclose: true, endDate: new Date() });
        }, 50);
    };
    DashboardComponent.prototype.getNewAdmissionList = function () {
        var _this = this;
        this.loaderadmission = true;
        this.getNewAdmissionListArray = [];
        this.destroyDataTable('new-admission-table');
        this.dashboardservice.fetchNewAdmissionList(this.getSchoolId, DashboardComponent.G_AcademicId, this.getDateAdmission, this.getAuthToken)
            .subscribe(function (data) {
            _this.loaderadmission = false;
            if (data.newadmissionlist.length == 0) {
                _this.noresultadmission = true;
            }
            else {
                _this.noresultadmission = false;
            }
            for (var i = 0; i < data.newadmissionlist.length; i++) {
                _this.getNewAdmissionListArray.push(data.newadmissionlist[i]);
                //check if for loop end, then re initialize datatable
                if (data.newadmissionlist.length - 1 == i) {
                    _this.initializeDataTable('new-admission-table');
                }
            }
        }, function (error) { return console.log(error); }, function () { });
    };
    //get admission list by date
    DashboardComponent.prototype.viewAdmissionByDate = function () {
        this.getDateAdmission = $('.getAdmissionDateBy').val();
        this.getNewAdmissionList();
    };
    //get school activity status
    DashboardComponent.prototype.getSchoolActivityStatus = function () {
        var _this = this;
        this.getTodayDate();
        this.loaderactivity = true;
        this.getSchoolStatusListArray = [];
        this.destroyDataTable('school-activity-table');
        this.dashboardservice.fetchSchoolActivity(this.getschoolStatusDate, this.getAuthToken)
            .subscribe(function (data) {
            for (var i = 0; i < data.length; i++) {
                _this.getSchoolStatusListArray.push(data[i]);
                //check if for loop end, then re initialize datatable
                if (data.length - 1 == i) {
                    _this.initializeDataTable('school-activity-table');
                    _this.loaderactivity = false;
                }
            }
        }, function (error) { return console.log(error); }, function () { });
    };
    //get school activity list by date
    DashboardComponent.prototype.viewSchoolActivityByDate = function () {
        this.getschoolStatusDate = $('.getstatusdate').val();
        this.getSchoolActivityStatus();
        var a = this.getschoolStatusDate.split("/");
        this.getschoolStatusDate = "" + a[0] + "-" + a[1] + "-" + a[2];
    };
    DashboardComponent.prototype.getProperNumber = function (num) {
        return num < 10 ? '0' + num : num;
    };
    DashboardComponent.prototype.getOnlineFeePayCollection = function (singleDate) {
        var _this = this;
        var d;
        var date;
        var endDate;
        if (singleDate) {
            var d = $("#online_fee_col_st1").val();
            var dr = d.split('/');
            date = dr[2] + "-" + dr[1] + "-" + dr[0];
            endDate = date;
        }
        else {
            var s2 = $("#online_fee_col_st2").val();
            var e2 = $("#online_fee_col_end").val();
            if (s2 && e2 && s2 != '' && e2 != '') {
                var s2A = s2.split("/");
                var e2A = e2.split("/");
                date = s2A[2] + "-" + s2A[1] + "-" + s2A[0];
                endDate = e2A[2] + "-" + e2A[1] + "-" + e2A[0];
            }
            else {
                d = new Date();
                date = d.getFullYear() + "-" + this.getProperNumber(d.getMonth()) + "-" + this.getProperNumber(d.getDate());
                endDate = date;
                $("#online_fee_col_st1").val(this.getProperNumber(d.getDate()) + "/" + this.getProperNumber(d.getMonth()) + "/" + d.getFullYear());
            }
        }
        if (this.onlineFeeCollectionTable) {
            this.onlineFeeCollectionTable.destroy();
            this.onlineFeeCollectionTable = null;
        }
        this.dashboardservice.onlineFeeCollectionHistory(date, endDate, this.getAuthToken)
            .subscribe(function (data) {
            _this.onlineFeeCollection = data;
            var h = _this;
            setTimeout(function () {
                h.applyOnlinePayHistoryTable();
            }, 100);
        }, function (error) { return console.log(error); }, function () { });
    };
    DashboardComponent.prototype.applyOnlinePayHistoryTable = function () {
        if (this.onlineFeeCollectionTable == null) {
            this.onlineFeeCollectionTable = $('#onlineFeeCollectionTable').DataTable({
                "dom": 'lCfrtip',
                "order": [],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "oLanguage": {
                    "sEmptyTable": "There are no details to show"
                },
                "language": {
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
        }
    };
    //view activity detail
    DashboardComponent.prototype.viewActivityDetail = function (getClassId, totalStudents, attendence, fessCollected, diaries, teacher1, teacher2, teacher3) {
        $('#offcanvas-view-activity-click').trigger('click');
        var viewActivityClassId = getClassId;
        this.getschoolStatusDate = $('.getstatusdate').val();
        this.viewActivityTotalStudent = totalStudents;
        this.viewActivityAttendence = attendence;
        this.viewActivityFeesCollected = fessCollected;
        this.viewActivityDiaries = diaries;
        this.viewActivityTeacher1 = teacher1;
        this.viewActivityTeacher2 = teacher2;
        this.viewActivityTeacher3 = teacher3;
        this.getTodayDate();
        this.getClassFeed(viewActivityClassId, this.getschoolStatusDate);
    };
    //get feed for class
    DashboardComponent.prototype.getClassFeed = function (viewActivityClassId, getschoolStatusDate) {
        var _this = this;
        this.getgetClassFeedListArray = [];
        this.dashboardservice.fetchClassFeed(viewActivityClassId, getschoolStatusDate, this.getAuthToken)
            .subscribe(function (data) {
            if (data.length == 0) {
                _this.feedCount = false;
            }
            else {
                _this.feedCount = true;
                for (var i = 0; i < data.length; i++) {
                    _this.getgetClassFeedListArray.push(data[i]);
                }
            }
        }, function (error) { return console.log(error); }, function () { });
    };
    DashboardComponent.prototype.getBWAppdata = function () {
        this.getAppMetrices();
    };
    //app data from metrices  /metrices
    DashboardComponent.prototype.getAppMetrices = function () {
        var _this = this;
        this.dashboardservice.fetchAppMetriceData()
            .subscribe(function (data) {
            _this.parentAppAndroidVersion = data.ParentAppAndroidVersion;
            _this.staffAppAndroidVersion = data.StaffAppAndroidVersion;
        }, function (error) { return console.log(error); }, function () { });
    };
    //get today date
    DashboardComponent.prototype.getTodayDate = function () {
        var getDateNow = new Date();
        var getMonth = getDateNow.getUTCMonth() + 1;
        var getDay = getDateNow.getUTCDate();
        var getYear = getDateNow.getUTCFullYear();
        this.todayDate = getYear + "/" + getMonth + "/" + getDay;
    };
    /*Data table operation*/
    DashboardComponent.prototype.destroyDataTable = function (table) {
        $('#' + table).DataTable().destroy();
    };
    DashboardComponent.prototype.initializeDataTable = function (table) {
        setTimeout(function () {
            $('#' + table).DataTable({
                "bSort": false
            });
            //this.scriptInit();
        }, 500);
    };
    DashboardComponent.prototype.getDashboardData = function () {
        var _this = this;
        this.dashboardservice.fetchDashboardData(this.getSchoolId, DashboardComponent.G_AcademicId, this.getAuthToken)
            .subscribe(function (data) {
            //init script
            _this.initScripts();
            setTimeout(function () { return _this.initScripts(); }, 500);
            /*///////////////////////////////////////////*/
            //matrices data
            _this.totalBroadcast = data.dashboardMetrices.totalBroadcast.toLocaleString('en');
            _this.totalStudents = data.dashboardMetrices.totalStudent.toLocaleString('en');
            _this.totalStaffs = data.dashboardMetrices.totalStaff.toLocaleString('en');
            _this.totalClasses = data.dashboardMetrices.totalClass.toLocaleString('en');
            /*//////////////////////////////*/
            //people status
            _this.activeStaffAppUser = data.dashboardMetrices.ActiveStaffAppUser.toLocaleString('en');
            _this.inActiveStaffAppUser = data.dashboardMetrices.InActiveStaffAppUser.toLocaleString('en');
            _this.activeParentAppUser = data.dashboardMetrices.ActiveParentAppUser.toLocaleString('en');
            _this.inActiveParentAppUser = data.dashboardMetrices.InActiveParentAppUser.toLocaleString('en');
            /*//////////////////////////*/
            //recent broadcast
            for (var i = 0; i < data.recentBroadcast.length; i++) {
                var message = decodeURIComponent(data.recentBroadcast[i].message);
                message = _this.getWords(message, 5) + ".....";
                var createdTime = data.recentBroadcast[i].createdTime;
                var createdBy = data.recentBroadcast[i].createdBy;
                var approvedStatus = data.recentBroadcast[i].approvedStatus;
                if (approvedStatus == 0) {
                    _this.isApproved = false;
                }
                else if (approvedStatus == 1) {
                    _this.isApproved = true;
                }
                data.recentBroadcast[i] = {
                    "message": message,
                    "createdTime": createdTime,
                    "createdBy": createdBy,
                    "approvedStatus": _this.isApproved
                };
                _this.recentBroadcast.push(data.recentBroadcast[i]);
            }
            /*//////////////////////////////////////////////*/
            //registarion graph
            _this.lengthRegStudentsArray = data.studentRegistration.length;
            _this.totalRegStudents = _this.totalStudents;
            for (var i = 0; i < _this.lengthRegStudentsArray; i++) {
                if (data.studentRegistration[i].year != 0 && data.studentRegistration[i].year != null) {
                    var coord1 = [];
                    coord1.push(i + 1);
                    coord1.push(data.studentRegistration[i].numberofstudents);
                    var coord2 = [];
                    coord2.push(i + 1);
                    coord2.push(data.studentRegistration[i].year);
                    _this.studentRegistration.push(coord1);
                    _this.regTickValue.push(coord2);
                }
            }
            var a = data.studentRegistration[0].numberofstudents;
            var b = _this.totalRegStudents - a;
            if (a > b) {
                _this.regAverage = (b / a) * 100;
            }
            else if (a < b) {
                _this.regAverage = (a / b) * 100;
            }
            _this.regAverage = parseFloat(_this.regAverage.toFixed(2));
            //get academic Id
            _this.getAcademicData();
        }, function (error) { return console.log(error); }, function () {
        });
    };
    //get academic details
    DashboardComponent.prototype.getAcademicData = function () {
        var _this = this;
        this.dashboardservice.fetchAcademicList(this.getSchoolId, this.getAuthToken)
            .subscribe(function (data) {
            for (var i = 0; i < data.academicList.length; i++) {
                //this.schoolAcademicList.push(data.academicList[i]);
                if (data.academicList[i].InActive == 0) {
                    DashboardComponent.G_AcademicId = data.academicList[i].pkAcademicId;
                    //get admission list
                    _this.getNewAdmissionList();
                }
            }
        }, function (error) { return console.log(error); }, function () { });
    };
    //string wrap to words function
    DashboardComponent.prototype.getWords = function (str, count) {
        return str.split(/\s+/).slice(0, count).join(" ");
    };
    //getWords(str,5);
    //token auth
    DashboardComponent.prototype.isValidToken = function () {
        var _this = this;
        var isValidToken = false;
        var isValidSchool = 0;
        this.loginservice.isValidToken(this.getAuthToken)
            .subscribe(function (data) {
            isValidToken = data.isValid;
            if (isValidToken == true) {
                _this.loginservice.isValidSchoolId(_this.getSchoolId)
                    .subscribe(function (data) {
                    isValidSchool = data.schoolExist;
                    if (isValidSchool != 1) {
                        _this.router.navigate(['/login']);
                    }
                    else {
                        _this.getDashboardData();
                        _this.getSchoolInfoFromHeader();
                    }
                }, function (error) { return console.log(error); }, function () { });
            }
            else {
                _this.router.navigate(['/login']);
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    DashboardComponent.prototype.initScripts = function () {
        $(".after").empty();
        $("#device-breakpoints").remove();
        $('.after').append("<script  async src='assets/js/libs/DataTables/jquery.dataTables.min.js'></" + "script><script ype='text/javascript' src='assets/js/misc/jquery.particleground.js'></" + "script><script>$('.particles-bg').particleground({dotColor: '#f3f3f3',lineColor: '#ffffff',lineWidth: .3});</" + "script>");
        $(".applyPicker").datepicker({ autoclose: true, endDate: new Date() });
    };
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isValidToken();
        setTimeout(function () { return _this.initScripts(); }, 500);
    };
    DashboardComponent.G_AcademicId = 0;
    DashboardComponent = __decorate([
        Component({
            selector: 'app-dashboard',
            templateUrl: './dashboard.component.html',
            styleUrls: [
                'assets/css/theme-default/bootstrap.css?1422792965',
                'assets/css/theme-default/materialadmin.css?1425466319',
                'assets/css/theme-default/font-awesome.min.css?1422529194',
                'assets/css/theme-default/material-design-iconic-font.min.css?1421434286',
                'assets/css/theme-default/libs/toastr/toastr.css?1425466569',
                'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
                'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
                '_crop/css/style.css',
                '_crop/css/style-example.css',
                '_crop/css/jquery.Jcrop.css',
                './dashboard.component.css'],
            encapsulation: ViewEncapsulation.None,
            providers: [DashboardService, LoginService, HeaderService]
        }), 
        __metadata('design:paramtypes', [DashboardService, Router, LoginService, HeaderService])
    ], DashboardComponent);
    return DashboardComponent;
}());
//# sourceMappingURL=F:/BW_PR/src/app/dashboard/dashboard.component.js.map