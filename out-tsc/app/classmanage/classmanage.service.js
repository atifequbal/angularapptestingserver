var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import 'rxjs/Rx';
import { AppSettings } from "../../config/config";
export var ClassManageService = (function () {
    function ClassManageService(http) {
        this.http = http;
        this._url = AppSettings.API_ENDPOINT;
    }
    ClassManageService.prototype.schoolClassManagementGetCategoryList = function (getSchoolId, academicId, AUTHTOKEN) {
        var json = JSON.stringify({ getSchoolId: getSchoolId, academicId: academicId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/schoolclassmanagement/categorylist', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.schoolClassManagementGetClassList = function (getSchoolId, academicId, AUTHTOKEN) {
        var json = JSON.stringify({ getSchoolId: getSchoolId, academicId: academicId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/schoolclassmanagement/classlist', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //list all subjects
    ClassManageService.prototype.schoolClassManagementGetSubjectList = function (getSchoolId, AUTHTOKEN) {
        var json = JSON.stringify({ getSchoolId: getSchoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/schoolclassmanagement/subjectlist', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //list all subjects [view]
    ClassManageService.prototype.schoolClassManagementGetSubjectListView = function (subjectId, AUTHTOKEN) {
        var json = JSON.stringify({ subjectId: subjectId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/schoolclassmanagement/subjectlist/view', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.getStudentListClassSection = function (schoolId, academicId, classId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, academicId: academicId, classId: classId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/studentlist/studentlistclassection', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.getCategoryListService = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageClass/categoryList', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.getCategoryClassListService = function (schoolId, academicId, categoryId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, academicId: academicId, categoryId: categoryId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageClass/classStandardListByCategory', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //add new category
    ClassManageService.prototype.addCategoryNewCategoryService = function (categoryName, schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ categoryName: categoryName, schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageClass/categoryAddNew', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //add new category
    ClassManageService.prototype.updateCategoryListCategoryService = function (categoryNewName, schoolId, categoryId, AUTHTOKEN) {
        var json = JSON.stringify({ categoryNewName: categoryNewName, schoolId: schoolId, categoryId: categoryId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageClass/categoryUpdateName', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //update class name
    ClassManageService.prototype.updateClassNameService = function (standardId, standard, AUTHTOKEN) {
        var json = JSON.stringify({ classStandardsId: standardId, classStandard: standard });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/className/updateclassname', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //list class classes
    ClassManageService.prototype.getClassListService = function (schoolId, academicId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, academicId: academicId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageClass/classList', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.getClassSectionListService = function (schoolId, standardId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, classId: standardId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classSectionName', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.getClassViewaddNewClassSections = function (schoolId, NewClassName, SectionName, categoryId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, className: NewClassName, section: SectionName, categoryId: categoryId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classManagement/classmanagement_addStandard', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //list all subjects
    ClassManageService.prototype.getSubjectList = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageClass/subjectList', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.updateSubject = function (subId, subName, AUTHTOKEN) {
        var json = JSON.stringify({ subjectId: subId, subjectName: subName });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classManagement/classmanagement_standard_UpdateSubject', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.listClassCategory = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classManagement/category', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.listClassCategoryDetailList = function (schoolId, categoryId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, categoryId: categoryId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classManagement/categorydetailslist', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //add new category
    ClassManageService.prototype.addNewCategoryService = function (schoolId, categoryName, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, catName: categoryName });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classManagement/category/categoryaddnewcategory', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.listClassSection = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classManagement/listStandards', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //add new category
    ClassManageService.prototype.addNewClassSections = function (schoolId, NewClassName, SectionName, categoryId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, className: NewClassName, section: SectionName, categoryId: categoryId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classManagement/classmanagement_addStandard', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //list class section in standard view
    ClassManageService.prototype.viewStandardListSection = function (classId, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classSectionName', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //delete section from std view
    ClassManageService.prototype.deleteSectionInStdView = function (secid, AUTHTOKEN) {
        var json = JSON.stringify({ classId: secid });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classManagement/classmanagement_standard_DeleteSection', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //Update section from std view
    ClassManageService.prototype.UpdateSectionInStdView = function (stdId, secid, section, categoryId, AUTHTOKEN) {
        var json = JSON.stringify({ classStandardId: stdId, classId: secid, section: section, categoryId: categoryId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classManagement/classmanagement_standard_UpdateSection', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.fetchSubjectListandStaff = function (classId, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/subject/class/subjectswithstaff', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    //list class standards
    ClassManageService.prototype.viewStandardListStudent = function (schoolId, academicId, categoryId, classId, sectionId, countFrom, lengthOf, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, academicId: academicId, categoryId: categoryId, classId: classId, sectionId: sectionId, countFrom: countFrom, lengthOf: lengthOf });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/studentlist', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.assignSubjectToClass = function (classId, subjectId, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classId, subjectId: subjectId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/subject/class/add', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.unAssignSubjectTostaff = function (classId, staffId, subjectId, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classId, staffId: staffId, subjectId: subjectId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/subject/class/unassignsubjecttostaff', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.addSubject = function (subjectName, AUTHTOKEN) {
        var json = JSON.stringify({ subjectname: subjectName });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/class_management/subjects/addnewsubject', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.removeSubjectfromClass = function (classId, subjectId, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classId, subjectId: subjectId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/subject/class/remove', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.searchStaff = function (searchData, AUTHTOKEN) {
        var json = JSON.stringify({ searchData: searchData });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/stafflist/searchStaffWithName', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.searchClassTeacher = function (searchData, AUTHTOKEN) {
        var json = JSON.stringify({ searchData: searchData });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/stafflist/getStaffWithClassTeacherPermission', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.assignSubjectTostaff = function (staffId, classId, subjectId, AUTHTOKEN) {
        var json = JSON.stringify({ staffId: staffId, classId: classId, subjectId: subjectId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/subject/class/assignsubjecttostaff', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.fetchClassTeacher = function (classId, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classManagement/classmanagement/getClassTeacher', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.addClassTeacher = function (classId, staffId, staffPosition, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classId, staffId: staffId, staffPosition: staffPosition });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classManagement/classmanagement/assignClassTeacher', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.removeteacher = function (classId, staffId, staffPosition, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classId, staffId: staffId, staffPosition: staffPosition });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classManagement/classmanagement/removeClassTeacher', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.updateClassOrder = function (classStandardId, AUTHTOKEN) {
        var json = JSON.stringify({ classStdList: classStandardId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/schoolclassmanagement/classlist/updateClassListSequence', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.updateClassSectionOrder = function (classId, AUTHTOKEN) {
        var json = JSON.stringify({ sectionList: classId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/schoolclassmanagement/updateClassSectionSequence', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService.prototype.sectionDetail = function (classId, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classSectionName/sectionDetail', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    ClassManageService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [Http])
    ], ClassManageService);
    return ClassManageService;
}());
//# sourceMappingURL=F:/BW_PR/src/app/classmanage/classmanage.service.js.map