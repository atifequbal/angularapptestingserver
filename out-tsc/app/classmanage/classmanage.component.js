var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewEncapsulation, Renderer } from '@angular/core';
import { Router } from "@angular/router";
import { LoginService } from "../login/login.service";
import { ClassManageService } from "./classmanage.service";
import { GroupService } from "../groups/groups.service";
import { AppSettings } from "../../config/config";
import { FormValidator } from "../form.validator";
import { DashboardComponent } from "../dashboard/dashboard.component";
export var ClassmanageComponent = (function () {
    function ClassmanageComponent(router, loginservice, classManageService, groupService, renderer, formValidator) {
        this.router = router;
        this.loginservice = loginservice;
        this.classManageService = classManageService;
        this.groupService = groupService;
        this.formValidator = formValidator;
        this._AcademicId = DashboardComponent.G_AcademicId;
        this.academicId = 0;
        this.getAuthToken = localStorage.getItem('usertoken');
        this.getSchoolId = localStorage.getItem('schoolinfo');
        //category
        this.getCategoryListArray = [];
        this.getCategoryClassListArray = [];
        this.updateClassStdName = { className: "" };
        this.addNewCategoryCategoryName = "";
        this.editCategoryCategoryName = "";
        //class
        this.getSectionNameInClass = "";
        this.getClassListArray = [];
        this.getClassViewClassSectionArray = [];
        this.getClassSectionStudentListArray = [];
        this.addNewSectionStatus = false;
        this.getNewSectionName = "";
        this.getSectiondIdClass = "";
        this.getClassStdId = "";
        this.classStdId = "";
        //subjects
        this.getSubjectListArray = [];
        this.getSubjectListViewArray = [];
        this.classCategory = [];
        this.classStandards = [];
        this.classCategoryDetailList = [];
        this.schoolCategories = [];
        this.viewClassCategoryId = 0;
        this.viewClassCategorytotalclasses = 0;
        this.viewClassCategorytotalsection = 0;
        this.viewClassCategorytotalStudents = 0;
        this.viewClassCategoryName = null;
        this.newCategoryName = null;
        this.getSubjectId = "";
        this.newStandardName = null;
        this.newStandardSections = [];
        //view standards
        this.viewStandardName = null;
        this.viewStandardcategoryName = null;
        this.viewStandardcategoryId = null;
        this.viewStandardtotalSections = 0;
        this.viewStandardtotalStudents = 0;
        this.viewStandardtotalStaffs = 0;
        this.viewStandardId = "";
        this.viewStandardIdInClass = "";
        this.viewStandardSections = [];
        this.selectedClassId = "";
        this.selectedCatIdAddSection = 0;
        this.selectedSectionIdViewStandards = 0;
        this.selectedStatuaViewStandards = null;
        this.classlistbycat = [];
        this.sectionId = "";
        this.subjectList = "";
        this.classSectionId = "";
        this.teacherList = [];
        this.staffsList = [];
        this.classList = [];
        this.addSub = { subjectname: "" };
        this.teacher = { searchTeacher: "" };
        this.sectionDetail = {};
        this.permissions = {};
        this.studentListClassSectionStatus = true;
    }
    ClassmanageComponent.prototype.classManagementGetClassCategory = function () {
        var _this = this;
        this.getCategoryListArray = [];
        this.destroyDataTable('categoryview-detail-table');
        this.classManageService.schoolClassManagementGetCategoryList(this.getSchoolId, this._AcademicId, this.getAuthToken)
            .subscribe(function (data) {
            for (var i = 0; i < data.classcategory.length; i++) {
                _this.getCategoryListArray.push(data.classcategory[i]);
                if (data.classcategory.length - 1 == i) {
                    _this.initializeDataTable('categoryview-detail-table');
                }
            }
            _this.getClassList();
        }, function (error) {
            return function () {
            };
        });
    };
    //get class category list
    ClassmanageComponent.prototype.getClassCategoryList = function () {
        var _this = this;
        this.classManageService.listClassCategory(localStorage.getItem('schoolinfo'), localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            _this.destroyDataTable('class-category-table');
            if (data.classcategory.length >= 0) {
                for (var i = 0; i < data.classcategory.length; i++) {
                    _this.classCategory.push(data.classcategory[i]);
                    if (data.classcategory.length - 1 == i) {
                        _this.initializeDataTable('class-category-table');
                        _this.scriptInit();
                    }
                }
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.getClassCategoryDetailList = function () {
        var _this = this;
        this.classManageService.listClassCategoryDetailList(localStorage.getItem('schoolinfo'), this.viewClassCategoryId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            _this.destroyDataTable('categoryviewdetail');
            if (data.classlist.length >= 0) {
                for (var i = 0; i < data.classlist.length; i++) {
                    _this.classCategoryDetailList.push(data.classlist[i]);
                    if (data.classlist.length - 1 == i) {
                        _this.initializeDataTable('categoryviewdetail');
                    }
                }
            }
        }, function (error) {
            return function () {
            };
        });
    };
    //add new category
    ClassmanageComponent.prototype.addNewCategory = function () {
        this.classManageService.addNewCategoryService(localStorage.getItem('schoolinfo'), this.newCategoryName, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            if (data.addNewCategory == false) {
                customToastr('Error!! Please try again', 'error');
            }
            else {
                customToastr('New category added', 'success');
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.viewClassCategory = function (getid, totalclasses, totalsection, totalStudents, categoryname) {
        this.viewClassCategoryId = getid;
        this.viewClassCategorytotalclasses = totalclasses;
        this.viewClassCategorytotalsection = totalsection;
        this.viewClassCategorytotalStudents = totalStudents;
        this.viewClassCategoryName = categoryname;
        this.getClassCategoryDetailList();
    };
    //get class standard list
    ClassmanageComponent.prototype.getClassStandardList = function () {
        var _this = this;
        this.classManageService.listClassSection(localStorage.getItem('schoolinfo'), localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            _this.destroyDataTable('class-standard-table');
            if (data.schoolStandards.length >= 0) {
                for (var i = 0; i < data.schoolStandards.length; i++) {
                    _this.classStandards.push(data.schoolStandards[i]);
                    if (data.schoolStandards.length - 1 == i) {
                        _this.initializeDataTable('class-standard-table');
                    }
                }
            }
        }, function (error) {
            return function () {
            };
        });
    };
    //get all class category
    ClassmanageComponent.prototype.getClassCategories = function () {
        var _this = this;
        this.groupService.groupsClassCategories(localStorage.getItem('schoolinfo'), localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            for (var i = 0; i < data.schoolCategory.length; i++) {
                _this.schoolCategories.push(data.schoolCategory[i]);
            }
            $(".insertsection div.bootstrap-tagsinput:not(:first)").remove();
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.sectionDetails = function () {
        var _this = this;
        this.classManageService.sectionDetail(this.getSectiondIdClass, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            _this.sectionDetail = data[0];
        }, function (error) {
            return function () {
            };
        });
    };
    //selected category for section add
    ClassmanageComponent.prototype.selectcategoryAddSection = function (selectedCatId) {
        this.selectedCatIdAddSection = selectedCatId;
    };
    ClassmanageComponent.prototype.selectSectionViewStandards = function (selectedSection) {
        this.selectedSectionIdViewStandards = selectedSection;
    };
    ClassmanageComponent.prototype.selectStatusStandards = function (selectedStatus) {
        this.selectedStatuaViewStandards = selectedStatus;
    };
    //add new section
    ClassmanageComponent.prototype.addNewSection = function () {
        $(".insertsection div.bootstrap-tagsinput:not(:first)").remove();
    };
    ClassmanageComponent.prototype.addNewSection2 = function (newStandardSections) {
        var _this = this;
        if (this.formValidator.validateIsData(newStandardSections)) {
            $('#stdName').text("Please wait");
            $('#stdName').attr('disabled', true);
            this.newStandardSections = newStandardSections.split(',');
            if (this.newStandardSections.length > 0 && this.formValidator.validateIsData(this.newStandardName) && this.selectedCatIdAddSection != 0 && this.selectedCatIdAddSection != null && this.selectedCatIdAddSection != undefined && this.newStandardSections != null && this.newStandardSections != undefined && this.newStandardSections.length != 0) {
                for (var i = 0; i < this.newStandardSections.length; i++) {
                    this.classManageService.addNewClassSections(localStorage.getItem('schoolinfo'), this.newStandardName, this.newStandardSections[i], this.selectedCatIdAddSection, localStorage.getItem('usertoken'))
                        .subscribe(function (data) {
                        if (data.status == true) {
                            $('#stdName').text("ADD");
                            $('#stdName').attr('disabled', false);
                            customToastr('New standard added', 'success');
                            $('#addstandards').trigger("click");
                            _this.getClassList();
                        }
                        else if (data.status == false) {
                            $('#stdName').text("ADD");
                            $('#stdName').attr('disabled', false);
                            customToastr('Class and section are already exist, Please enter different name!', 'error');
                        }
                        else {
                            $('#stdName').text("ADD");
                            $('#stdName').attr('disabled', false);
                            customToastr('Error!! Please try again', 'error');
                        }
                    }, function (error) {
                        return function () {
                        };
                    });
                }
            }
            else {
                $('#stdName').text("ADD");
                $('#stdName').attr('disabled', false);
                customToastr('Please fill all fields', 'error');
            }
        }
        else {
            customToastr('Please fill all fields', 'error');
        }
    };
    ClassmanageComponent.prototype.addNewSectionInDetail = function () {
        $('#add-section-new').popover('show');
        $('.close-add-section').click(function () {
            $('#add-section-new').popover('hide');
        });
    };
    //delete section from standard
    ClassmanageComponent.prototype.deleteSectionInStdView = function () {
        var _this = this;
        this.classManageService.deleteSectionInStdView(this.getSectiondIdClass, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            if (data.status == false) {
                customToastr('Error!! Please try again', 'error');
            }
            else {
                customToastr('Delete section success', 'success');
                $("#deletepopupclose").trigger("click");
                _this.getStandardViewSectionList();
                _this.getSectionListClassView(_this.classStdId);
                _this.getClassList();
                _this.getCategoryClassListtotalSections = _this.getCategoryClassListtotalSections - 1;
            }
        }, function (error) {
            return function () {
            };
        });
    };
    //Update section from standard
    ClassmanageComponent.prototype.UpdateSectionInStdView = function (getSectionNameInClass) {
        var _this = this;
        $('#updateSec').text("Please wait");
        $('#updateSec').attr('disabled', true);
        var catId = $('#cat').val();
        if (this.formValidator.validateIsData(getSectionNameInClass) && catId && this.formValidator.validateIsData(catId)) {
            this.classManageService.UpdateSectionInStdView(this.classStdId, this.getSectiondIdClass, getSectionNameInClass, catId, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                $('#updateSec').text("Update");
                $('#updateSec').attr('disabled', false);
                if (data.status == true) {
                    customToastr('Section updated successfully', 'success');
                    $("#closeupdatesection").trigger("click");
                    _this.getSectionListClassView(_this.classStdId);
                    _this.getStandardViewSectionList();
                }
                else if (data.status == false) {
                    customToastr('Section name is already exist!! Please try different name!', 'error');
                }
                else {
                    customToastr('Section is not updated!! Please try again', 'error');
                }
            }, function (error) {
                return function () {
                };
            });
        }
        else {
            $('#updateSec').text("Update");
            $('#updateSec').attr('disabled', false);
            customToastr('Please fill all fields', 'error');
        }
    };
    //class standatrds detailed view///////////
    ClassmanageComponent.prototype.viewClassStandards = function (className, totalSections, categoryName, totalStudents, totalStaffs, standardId, categoryId) {
        this.viewStandardName = className;
        this.viewStandardId = standardId;
        this.viewStandardIdInClass = standardId;
        this.viewStandardcategoryName = categoryName;
        this.viewStandardcategoryId = categoryId;
        this.viewStandardtotalSections = totalSections.replace('Sections', '');
        this.viewStandardtotalStudents = totalStudents.replace('Students', '');
        this.viewStandardtotalStaffs = totalStaffs.replace('Staffs', '');
        this.getStandardViewSectionList();
    };
    //get list of section in standard view
    ClassmanageComponent.prototype.getStandardViewSectionList = function () {
        var _this = this;
        this.viewStandardSections = [];
        this.classManageService.viewStandardListSection(this.getClassStdId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            for (var i = 0; i < data.length; i++) {
                _this.viewStandardSections.push(data[i]);
            }
        }, function (error) {
            return function () {
            };
        });
    };
    /*Sample datatable /////////////////////////////////////////////*/
    ClassmanageComponent.prototype.initDataTable = function (academicId, categoryId, classId, sectionId, studenttableName) {
        $('#' + studenttableName).DataTable({
            destroy: true,
            language: {
                "processing": "<h3 style='color: #2196f3;font-size: 2em;font-weight: 100;'>Please wait...</h3>"
            },
            "processing": true,
            "serverSide": true,
            "ajax": {
                beforeSend: function (request) {
                    request.setRequestHeader("Content-Type", 'application/json');
                    request.setRequestHeader("authToken", localStorage.getItem('usertoken'));
                },
                url: AppSettings.API_ENDPOINT + '/studentlist',
                type: "POST",
                'data': function (d) {
                    return JSON.stringify({
                        "schoolId": localStorage.getItem('schoolinfo'),
                        "academicId": academicId,
                        "categoryId": categoryId,
                        "classId": classId,
                        "sectionId": sectionId,
                        "countFrom": d.start,
                        "lengthOf": d.length
                    });
                }
            },
            "columns": [
                { "data": "rollNumber" },
                { "data": "studentName" },
                { "data": "parentName" },
                { "data": "mobileNumber" },
                { "data": "dateOfBirth" },
                { "data": "gender" },
                { "data": "isAppUser" }
            ]
        });
    };
    //get current academic id
    ClassmanageComponent.prototype.getAcademicList = function () {
        var _this = this;
        this.groupService.groupsStudentAcademicList(localStorage.getItem('schoolinfo'), localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            for (var i = 0; i < data.academicList.length; i++) {
                if (data.academicList[i].InActive == 0) {
                    _this.academicId = data.academicList[i].pkAcademicId;
                }
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.getCategoryList = function () {
        var _this = this;
        this.getCategoryListArray = [];
        this.destroyDataTable('categoryview-detail-table');
        this.classManageService.getCategoryListService(this.getSchoolId, this.getAuthToken)
            .subscribe(function (data) {
            for (var i = 0; i < data.cateogorylist.length; i++) {
                _this.getCategoryListArray.push(data.cateogorylist[i]);
                if (data.cateogorylist.length - 1 == i) {
                    _this.initializeDataTable('categoryview-detail-table');
                }
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.classDetailView = function (catname, ttclasses, ttsections, ttstudents, catid) {
        $('.offcanvas-view-class-categories').click();
        this.getViewCatName = catname;
        this.getViewttclasses = ttclasses;
        this.getViewttsections = ttsections;
        this.getViewttstudents = ttstudents;
        this.getViewcatid = catid;
        this.getCategoryClassList();
    };
    ClassmanageComponent.prototype.getCategoryClassList = function () {
        var _this = this;
        this.getCategoryClassListArray = [];
        this.destroyDataTable('category-detail-view-table');
        this.classManageService.getCategoryClassListService(this.getSchoolId, this.academicId, this.getViewcatid.CategoryId, this.getAuthToken)
            .subscribe(function (data) {
            _this.classlistbycat = data;
            setTimeout(function () {
                $('#category-detail-view-table').DataTable();
            }, 100);
            if (_this.classlistbycat.length == 0) {
                $('#dataCategory').show();
                $('#tablebycategorylist').hide();
            }
            else {
                $('#dataCategory').hide();
                $('#tablebycategorylist').show();
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.categoryClassDetailView = function (className, totalSections, totalStudents, classId) {
        this.classStandardName = className;
        this.getCategoryClassListtotalSections = totalSections;
        this.getCategoryClassListtotalStudents = totalStudents;
        this.getCategoryClassListclassId = classId;
    };
    ClassmanageComponent.prototype.offcanvasviewclass = function (categoryclass, classId) {
        $("#offcanvas-view-class").css("transform", "translate(-1000px, 0px)");
        this.getCategoryClassListclassIdView = categoryclass.classId;
        this.getSectionListClassView(classId);
        this.getClassSectionStudentListArray = [];
    };
    ClassmanageComponent.prototype.addNewCategoryCategoryList = function () {
        var _this = this;
        $("#addNewCat").text("Please wait");
        $("#addNewCat").attr('disabled', true);
        if (!this.formValidator.validateIsData(this.addNewCategoryCategoryName)) {
            $("#addNewCat").text("ADD");
            $("#addNewCat").attr('disabled', false);
            customToastr('Please enter a valid name!', 'error');
        }
        else {
            this.classManageService.addCategoryNewCategoryService(this.addNewCategoryCategoryName, this.getSchoolId, this.getAuthToken)
                .subscribe(function (data) {
                if (data.status == true) {
                    $("#addNewCat").text("ADD");
                    $("#addNewCat").attr('disabled', false);
                    customToastr('New category added!', 'success');
                    _this.addNewCategoryCategoryName = "";
                    $("#addNewCategoryCategoryName").val("");
                    $("#addcategory").trigger("click");
                    _this.getCategoryList();
                }
                else if (data.status == false) {
                    $("#addNewCat").text("ADD");
                    $("#addNewCat").attr('disabled', false);
                    customToastr('Category name is already exist, Please enter different name!', 'error');
                }
                else {
                    customToastr('Category is not added!', 'error');
                }
            }, function (error) {
                return function () { };
            });
        }
    };
    ClassmanageComponent.prototype.updateCategoryCategoryName = function () {
        var _this = this;
        $('#upCatName').text("Please wait");
        $('#upCatName').attr('disabled', true);
        if (!this.formValidator.validateIsData(this.editCategoryCategoryName)) {
            $('#upCatName').text("Update");
            $('#upCatName').attr('disabled', false);
            customToastr('Please do some changes!', 'error');
        }
        else {
            this.classManageService.updateCategoryListCategoryService(this.editCategoryCategoryName, this.getSchoolId, this.getViewcatid.CategoryId, this.getAuthToken)
                .subscribe(function (data) {
                if (data.categoryUpdateStatus == true) {
                    $('#upCatName').text("Update");
                    $('#upCatName').attr('disabled', false);
                    _this.getViewCatName = _this.editCategoryCategoryName;
                    customToastr('Category name updated!', 'success');
                    $('#closeUpdateCatPopup').trigger("click");
                    _this.getCategoryList();
                }
                else if (data.status == false) {
                    $('#upCatName').text("Update");
                    $('#upCatName').attr('disabled', false);
                    customToastr('Given category name is already exist!, please give different name!', 'error');
                }
                else {
                    $('#upCatName').text("Update");
                    $('#upCatName').attr('disabled', false);
                    customToastr('Category name is not updated!, please try again!', 'error');
                }
            }, function (error) {
                return function () { };
            });
        }
    };
    ClassmanageComponent.prototype.updateClassName = function (updateClassStdName) {
        var _this = this;
        $('#classButton').text("Please wait");
        $('#classButton').attr('disabled', true);
        this.updateClassStdName.className = $('.stdName').val();
        if (this.updateClassStdName.className != null && this.updateClassStdName.className != "" && this.updateClassStdName.className != undefined) {
            this.classManageService.updateClassNameService(this.classStdId, this.updateClassStdName.className, this.getAuthToken)
                .subscribe(function (data) {
                $('#classButton').text("Update");
                $('#classButton').attr('disabled', false);
                if (data.classNameUpdateStatus == true) {
                    _this.classStandardName = _this.updateClassStdName.className;
                    customToastr('Class name updated!', 'success');
                    $("#closeUpdateClass").trigger("click");
                    _this.getClassList();
                    _this.getSectionListClassView(_this.classStdId);
                }
                else if (data.status == false) {
                    customToastr('Given class name is already exist, Please give different name!', 'error');
                }
                else {
                    customToastr('Class name is not updated!', 'error');
                }
            }, function (error) {
                return function () { };
            });
        }
        else {
            $('#classButton').text("Update");
            $('#classButton').attr('disabled', false);
            customToastr('Please enter class name', 'error');
        }
    };
    //add new class modal click
    ClassmanageComponent.prototype.addNewClassModal = function () {
        $(".insertsection div.bootstrap-tagsinput:not(:first)").remove();
        this.getClassCategories();
    };
    ClassmanageComponent.prototype.getClassList = function () {
        var _this = this;
        this.getClassListArray = [];
        this.destroyDataTable('classview-detail-table');
        this.groupService.groupsStudentAcademicList(this.getSchoolId, this.getAuthToken)
            .subscribe(function (data) {
            for (var i = 0; i < data.academicList.length; i++) {
                //this.schoolAcademicList.push(data.academicList[i]);
                if (data.academicList[i].InActive == 0) {
                    _this.academicId = data.academicList[i].pkAcademicId;
                    //call class list
                    _this.classManageService.schoolClassManagementGetClassList(_this.getSchoolId, _this.academicId, _this.getAuthToken)
                        .subscribe(function (data) {
                        for (var i = 0; i < data.classlist.length; i++) {
                            _this.getClassListArray.push(data.classlist[i]);
                            if (data.classlist.length - 1 == i) {
                                _this.initializeDataTable('classview-detail-table');
                            }
                        }
                        _this.listAllSubjects();
                    }, function (error) {
                        return function () { };
                    });
                }
            }
        }, function (error) {
            return function () { };
        });
    };
    ClassmanageComponent.prototype.getclassDetailView = function (className, totalSections, categoryName, classId, totalStudents, totalStaffs, catId, classStdId) {
        $('#updateSectionSequence').hide();
        $('#section-List').show();
        $('.offcanvas-view-class').trigger("click");
        this.classStandardName = className;
        this.getViewCatName = categoryName;
        this.getClassListCatId = catId;
        this.viewClassCategorytotalStudents = totalStudents;
        this.viewStandardtotalStaffs = totalStaffs;
        this.getCategoryClassListtotalSections = totalSections;
        this.getCategoryClassListclassId = classId;
        this.classStdId = classStdId;
        this.getSectionListClassView(this.getCategoryClassListclassId);
    };
    //get section list with classtandard id
    ClassmanageComponent.prototype.getSectionListClassView = function (classStandardId) {
        var _this = this;
        this.destroyDataTable('view-class-detail-student-table');
        this.getClassStandardIdList = classStandardId;
        this.getClassViewClassSectionArray = [];
        this.classManageService.getClassSectionListService(this.getSchoolId, this.getClassStandardIdList, this.getAuthToken)
            .subscribe(function (data) {
            for (var i = 0; i < data.length; i++) {
                _this.getClassViewClassSectionArray.push(data[i]);
            }
        }, function (error) {
            return function () { };
        });
    };
    ClassmanageComponent.prototype.selectcategoryViewClassSection = function (selSecId) {
        this.destroyDataTable('view-class-detail-student-table');
        this.getSeletedSectionClassIdView = $('#selectedsection').val();
        this.getSeletedSectionSectionId = selSecId;
    };
    ClassmanageComponent.prototype.classViewAddNewSection = function () {
        this.addNewSectionStatus = true;
    };
    ClassmanageComponent.prototype.classViewAddNewSectionCancel = function () {
        this.addNewSectionStatus = false;
    };
    ClassmanageComponent.prototype.classViewAddNewSectionProcess = function (classStandardId) {
        var _this = this;
        $('#addSectionButton').text('Please wait');
        $('#addSectionButton').attr('disabled', true);
        if (!this.formValidator.validateIsData(this.getNewSectionName)) {
            $('#addSectionButton').text('ADD SECTION');
            $('#addSectionButton').attr('disabled', false);
            customToastr('Please enter a valid section name!', 'error');
        }
        else {
            this.classManageService.getClassViewaddNewClassSections(this.getSchoolId, this.classStandardName, this.getNewSectionName, this.getClassListCatId, this.getAuthToken)
                .subscribe(function (data) {
                if (data.status == true) {
                    $('#addSectionButton').text('ADD SECTION');
                    $('#addSectionButton').attr('disabled', false);
                    _this.addNewSectionStatus = false;
                    customToastr('New section added to the class successfully!', 'success');
                    _this.getNewSectionName;
                    _this.getSectionListClassView(_this.classStdId);
                    _this.getClassList();
                    _this.getCategoryClassListtotalSections = _this.getCategoryClassListtotalSections + 1;
                }
                else if (data.status == false) {
                    $('#addSectionButton').text('ADD SECTION');
                    $('#addSectionButton').attr('disabled', false);
                    customToastr('Class section already exist, Please use different name!', 'error');
                }
                else {
                    $('#addSectionButton').text('ADD SECTION');
                    $('#addSectionButton').attr('disabled', false);
                    customToastr('Something went wrong, Please try again!', 'error');
                }
            }, function (error) {
                return function () { };
            });
        }
    };
    //deletesection and edit
    ClassmanageComponent.prototype.editSectionPopup = function () {
        $('#editSectionModal').modal('show');
    };
    ClassmanageComponent.prototype.deleteSectionPopup = function () {
        $('#deletesectionmodal').modal('show');
    };
    ClassmanageComponent.prototype.classViewSectionName = function (sectionName, sectionId, classStdId) {
        this.getSectionNameInClass = sectionName;
        this.getSectiondIdClass = sectionId;
        this.getClassStdId = classStdId;
    };
    /*Subject start////////////////////////////*/
    ClassmanageComponent.prototype.viewSubject = function () {
        $('#subject-Status').show();
        $('#subject-table').hide();
        var selectSection = $("#selectSectionId").val();
        this.sectionId = selectSection;
        if (selectSection != '') {
            if (selectSection != 0) {
                this.getSubjectListByClass(selectSection);
            }
            else {
                $('#subject-Status').hide();
                customToastr('Please select section', 'error');
            }
        }
    };
    ClassmanageComponent.prototype.listAllSubjects = function () {
        var _this = this;
        this.destroyDataTable('subject-List-table');
        this.getSubjectListArray = [];
        this.classManageService.schoolClassManagementGetSubjectList(this.getSchoolId, this.getAuthToken)
            .subscribe(function (data) {
            for (var i = 0; i < data.subjectlist.length; i++) {
                _this.getSubjectListArray.push(data.subjectlist[i]);
                if (data.subjectlist.length - 1 == i) {
                    _this.initializeDataTable('subject-List-table');
                }
            }
        }, function (error) {
            return function () { };
        });
    };
    ClassmanageComponent.prototype.getSubjectListByClass = function (classId) {
        var _this = this;
        this.selectedClassId = classId;
        this.classManageService.fetchSubjectListandStaff(this.selectedClassId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                $('#subject-Status').hide();
                $('#subject-table').show();
                _this.subjectList = data;
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.getsubjectDetailView = function (subjectName, subjectId, subjectBelonging) {
        var _this = this;
        $('#offcanvas-view-classsubjects_click').click();
        this.getViewsubjectName = subjectName;
        this.getViewsubjectCategoryName = null;
        this.getViewsubjectBelonging = subjectBelonging;
        this.getSubjectId = subjectId;
        this.getSubjectListViewArray = [];
        this.classManageService.schoolClassManagementGetSubjectListView(subjectId, this.getAuthToken)
            .subscribe(function (data) {
            for (var i = 0; i < data.subjectlistview.length; i++) {
                _this.getSubjectListViewArray.push(data.subjectlistview[i]);
                if (data.subjectlistview.length - 1 == i) {
                    _this.scriptInit();
                }
            }
            if (_this.getSubjectListViewArray.length == 0) {
                $('#unavailSub').show();
            }
            else {
                $('#unavailSub').hide();
            }
        }, function (error) {
            return function () { };
        });
    };
    ClassmanageComponent.prototype.assignsubjectinclass = function (sub, val) {
        if (val == true) {
            this.classManageService.assignSubjectToClass(this.sectionId, sub.subjectId, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                if (data.status == true) {
                    customToastr('Subject successfully assigned to class', 'success');
                }
                else {
                    customToastr('Subject is not assigned to class, please try again', 'error');
                }
            }, function (error) {
                return function () {
                };
            });
        }
        else {
            this.classManageService.removeSubjectfromClass(this.sectionId, sub.subjectId, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                if (data.status == true) {
                    customToastr('Subject successfully unassigned to class', 'success');
                }
                else {
                    customToastr('Subject is not unassigned to class, please try again', 'error');
                }
            }, function (error) {
                return function () {
                };
            });
        }
    };
    ClassmanageComponent.prototype.searchstaffForsubject = function (searchStaff) {
        this.classManageService.searchStaff(searchStaff.searchData, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            if (data.errorcode) {
            }
            else {
                searchStaff.stafList = data;
                if (data > 0) {
                    $('#search-staff_' + searchStaff.subjectId).show();
                }
                else {
                    $('#search-staff_' + searchStaff.subjectId).hide();
                }
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.assignSubjectToStaff = function (staff, sub) {
        var _this = this;
        sub.stafList = [];
        $('#search-staff_' + sub.stafList).hide();
        var classId = this.sectionId;
        this.classManageService.assignSubjectTostaff(staff.staffId, classId, sub.subjectId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            if (data.status == true) {
                customToastr('Subject assign to staff successfully', 'success');
                _this.getSubjectListByClass(classId);
            }
            else {
                customToastr('Subject is not assign to staff please try again', 'error');
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.unAssignStaffFromSubject = function (staf) {
        var _this = this;
        var classId = this.sectionId;
        this.classManageService.unAssignSubjectTostaff(classId, staf.staffId, staf.subjectId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            if (data.status == true) {
                customToastr('Staff successfully unassigned from subject', 'success');
                _this.getSubjectListByClass(classId);
            }
            else {
                customToastr('Staff not unassigned from subject, please try again', 'error');
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.addNewSubject = function (addSub) {
        var _this = this;
        $('#addSubButton').text("Please wait");
        $('#addSubButton').attr('disabled', true);
        $('#addSub').text("Please wait");
        $('#addSub').attr('disabled', true);
        var classId = this.sectionId;
        if (addSub.subjectname != null && addSub.subjectname != "" && addSub.subjectname != undefined) {
            this.classManageService.addSubject(addSub.subjectname, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                $('#addSubButton').text("ADD SUBJECT");
                $('#addSubButton').attr('disabled', false);
                $('#addSub').text("ADD SUBJECT");
                $('#addSub').attr('disabled', false);
                if (data.status == true) {
                    customToastr('Subject added successfully', 'success');
                    $('#addNewSubject').hide();
                    $('#addSubject').show();
                    $('#add-new-subject').hide();
                    $('#addSubjectNew').show();
                    _this.getSubjectListByClass(classId);
                    _this.listAllSubjects();
                }
                else if (data.status == false) {
                    customToastr('Subject is already exist, please try different name', 'error');
                }
                else {
                    customToastr('Subject is not added, please try again', 'error');
                }
            }, function (error) {
                return function () {
                };
            });
        }
        else {
            $('#addSubButton').text("ADD SUBJECT");
            $('#addSubButton').attr('disabled', false);
            $('#addSub').text("ADD SUBJECT");
            $('#addSub').attr('disabled', false);
            customToastr('Please give subject name', 'error');
        }
    };
    ClassmanageComponent.prototype.viewTeacher = function () {
        $('#classTeacherList').hide();
        $('#noClassTeacher').hide();
        $('#classTeacher-Status').show();
        var selectSection = $("#selectSection").val();
        this.classSectionId = selectSection;
        if (selectSection != '') {
            if (selectSection != 0) {
                this.getClassTeacherListByClass(selectSection);
            }
            else {
                $('#classTeacher-Status').hide();
                customToastr('Please select section!', 'error');
            }
        }
    };
    ClassmanageComponent.prototype.getClassTeacherListByClass = function (classId) {
        var _this = this;
        this.classManageService.fetchClassTeacher(this.classSectionId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            if (data.length > 0) {
                $('#classTeacherList').show();
            }
            $('#classTeacher-Status').hide();
            _this.teacherList = data.status == false ? [] : data;
            if (data.length == 3) {
                $('#addTeacher').hide();
            }
            else {
                $('#addTeacher').show();
            }
            if (data.status == false) {
                $('#noClassTeacher').show();
            }
            else {
                $('#noClassTeacher').hide();
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.searchClassTeacher = function (searchTeacher) {
        var _this = this;
        this.staffsList = [];
        this.classManageService.searchClassTeacher(this.teacher.searchTeacher, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            _this.staffsList = data;
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.assignClassTeacher = function (staffs) {
        var _this = this;
        $('#noClassTeacher').hide();
        $('#stdTeacherList').hide();
        $('#addNewClassTeacher').hide();
        var position;
        if (this.teacherList.length == 0) {
            position = 0;
        }
        else if (this.teacherList.length == 1) {
            position = 1;
        }
        else {
            position = 2;
        }
        var classId = this.classSectionId;
        this.classManageService.addClassTeacher(classId, staffs.staffId, position, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            if (data.status == true) {
                customToastr('Teacher assign to this class successfully', 'success');
                _this.getClassTeacherListByClass(classId);
            }
            else {
                customToastr('Teacher is not assign to this class, please try again', 'error');
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.removeclassTeacherPopUp = function (id, pos) {
        $('#remove-teacher').modal('show');
        this.delTeacherId = id;
        this.delTeacherPos = pos;
    };
    ClassmanageComponent.prototype.removeTeacherFromClass = function () {
        var _this = this;
        var classId = this.classSectionId;
        this.classManageService.removeteacher(classId, this.delTeacherId, this.delTeacherPos, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            if (data.status == true) {
                customToastr('Teacher removed from this class successfully', 'success');
                $('#closeremoveTeacherpopup').trigger("click");
                _this.getClassTeacherListByClass(classId);
            }
            else {
                customToastr('Teacher is not removed from this class, please try again', 'error');
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.updateClassOrder = function () {
        var _this = this;
        $('#classes-Status').show();
        var classArray = [];
        $('#sequence-classList .classId').each(function () {
            classArray.push($(this).html());
        });
        this.classList = classArray;
        var sequence, classId, sequenceStatus;
        var classStdList = [];
        for (var i = 0; i < this.classList.length; i++) {
            sequence = i + 1;
            classId = this.classList[i];
            classStdList.push({ pkClassStandardsId: classId, sequence: sequence });
        }
        this.classManageService.updateClassOrder(classStdList, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            sequenceStatus = data.status;
            if (data.status == true) {
                customToastr('Class successfully ordered', 'success');
                _this.getClassList();
                $('#classes-Status').hide();
                $('#classList').show();
                $('#updateClassSequenceOrder').hide();
            }
            else {
                customToastr('Class is not ordered, please try again', 'error');
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.updateSectionOrder = function (classStandardId) {
        var _this = this;
        var stdId = this.classStdId;
        $('#sectionOrder-Status').show();
        var sectionArray = [];
        var sequence, sectionId;
        var sectionList = [];
        $('#sequence-SectionList .sectionId').each(function () {
            sectionArray.push($(this).html());
        });
        for (var i = 0; i < sectionArray.length; i++) {
            sequence = i + 1;
            sectionId = sectionArray[i];
            sectionList.push({ classId: sectionId, sectionSequence: sequence });
        }
        this.classManageService.updateClassSectionOrder(sectionList, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            if (data.status == true) {
                customToastr('Class section successfully ordered', 'success');
                $('#sectionOrder-Status').hide();
                $('#updateSectionSequence').hide();
                $('#section-List').show();
                _this.getSectionListClassView(_this.classStdId);
            }
            else {
                customToastr('Class section is not ordered, please try again', 'error');
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.updateSubjectName = function () {
        var _this = this;
        $('#updateSub').text("Please wait");
        $('#updateSub').attr('disabled', true);
        var subjectName = $('#subject').val();
        if (this.formValidator.validateIsData(subjectName)) {
            this.classManageService.updateSubject(this.getSubjectId, subjectName, localStorage.getItem('usertoken'))
                .subscribe(function (data) {
                $('#updateSub').text("Update");
                $('#updateSub').attr('disabled', false);
                if (data.status == true) {
                    $('#closeUpdateSubject').trigger("click");
                    customToastr('Subject updated successfully', 'success');
                    _this.listAllSubjects();
                }
                else if (data.status == false) {
                    customToastr('Subject is already exist, please try different subject name!', 'error');
                }
                else {
                    customToastr('Subject is not updated, please try again', 'error');
                }
            }, function (error) {
                return function () {
                };
            });
        }
        else {
            $('#updateSub').text("Update");
            $('#updateSub').attr('disabled', false);
            customToastr('Please enter subject name', 'error');
        }
    };
    /*Data table operation*/
    ClassmanageComponent.prototype.destroyDataTable = function (table) {
        $('#' + table).DataTable().destroy();
    };
    ClassmanageComponent.prototype.initializeDataTable = function (table) {
        setTimeout(function () {
            $('#' + table).DataTable({
                "order": [[0, 'asc']]
            });
            //this.scriptInit();
        }, 500);
    };
    ClassmanageComponent.prototype.ViewstudentListClassSection = function () {
        var _this = this;
        $('#viewstud').text("Please wait");
        $('#viewstud').attr('disabled', true);
        this.getClassSectionStudentListArray = [];
        this.destroyDataTable('view-class-detail-student-table');
        if (this.getSeletedSectionClassIdView != undefined) {
            this.classManageService.getStudentListClassSection(this.getSchoolId, this.academicId, this.getSeletedSectionClassIdView, this.getAuthToken)
                .subscribe(function (data) {
                $('#viewstud').text("View students");
                $('#viewstud').attr('disabled', false);
                for (var i = 0; i < data.data.length; i++) {
                    _this.getClassSectionStudentListArray.push(data.data[i]);
                    if (data.data.length - 1 == i) {
                        _this.initializeDataTable('view-class-detail-student-table');
                    }
                }
                if (_this.getClassSectionStudentListArray.length == 0) {
                    //$('#studentList').show();
                    _this.studentListClassSectionStatus = true;
                }
                else {
                    //$('#studentList').hide();
                    _this.studentListClassSectionStatus = false;
                }
            }, function (error) {
                return function () {
                };
            });
        }
        else {
            $('#viewstud').text("View students");
            $('#viewstud').attr('disabled', false);
            customToastr('Please select section!', 'error');
        }
    };
    //token auth
    ClassmanageComponent.prototype.isValidToken = function () {
        var _this = this;
        var isValidToken = false;
        var isValidSchool = 0;
        this.loginservice.isValidToken(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            isValidToken = data.isValid;
            if (isValidToken == true) {
                _this.loginservice.isValidSchoolId(localStorage.getItem('schoolinfo'))
                    .subscribe(function (data) {
                    isValidSchool = data.schoolExist;
                    _this.getSchoolId = localStorage.getItem('schoolinfo');
                    //this.getSchoolId=20;
                    if (isValidSchool != 1) {
                        _this.router.navigate(['/login']);
                    }
                    else {
                        _this.scriptInit();
                        _this.onPageLoad();
                        _this.classManagementGetClassCategory();
                    }
                }, function (error) {
                    return function () { };
                });
            }
            else {
                _this.router.navigate(['/login']);
            }
        }, function (error) {
            return function () {
            };
        });
    };
    ClassmanageComponent.prototype.scriptInit = function () {
        $(".after").empty();
        $("#device-breakpoints").remove();
        $(".after").append("<script>function customToastr(message,type){toastr.options.hideDuration = 0;toastr.clear();toastr.options.closeButton = true;toastr.options.progressBar = false;toastr.options.debug = false;toastr.options.positionClass = 'toast-top-right';toastr.options.showDuration = 330;toastr.options.hideDuration = 330;toastr.options.timeOut = 5000;toastr.options.extendedTimeOut = 1000;toastr.options.showEasing = 'swing';toastr.options.hideEasing = 'swing';toastr.options.showMethod = 'slideDown';toastr.options.hideMethod = 'slideUp';toastr[type](message, '');return 0;}	</" + "script>");
    };
    ClassmanageComponent.prototype.onPageLoad = function () {
        $('.particles-bg').particleground({
            dotColor: '#f3f3f3',
            lineColor: '#ffffff',
            lineWidth: .3
        });
        $('.particles-bg-main').particleground({
            dotColor: '#dadada',
            lineColor: '#ffffff',
            lineWidth: .3
        });
        //disable clicks
        $(".tab-timetable").children().unbind('click');
        $(".timetable-tab").children().unbind('click');
    };
    ClassmanageComponent.prototype.ngOnInit = function () {
        var adminPermissionlist = localStorage.getItem('adminPermissionlist');
        if (adminPermissionlist) {
            var list = JSON.parse(adminPermissionlist);
            this.permissions = list;
        }
        this.isValidToken();
        $('#subjectAdd').click(function () {
            $('#addNewSubject').show();
            $('#addSubject').hide();
        });
        $('#canceladdSubject').click(function () {
            $('#addNewSubject').hide();
            $('#addSubject').show();
        });
        $('#addnewteacher').click(function () {
            $('#addNewClassTeacher').show();
        });
        $('#sequence-class').click(function () {
            $('#classList').hide();
            $('#updateClassSequenceOrder').show();
        });
        $('#cancelupdateorder').click(function () {
            $('#classList').show();
            $('#updateClassSequenceOrder').hide();
        });
        $('#sequence-section').click(function () {
            $('#updateSectionSequence').show();
            $('#section-List').hide();
        });
        $('#cancelSectionorder').click(function () {
            $('#updateSectionSequence').hide();
            $('#section-List').show();
        });
        $('#addSubjectButton').click(function () {
            $('#add-new-subject').show();
            $('#addSubjectNew').hide();
        });
        $('#canceladdnewSubject').click(function () {
            $('#add-new-subject').hide();
            $('#addSubjectNew').show();
        });
    };
    ClassmanageComponent = __decorate([
        Component({
            selector: 'app-classmanage',
            templateUrl: './classmanageV1.component.html',
            styleUrls: [
                './../../assets/css/theme-default/bootstrap.css?1422792965',
                './../../assets/css/theme-default/materialadmin.css?1425466319',
                './../../assets/css/theme-default/font-awesome.min.css?1422529194',
                './../../assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
                './../../assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
                './../../assets/css/theme-default/material-design-iconic-font.min.css?1421434286',
                './../../assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
                './../../assets/css/theme-default/libs/bootstrap-tagsinput/bootstrap-tagsinput.css?1424887862',
                './../../assets/css/theme-default/libs/toastr/toastr.css?1425466569',
                './../../assets/css/theme-default/libs/nestable/nestable.css?1423393667',
                './../../assets/css/theme-default/libs/jquery-ui/jquery-ui-theme.css?1423393666',
                './../../css/timepicker.css',
                './classmanage.component.css'],
            encapsulation: ViewEncapsulation.None,
            providers: [LoginService, ClassManageService, GroupService, FormValidator]
        }), 
        __metadata('design:paramtypes', [Router, LoginService, ClassManageService, GroupService, Renderer, FormValidator])
    ], ClassmanageComponent);
    return ClassmanageComponent;
}());
//# sourceMappingURL=F:/BW_PR/src/app/classmanage/classmanage.component.js.map