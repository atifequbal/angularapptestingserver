var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import { LoginService } from "../login/login.service";
export var BroadcastComponent = (function () {
    function BroadcastComponent(router, loginservice) {
        this.router = router;
        this.loginservice = loginservice;
    }
    //token auth
    BroadcastComponent.prototype.isValidToken = function () {
        var _this = this;
        var isValidToken = false;
        var isValidSchool = 0;
        this.loginservice.isValidToken(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            isValidToken = data.isValid;
            console.log("Token Validation in login page: ", isValidToken);
            if (isValidToken == true) {
                //this.router.navigate(['/dashboard']);
                //school id validation///////////////////////////////////////////////
                _this.loginservice.isValidSchoolId(localStorage.getItem('schoolinfo'))
                    .subscribe(function (data) {
                    isValidSchool = data.schoolExist;
                    _this.getSchoolId = localStorage.getItem('schoolinfo');
                    if (isValidSchool != 1) {
                        _this.router.navigate(['/login']);
                    }
                    else {
                        console.log("Stay in page (Dash board)");
                    }
                }, function (error) { return console.log(error); }, function () { });
            }
            else {
                _this.router.navigate(['/login']);
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    BroadcastComponent.prototype.ngOnInit = function () {
        this.isValidToken();
        $(".after").empty();
        $("#device-breakpoints").remove();
        //$('.after').append("<script type='text/javascript' src='inline.bundle.js'></script><script type='text/javascript' src='scripts.bundle.js'></script>");
    };
    BroadcastComponent = __decorate([
        Component({
            selector: 'app-broadcast',
            templateUrl: './broadcast.component.html',
            styleUrls: [
                'assets/css/theme-default/bootstrap.css?1422792965',
                'assets/css/theme-default/materialadmin.css?1425466319',
                'assets/css/theme-default/font-awesome.min.css?1422529194',
                'assets/css/theme-default/material-design-iconic-font.min.css?1421434286',
                'assets/css/theme-default/libs/summernote/summernote.css?1425218701',
                './broadcast.component.css'],
            encapsulation: ViewEncapsulation.None,
            providers: [LoginService]
        }), 
        __metadata('design:paramtypes', [Router, LoginService])
    ], BroadcastComponent);
    return BroadcastComponent;
}());
//# sourceMappingURL=F:/BW_PR/src/app/broadcast/broadcast.component.js.map