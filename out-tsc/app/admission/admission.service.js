var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import 'rxjs/Rx';
import { AppSettings } from "../../config/config";
export var AdmissionService = (function () {
    //private AUTHTOKEN = LoginComponent.LOGIN_TOKEN
    function AdmissionService(http) {
        this.http = http;
        this._url = AppSettings.API_ENDPOINT;
    }
    AdmissionService.prototype.getAllAcademicYears = function (schoolId, AUTHTOKEN) {
        console.log("getAllAcademicYears api call");
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/academics/getacademicyearsfromnow', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AdmissionService.prototype.getAllCategories = function (schoolId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classManagement/getcategories', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AdmissionService.prototype.getCategoryDetailsFromCategoryId = function (schoolId, categoryId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, categoryId: categoryId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/className', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AdmissionService.prototype.getClassDetailsFromClassId = function (classId, AUTHTOKEN) {
        var json = JSON.stringify({ classId: classId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/classSectionName', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AdmissionService.prototype.getFeeComponentsOfClass = function (schoolId, academicId, classId, AUTHTOKEN) {
        var json = JSON.stringify({ schoolId: schoolId, academicId: academicId, classId: classId });
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageFees/feeboard/classId', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AdmissionService.prototype.applyFeesAmountForTerm = function (schoolId, academicId, classId, feesTypeId, amount, AUTHTOKEN) {
        var feesObject = {
            schoolId: schoolId,
            academicId: academicId,
            classId: classId,
            fkFeesTypeIdList: [{ feeTypeId: feesTypeId, amount: amount }]
        };
        var json = JSON.stringify(feesObject);
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageFees/feeboard/addNewFees', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AdmissionService.prototype.createFeesComponent = function (feesObj, AUTHTOKEN) {
        var json = JSON.stringify(feesObj);
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/manageFees/createNewfeeComponent', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AdmissionService.prototype.uploadImage = function (imageString, AUTHTOKEN) {
        var data = new FormData();
        var block = imageString.split(";");
        var contentType = block[0].split(":")[1];
        var realData = block[1].split(",")[1];
        var blob = this.b64toBlob(realData, contentType);
        data.append('image', blob);
        var headers = new Headers();
        //headers.append('Content-Type','application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/upload/image/uploadandgetpath?userType=NEW_ADMISSION', data, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AdmissionService.prototype.createNewAdmission = function (admissionObj, AUTHTOKEN) {
        var json = JSON.stringify(admissionObj);
        var params = json;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('authToken', AUTHTOKEN);
        return this.http.post(this._url + '/addstudent/newAdmission', params, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    AdmissionService.prototype.b64toBlob = function (b64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };
    AdmissionService = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [Http])
    ], AdmissionService);
    return AdmissionService;
}());
//# sourceMappingURL=F:/BW_PR/src/app/admission/admission.service.js.map