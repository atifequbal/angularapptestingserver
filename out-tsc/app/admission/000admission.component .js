var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewEncapsulation, ElementRef } from '@angular/core';
import { AdmissionService } from "./admission.service";
import { Router } from "@angular/router";
import { LoginService } from "../login/login.service";
import { GroupService } from "../groups/groups.service";
import { TransportService } from "../transport/transport.service";
export var AdmissionComponent = (function () {
    function AdmissionComponent(admissionservice, groupservice, router, loginservice, elRef, transportservice) {
        this.admissionservice = admissionservice;
        this.groupservice = groupservice;
        this.router = router;
        this.loginservice = loginservice;
        this.elRef = elRef;
        this.transportservice = transportservice;
        this.currentSchoolId = "";
        this.selectedAcademicYearObj = null;
        this.selectedCategory = null;
        this.selectedClassSection = null;
        this.selectedBusRoute = null;
        this.selectedBusStop = null;
        this.academicId = 0;
        this.schoolAcademicCurrentYear = "";
        this.feesComponentsList = [];
        this.addAmountStatus = true;
        this.applyAmountStatus = false;
        this.mTotalFees = "Rs. 0";
        this.mTotalBusFees = "Rs. 0";
        this.feesAmount = 0;
        this.mFeesComponentName = "";
        this.mFeesComponentDue = "";
        this.mFeesComponentAmount = "0";
        this.busRouteList = [];
        this.busStopList = [];
        this.admissionStudentImage = "assets/img/avatar-students.png";
        this.mStudentName = "";
        this.mTotalFees = "Rs. 0";
    }
    AdmissionComponent.prototype.initScript = function () {
        $(".after").empty();
        $("#device-breakpoints").remove();
        //$('.after').append("<script type='text/javascript' src='inline.bundle.js'></script><script type='text/javascript' src='scripts.bundle.js'></script>");
        $(".after").append("<script>$('.particles-bg').particleground({dotColor: '#dadada',lineColor: '#c4c4c4',lineWidth: .3});</" + "script>");
    };
    AdmissionComponent.prototype.isValidToken = function () {
        var _this = this;
        var isValidToken = false;
        var isValidSchool = 0;
        this.loginservice.isValidToken(localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            isValidToken = data.isValid;
            console.log("Token Validation in login page: ", isValidToken);
            if (isValidToken == true) {
                //this.router.navigate(['/dashboard']);
                //school id validation///////////////////////////////////////////////
                _this.loginservice.isValidSchoolId(localStorage.getItem('schoolinfo'))
                    .subscribe(function (data) {
                    isValidSchool = data.schoolExist;
                    _this.currentSchoolId = localStorage.getItem('schoolinfo');
                    if (isValidSchool != 1) {
                        _this.router.navigate(['/login']);
                    }
                    else {
                        console.log("Isvalid token of Admission");
                        _this.getAllAcademicYears();
                        _this.getAllCategories();
                        _this.getCurrentAcademic(_this.currentSchoolId);
                        _this.getAllBusRoutes(_this.currentSchoolId);
                    }
                }, function (error) { return console.log(error); }, function () { });
            }
            else {
                _this.router.navigate(['/login']);
            }
        }, function (error) { return console.log(error); }, function () {
        });
    };
    AdmissionComponent.prototype.ngOnInit = function () {
        this.initScript();
        this.isValidToken();
    };
    AdmissionComponent.prototype.elementClick = function () {
        //this.elRef.nativeElement.querySelector('#add-amount').addEventListener('click', (event) => this.handleEvent(event));
    };
    // handleEvent(event) {
    //   console.log("add")
    // }
    AdmissionComponent.prototype.getCurrentAcademic = function (schoolId) {
        var _this = this;
        this.groupservice.groupsStudentAcademicList(schoolId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            for (var i = 0; i < data.academicList.length; i++) {
                //this.schoolAcademicList.push(data.academicList[i]);
                if (data.academicList[i].InActive == 0) {
                    _this.academicId = data.academicList[i].pkAcademicId;
                    _this.schoolAcademicCurrentYear = data.academicList[i].Year;
                }
            }
            console.log("School  Academic Current id: ", _this.academicId);
        }, function (error) { return console.log(error); }, function () {
        });
    };
    AdmissionComponent.prototype.getAllBusRoutes = function (schoolId) {
        var _this = this;
        this.transportservice.getAllBuses(schoolId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Bus Routes", data);
            _this.busRouteList = data;
        }, function (error) { return console.log(error); }, function () {
        });
    };
    AdmissionComponent.prototype.getStopsOfRoute = function (schoolId, classId) {
        var _this = this;
        this.transportservice.getstopsOfRoute(schoolId, classId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Bus Stops", data);
            _this.busStopList = data;
        }, function (error) { return console.log(error); }, function () {
        });
    };
    AdmissionComponent.prototype.getAllAcademicYears = function () {
        var _this = this;
        this.admissionservice.getAllAcademicYears(this.currentSchoolId, localStorage.getItem('usertoken'))
            .subscribe(
        // data => this.schoolMatrices  = data,
        function (data) {
            console.log("Fetch Academic years : ", data);
            if (data != null && data != undefined && data.length > 0) {
                _this.academicYearsList = data;
                _this.selectedAcademicYearObj = data[0];
            }
        }, function (error) { return console.log(error); });
    };
    AdmissionComponent.prototype.getAllCategories = function () {
        var _this = this;
        this.admissionservice.getAllCategories(this.currentSchoolId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Fetch Categories : ", data);
            _this.categoryList = data; //
        }, function (error) { return console.log(error); });
    };
    AdmissionComponent.prototype.fetchCategoryDetails = function (categoryId) {
        var _this = this;
        this.admissionservice.getCategoryDetailsFromCategoryId(this.currentSchoolId, categoryId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Fetch Category Details : ", data);
            if (data != undefined && data != null && data.classlist != undefined && data.classlist.length > 0) {
                _this.classSectionList = data.classlist;
            }
        }, function (error) { return console.log(error); });
    };
    AdmissionComponent.prototype.fetchFeeComponentOfClass = function (classId) {
        var _this = this;
        this.feesComponentsList = [];
        this.admissionservice.getFeeComponentsOfClass(this.currentSchoolId, this.academicId, classId, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Fetch Fee Component Details : ", data);
            if (data != null && data != undefined && data.length > 0) {
                _this.feesComponentsList = data;
                _this.calculateTotalFees();
            }
        }, function (error) { return console.log(error); });
    };
    AdmissionComponent.prototype.showAddAmount = function (feesTypeId, position) {
        console.log("component ", feesTypeId);
        $(".showaddamount_" + feesTypeId).hide();
        $(".showapplyamount_" + feesTypeId).show();
    };
    AdmissionComponent.prototype.applyAmount = function (feesTypeId, position) {
        console.log("apply_", feesTypeId + "__" + position);
        $(".showaddamount_" + feesTypeId).show();
        $(".showapplyamount_" + feesTypeId).hide();
        var amount = $(".feesamount_" + feesTypeId).val();
        console.log("Amount: " + amount + " _ ");
        if (amount != null && amount != undefined && amount != "") {
            try {
                amount = parseInt(amount);
                var feesData = this.feesComponentsList;
                feesData[position].Amount = amount;
                this.feesComponentsList = feesData;
                this.calculateTotalFees();
                this.applyFeesAmountForTerm(feesTypeId, amount, position);
            }
            catch (err) {
                console.log(err);
            }
        }
    };
    AdmissionComponent.prototype.applyFeesAmountForTerm = function (feesTypeId, amount, position) {
        var _this = this;
        this.admissionservice.applyFeesAmountForTerm(this.currentSchoolId, this.academicId, this.selectedClassSection, feesTypeId, amount, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Fetch Fee Component Details : ", data);
            if (data != undefined && data != null && data.status != undefined) {
                if (data.status == false) {
                    delete _this.feesComponentsList[position].Amount;
                }
                else {
                    console.log("Applied");
                }
            }
            else {
                delete _this.feesComponentsList[position].Amount;
            }
            _this.calculateTotalFees();
            //this.fetchFeeComponentOfClass(this.selectedClassSection);
        }, function (error) {
            console.log("ERR");
            console.log(error);
            delete _this.feesComponentsList[position].Amount;
            _this.calculateTotalFees();
        });
    };
    AdmissionComponent.prototype.addFeesComponent = function () {
        var _this = this;
        var selectedAssignee = $('#fees-comp-assigned').find(":selected").text();
        console.log(this.mFeesComponentName);
        console.log(this.mFeesComponentAmount);
        this.mFeesComponentDue = $("#fees-comp-due").val();
        console.log($("#fees-comp-due").val());
        console.log(selectedAssignee);
        var feesObj = {
            Name: this.mFeesComponentName,
            includeInTerm: 0,
            academicId: this.academicId,
            AppliesTo: selectedAssignee,
            duedate: this.mFeesComponentDue,
            amount: this.mFeesComponentAmount,
            classId: this.selectedClassSection,
            schoolId: this.currentSchoolId
        };
        var updateInfo = {
            Name: feesObj.Name,
            Amount: this.mFeesComponentAmount,
            pkFeesTypeId: -100
        };
        this.feesComponentsList.push(updateInfo);
        this.admissionservice.createFeesComponent(feesObj, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("Fetch Fee Component Details : ", data);
            //this.fetchFeeComponentOfClass(this.selectedClassSection);
            if (data.status != undefined && data.status == true) {
                _this.feesComponentsList[_this.feesComponentsList.length - 1].pkFeesTypeId = data.data.pkFeesTypeId;
            }
        }, function (error) {
            console.log("ERR");
            console.log(error);
        });
        $("#newcomponent .close").click();
    };
    AdmissionComponent.prototype.calculateTotalFees = function () {
        this.feesAmount = 0;
        for (var i = 0; i < this.feesComponentsList.length; i++) {
            if (this.feesComponentsList[i].Amount != undefined) {
                this.feesAmount += this.feesComponentsList[i].Amount;
            }
        }
        this.mTotalFees = "Rs. " + this.feesAmount;
    };
    AdmissionComponent.prototype.iterateFareOfBus = function (stopNumber) {
        for (var i = 0; i < this.busStopList.length; i++) {
            if (this.busStopList[i].StopNumber == stopNumber) {
                this.mTotalBusFees = "Rs " + this.busStopList[i].fare;
                break;
            }
        }
    };
    AdmissionComponent.prototype.iterateBusId = function (routeId) {
        for (var i = 0; i < this.busRouteList.length; i++) {
            if (this.busRouteList[i].pkRouteId == routeId) {
                this.mBusId = this.busRouteList[i].pkBusId;
                break;
            }
        }
    };
    AdmissionComponent.prototype.finishedAdmission = function () {
        if (this.mFatherEmail != undefined && this.mFatherEmail != "" && this.mStudentName != undefined && this.mStudentName != "" && this.selectedClassSection != undefined) {
            this.mMotherOccupation = $('#admission-mother-occupation').find(":selected").text();
            this.mAdmissionBase = $("#admission-base").find(":selected").text();
            this.mAdmissionStream = $("#admission-stream").find(":selected").text();
            this.mDOB = $("#admission-dob").val();
            this.mAdmissionDate = $("#admission-date").val();
            this.mGuardianOccupation = $("#admission-guardian-occupation").find(":selected").text();
            this.mFatherOccupation = $("#admission-father-occupation").val();
            this.mBloodGroup = $("#admission-blood-group").find(":selected").text();
            this.mReligion = $("#admission-religion").find(":selected").text();
            var admissionObj = {
                schoolId: this.currentSchoolId,
                registrationId: this.mAdmissionNumber,
                studentName: this.mStudentName,
                classId: this.selectedClassSection,
                parentName: this.mFatherName,
                studentEmail: this.mStudentEmail,
                emailID: this.mFatherEmail,
                dateOfBirth: this.mDOB,
                mobileNumber: this.mFatherNumber,
                dateOfAdmission: this.mAdmissionDate,
                address: this.mFatherAddress,
                fkAcademicId: this.selectedAcademicYearObj,
                academicId: this.selectedAcademicYearObj,
                surname: this.mSurName,
                bloodGroup: this.mBloodGroup,
                religion: this.mReligion,
                cast: this.mCast,
                nationality: this.mNationality,
                studentPhone: this.mStudenNumber,
                fatherOccupation: this.mFatherOccupation,
                fatherQualification: this.mFatherQualification,
                fatherEmail: this.mFatherEmail,
                motherName: this.mMotherName,
                motherPhone: this.mMotherNumber,
                motherOccupation: this.mMotherOccupation,
                motherQualification: this.mMotherQualification,
                motherEmail: this.mMotherEmail,
                motherAddress: this.mMotherAddress,
                guardianName: this.mGuardianName,
                guardianPhone: this.mGuardianNumber,
                guardianOccupation: this.mGuardianOccupation,
                guardianQualification: this.mGuardianQualification,
                guardianEmail: this.mGuardianEmail,
                gender: this.mGenderGroup,
                admissionBase: this.mAdmissionBase,
                admissionStream: this.mAdmissionStream,
                studentImage: "/image/placeholder.png",
                stopId: this.selectedBusStop,
                busId: this.mBusId
            };
            var imageData = $(".cropme-admission img").attr('src');
            console.log(imageData);
            console.log(admissionObj);
            //TODO: Show progress
            if (imageData != this.admissionStudentImage) {
                this.uploadNewAdmissionImage(imageData, function (imageName) {
                    admissionObj.studentImage = imageName;
                    this.callNewAdmissionService(admissionObj);
                });
            }
            else {
                this.callNewAdmissionService(admissionObj);
            }
        }
        else {
        }
    };
    AdmissionComponent.prototype.callNewAdmissionService = function (admissionObj) {
        this.admissionservice.createNewAdmission(admissionObj, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            //callback(data);
            //TODO: Check Status & show status message
        }, function (error) {
            console.log(error);
            //callback(false)
            //TODO: Show error message
        });
    };
    AdmissionComponent.prototype.uploadNewAdmissionImage = function (imageData, callback) {
        this.admissionservice.uploadImage(imageData, localStorage.getItem('usertoken'))
            .subscribe(function (data) {
            console.log("FIMAGE UPLAOD : ", data);
            callback(data);
        }, function (error) {
            console.log(error);
            callback(false);
        });
    };
    AdmissionComponent.prototype.updateSelectedAcademic = function (academicId) {
        this.selectedAcademicYearObj = academicId;
        console.log(this.selectedAcademicYearObj);
    };
    AdmissionComponent.prototype.updateSelectedCategory = function (categoryId) {
        this.selectedCategory = categoryId;
        console.log(this.selectedCategory);
        this.fetchCategoryDetails(this.selectedCategory);
    };
    AdmissionComponent.prototype.updateSelectedClassSection = function (classId) {
        this.selectedClassSection = classId;
        console.log(this.selectedClassSection);
        this.fetchFeeComponentOfClass(this.selectedClassSection);
    };
    AdmissionComponent.prototype.updateSelectedBusRoute = function (routeId) {
        this.selectedBusRoute = routeId;
        this.iterateBusId(routeId);
        this.getStopsOfRoute(this.currentSchoolId, this.selectedBusRoute);
    };
    AdmissionComponent.prototype.updateSelectedBusStop = function (stopNumber) {
        this.selectedBusStop = stopNumber;
        this.iterateFareOfBus(stopNumber);
    };
    AdmissionComponent = __decorate([
        Component({
            selector: 'app-admission',
            templateUrl: './admission.component.html',
            styleUrls: [
                'assets/css/theme-default/bootstrap.css?1422792965',
                'assets/css/theme-default/materialadmin.css?1425466319',
                'assets/css/theme-default/font-awesome.min.css?1422529194',
                'assets/css/theme-default/material-design-iconic-font.min.css?1421434286',
                'assets/css/theme-default/libs/rickshaw/rickshaw.css?1422792967',
                'assets/css/theme-default/libs/morris/morris.core.css?1420463396',
                'assets/css/theme-default/libs/wizard/wizard.css?1425466601',
                'assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858',
                '_crop/css/style.css',
                '_crop/css/style-example.css',
                '_crop/css/jquery.Jcrop.css',
                './admission.component.css'],
            encapsulation: ViewEncapsulation.None,
            providers: [AdmissionService, GroupService, LoginService, TransportService]
        }), 
        __metadata('design:paramtypes', [AdmissionService, GroupService, Router, LoginService, ElementRef, TransportService])
    ], AdmissionComponent);
    return AdmissionComponent;
}());
//# sourceMappingURL=F:/BW_PR/src/app/admission/000admission.component .js.map